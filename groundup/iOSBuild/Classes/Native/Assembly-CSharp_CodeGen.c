﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Actor::Start()
extern void Actor_Start_m1CFDC1005F8A86658B8C3EB9A6C86ABF02C3E265 ();
// 0x00000002 System.Void Actor::OnDestroy()
extern void Actor_OnDestroy_m490DA289265E3BC6984CE42330699CAB75C0433E ();
// 0x00000003 System.Void Actor::.ctor()
extern void Actor__ctor_m80C1F3A5D7941420C895B29FF5B4930EE74AAC98 ();
// 0x00000004 System.Collections.Generic.List`1<Actor> ActorsManager::get_actors()
extern void ActorsManager_get_actors_m6499CE3048819DF26F7DA45E040FFBD01B38A6EB ();
// 0x00000005 System.Void ActorsManager::set_actors(System.Collections.Generic.List`1<Actor>)
extern void ActorsManager_set_actors_mAB53F8E386254363CA260817280FF1775000DCE1 ();
// 0x00000006 System.Void ActorsManager::Awake()
extern void ActorsManager_Awake_mEBE95BC11E7357D2EBC9D155BB406F8C8EF4CFCB ();
// 0x00000007 System.Void ActorsManager::.ctor()
extern void ActorsManager__ctor_mF1732900B4501292F24D047D975ECED1ACD42D3B ();
// 0x00000008 UnityEngine.Audio.AudioMixerGroup[] AudioManager::FindMatchingGroups(System.String)
extern void AudioManager_FindMatchingGroups_m29D056E41D326C21D1679C8432A60A73C2280079 ();
// 0x00000009 System.Void AudioManager::SetFloat(System.String,System.Single)
extern void AudioManager_SetFloat_mF3246EC004ED0D48CED84509978FECA38B1C5F92 ();
// 0x0000000A System.Void AudioManager::GetFloat(System.String,System.Single&)
extern void AudioManager_GetFloat_mE5520ACFE8A93DACC04D968C1195FDC2BADFD968 ();
// 0x0000000B System.Void AudioManager::.ctor()
extern void AudioManager__ctor_m9FC47B3998693846C091FB4C3731ACEF0359AC48 ();
// 0x0000000C System.Void AudioUtility::CreateSFX(UnityEngine.AudioClip,UnityEngine.Vector3,AudioUtility_AudioGroups,System.Single,System.Single)
extern void AudioUtility_CreateSFX_mE48AA1558D57DC802A15D0DA9CF515CC950B8878 ();
// 0x0000000D UnityEngine.Audio.AudioMixerGroup AudioUtility::GetAudioGroup(AudioUtility_AudioGroups)
extern void AudioUtility_GetAudioGroup_mA3EC513FB52E381B812EFCA7AD9836C71752E8E6 ();
// 0x0000000E System.Void AudioUtility::SetMasterVolume(System.Single)
extern void AudioUtility_SetMasterVolume_m9887F1897BF130E71C0AE14967E9BEEB145A9F89 ();
// 0x0000000F System.Single AudioUtility::GetMasterVolume()
extern void AudioUtility_GetMasterVolume_m02B43342A8ADBDE81F13947ECF68160400103CFF ();
// 0x00000010 System.Void AudioUtility::.ctor()
extern void AudioUtility__ctor_m18E244FB9EF75C53F8390179A4C77F4E0534F7BE ();
// 0x00000011 System.Void ChargedProjectileEffectsHandler::OnEnable()
extern void ChargedProjectileEffectsHandler_OnEnable_m9E47C18396BE334A32858D2C64B1B93108AB2A80 ();
// 0x00000012 System.Void ChargedProjectileEffectsHandler::OnShoot()
extern void ChargedProjectileEffectsHandler_OnShoot_mE2033D78B169013C97E3D828FB4D92E1199678A5 ();
// 0x00000013 System.Void ChargedProjectileEffectsHandler::.ctor()
extern void ChargedProjectileEffectsHandler__ctor_m7C12E5B48B719359CDAC871E5918E27CCBC0B393 ();
// 0x00000014 UnityEngine.GameObject ChargedWeaponEffectsHandler::get_particleInstance()
extern void ChargedWeaponEffectsHandler_get_particleInstance_m2E8CB349320F9D6666F4805F1FBEABD6D45802C1 ();
// 0x00000015 System.Void ChargedWeaponEffectsHandler::set_particleInstance(UnityEngine.GameObject)
extern void ChargedWeaponEffectsHandler_set_particleInstance_mEF0BF733A1A399CDCA53CA9457592FE54EB95218 ();
// 0x00000016 System.Void ChargedWeaponEffectsHandler::Awake()
extern void ChargedWeaponEffectsHandler_Awake_m7D50E318D3176CA9620E3CEA80C92CAD3057B1C3 ();
// 0x00000017 System.Void ChargedWeaponEffectsHandler::SpawnParticleSystem()
extern void ChargedWeaponEffectsHandler_SpawnParticleSystem_m81B98D965A9DA16C6F6DB339072848B10BD14F49 ();
// 0x00000018 System.Void ChargedWeaponEffectsHandler::FindReferences()
extern void ChargedWeaponEffectsHandler_FindReferences_mF8B5217D0EB36597719419971DA2CC9D104B6EA3 ();
// 0x00000019 System.Void ChargedWeaponEffectsHandler::Update()
extern void ChargedWeaponEffectsHandler_Update_m5CBD6ED1539B1F7E10D57CAAECC966A8F72FAE7A ();
// 0x0000001A System.Void ChargedWeaponEffectsHandler::.ctor()
extern void ChargedWeaponEffectsHandler__ctor_m2A6FD0736D8CAA5765D21CF13439E766D407BF3B ();
// 0x0000001B System.Void ConstantRotation::Update()
extern void ConstantRotation_Update_mE0FA9F3E254F9A9C5AD62BFB216C6B0BFAB47EEC ();
// 0x0000001C System.Void ConstantRotation::.ctor()
extern void ConstantRotation__ctor_m75B3E5212B6CD5EFF6C665A3F69174BB2DB441EC ();
// 0x0000001D System.Void DamageArea::InflictDamageInArea(System.Single,UnityEngine.Vector3,UnityEngine.LayerMask,UnityEngine.QueryTriggerInteraction,UnityEngine.GameObject)
extern void DamageArea_InflictDamageInArea_mA630415A77BBFC9FBAE48732DD882B4F6198D0B1 ();
// 0x0000001E System.Void DamageArea::OnDrawGizmosSelected()
extern void DamageArea_OnDrawGizmosSelected_m7CCE155FA01977FE894EFDE59D604E0045269CDE ();
// 0x0000001F System.Void DamageArea::.ctor()
extern void DamageArea__ctor_m128D8CD2252A50D4AC2C0AC07335F59488758081 ();
// 0x00000020 Health Damageable::get_health()
extern void Damageable_get_health_m5C73D88A893B8AC146904E41CA8903A71F222BC7 ();
// 0x00000021 System.Void Damageable::set_health(Health)
extern void Damageable_set_health_mF166A391D435846ABCA8BD65FE164E2FB70CE454 ();
// 0x00000022 System.Void Damageable::Awake()
extern void Damageable_Awake_m69DFF61C30925599331F15D993B98D59B3FE9342 ();
// 0x00000023 System.Void Damageable::InflictDamage(System.Single,System.Boolean,UnityEngine.GameObject)
extern void Damageable_InflictDamage_mDECE304C922F4499293C79E658DBA86723E760FF ();
// 0x00000024 System.Void Damageable::.ctor()
extern void Damageable__ctor_m7EFFA40D796018855099921A46D5AEBDE014251E ();
// 0x00000025 System.Void DebugUtility::HandleErrorIfNullGetComponent(UnityEngine.Component,UnityEngine.Component,UnityEngine.GameObject)
// 0x00000026 System.Void DebugUtility::HandleErrorIfNullFindObject(UnityEngine.Object,UnityEngine.Component)
// 0x00000027 System.Void DebugUtility::HandleErrorIfNoComponentFound(System.Int32,UnityEngine.Component,UnityEngine.GameObject)
// 0x00000028 System.Void DebugUtility::HandleWarningIfDuplicateObjects(System.Int32,UnityEngine.Component,UnityEngine.GameObject)
// 0x00000029 System.Void Destructable::Start()
extern void Destructable_Start_m1A4B6A76EE25D66D58F786B691FA081EF0417CA8 ();
// 0x0000002A System.Void Destructable::OnDamaged(System.Single,UnityEngine.GameObject)
extern void Destructable_OnDamaged_m5866FC6387B25C031B281EB93D0CD541BF6934D2 ();
// 0x0000002B System.Void Destructable::OnDie()
extern void Destructable_OnDie_mEC5CB14267BFFAF5413499FDB228DE7F8AD4D901 ();
// 0x0000002C System.Void Destructable::.ctor()
extern void Destructable__ctor_m1886A052EFD67D1EFC6F56DEA3D68F63C278C8C0 ();
// 0x0000002D UnityEngine.GameObject DetectionModule::get_knownDetectedTarget()
extern void DetectionModule_get_knownDetectedTarget_mF2B3DDC05AA1C5154FC3F32FEBA6CB8DE97E23AD ();
// 0x0000002E System.Void DetectionModule::set_knownDetectedTarget(UnityEngine.GameObject)
extern void DetectionModule_set_knownDetectedTarget_mA07CFCD0C355AE580CF4FE2B66C7C1D4A2CD6831 ();
// 0x0000002F System.Boolean DetectionModule::get_isTargetInAttackRange()
extern void DetectionModule_get_isTargetInAttackRange_m630F202D215C543946E51E216F5DE94B07B00571 ();
// 0x00000030 System.Void DetectionModule::set_isTargetInAttackRange(System.Boolean)
extern void DetectionModule_set_isTargetInAttackRange_mE68C945BEE3EF28DCA74E90DB22C4F8011768EF2 ();
// 0x00000031 System.Boolean DetectionModule::get_isSeeingTarget()
extern void DetectionModule_get_isSeeingTarget_m369B43689EEF7C25F516538704E424348D21B881 ();
// 0x00000032 System.Void DetectionModule::set_isSeeingTarget(System.Boolean)
extern void DetectionModule_set_isSeeingTarget_mD701C5BF6EC183D03859A7C81B95C561028DD3CD ();
// 0x00000033 System.Boolean DetectionModule::get_hadKnownTarget()
extern void DetectionModule_get_hadKnownTarget_mDAC73A0A56401B4BE42EF6024C530E08B18D26DC ();
// 0x00000034 System.Void DetectionModule::set_hadKnownTarget(System.Boolean)
extern void DetectionModule_set_hadKnownTarget_m26EDE8CE72FBF513EED0316CEA2CDBE63CA5114A ();
// 0x00000035 System.Void DetectionModule::Start()
extern void DetectionModule_Start_mC3DA4C42100A44284145E3547B876D75AFBC64C5 ();
// 0x00000036 System.Void DetectionModule::HandleTargetDetection(Actor,UnityEngine.Collider[])
extern void DetectionModule_HandleTargetDetection_m77D4628FCDA27C92A41997A17B0157F94BEE33F6 ();
// 0x00000037 System.Void DetectionModule::OnLostTarget()
extern void DetectionModule_OnLostTarget_mF223DF8489B168C75F5FAC07E685D23DFC1FC725 ();
// 0x00000038 System.Void DetectionModule::OnDetect()
extern void DetectionModule_OnDetect_mF7D947CF03D022722636A3E33642D1EB2965DCA8 ();
// 0x00000039 System.Void DetectionModule::OnDamaged(UnityEngine.GameObject)
extern void DetectionModule_OnDamaged_mD6282F65BCF6D26EDF2A18292C59C168643B16CD ();
// 0x0000003A System.Void DetectionModule::OnAttack()
extern void DetectionModule_OnAttack_m588177C79552DD150A65C84AB1EE04A7DAF55B86 ();
// 0x0000003B System.Void DetectionModule::.ctor()
extern void DetectionModule__ctor_m40CB2DED8B3C7650BE1CB8775107BF242D371718 ();
// 0x0000003C PatrolPath EnemyController::get_patrolPath()
extern void EnemyController_get_patrolPath_m909B79B2E017DD968BB24FB1DCD71E8D94E387C2 ();
// 0x0000003D System.Void EnemyController::set_patrolPath(PatrolPath)
extern void EnemyController_set_patrolPath_m07BD19F5E0DC7807E84301C2F52D661404297BDD ();
// 0x0000003E UnityEngine.GameObject EnemyController::get_knownDetectedTarget()
extern void EnemyController_get_knownDetectedTarget_mFA217FA349A99CD6D6A9762BEDF9D3758BB2D22A ();
// 0x0000003F System.Boolean EnemyController::get_isTargetInAttackRange()
extern void EnemyController_get_isTargetInAttackRange_m254A594C8BD65E49944969A61EA901759D04FEC5 ();
// 0x00000040 System.Boolean EnemyController::get_isSeeingTarget()
extern void EnemyController_get_isSeeingTarget_mA43D131E74DA340E4709F7840A7FF46F95CC131D ();
// 0x00000041 System.Boolean EnemyController::get_hadKnownTarget()
extern void EnemyController_get_hadKnownTarget_m47499D1E886C3B79EFF21D024B741DA34CC71DA5 ();
// 0x00000042 UnityEngine.AI.NavMeshAgent EnemyController::get_m_NavMeshAgent()
extern void EnemyController_get_m_NavMeshAgent_m4EC4520151B335660DDF076EDA24D8EA177A7738 ();
// 0x00000043 System.Void EnemyController::set_m_NavMeshAgent(UnityEngine.AI.NavMeshAgent)
extern void EnemyController_set_m_NavMeshAgent_m1DC3730A0C94361703E417DF57BED8C8A02A2128 ();
// 0x00000044 DetectionModule EnemyController::get_m_DetectionModule()
extern void EnemyController_get_m_DetectionModule_m621A35F8A2C9EBF83EC69E2990EECF682655435C ();
// 0x00000045 System.Void EnemyController::set_m_DetectionModule(DetectionModule)
extern void EnemyController_set_m_DetectionModule_mF57368D3BA35C2AC42A5D78FAC827F2BD3D0C1F0 ();
// 0x00000046 System.Void EnemyController::Start()
extern void EnemyController_Start_m098A57DECB6D20D8A8CF100D55F3814A033BEDD0 ();
// 0x00000047 System.Void EnemyController::Update()
extern void EnemyController_Update_mB42FF5202BE5953ED1E726A86BF4B61F19F8BB3B ();
// 0x00000048 System.Void EnemyController::EnsureIsWithinLevelBounds()
extern void EnemyController_EnsureIsWithinLevelBounds_mA1D24544900E9EE795FCA045A1CC407EC8BA6066 ();
// 0x00000049 System.Void EnemyController::OnLostTarget()
extern void EnemyController_OnLostTarget_mC6FD8B776CFB30E57A468B4761B20026A223F75F ();
// 0x0000004A System.Void EnemyController::OnDetectedTarget()
extern void EnemyController_OnDetectedTarget_m564BA886AD29A59E8DD7AEA1CF6000BE62289BD7 ();
// 0x0000004B System.Void EnemyController::OrientTowards(UnityEngine.Vector3)
extern void EnemyController_OrientTowards_mA278077A975C907560257782794863641CD2DD93 ();
// 0x0000004C System.Boolean EnemyController::IsPathValid()
extern void EnemyController_IsPathValid_mDECE3B0A6087522A1A9DC8608E2D84B987BB331D ();
// 0x0000004D System.Void EnemyController::ResetPathDestination()
extern void EnemyController_ResetPathDestination_m58F8EE6A1B56A4555A2C153A3261BF3EED38EDD2 ();
// 0x0000004E System.Void EnemyController::SetPathDestinationToClosestNode()
extern void EnemyController_SetPathDestinationToClosestNode_m00560583B1D792D9303D3F06F65506DDF7AA9548 ();
// 0x0000004F UnityEngine.Vector3 EnemyController::GetDestinationOnPath()
extern void EnemyController_GetDestinationOnPath_m3DA6DF076D1B2EA57C07CC6CF3B1E830B027BB9A ();
// 0x00000050 System.Void EnemyController::SetNavDestination(UnityEngine.Vector3)
extern void EnemyController_SetNavDestination_mBD072B5C7399FF79BB1ECC744D571C1E5DA07882 ();
// 0x00000051 System.Void EnemyController::UpdatePathDestination(System.Boolean)
extern void EnemyController_UpdatePathDestination_m1D0BABFB9135E76ED421677C699768B67F9E113B ();
// 0x00000052 System.Void EnemyController::OnDamaged(System.Single,UnityEngine.GameObject)
extern void EnemyController_OnDamaged_m8798AEEB37B85FDD2EA8FE4114F4CE347C923164 ();
// 0x00000053 System.Void EnemyController::OnDie()
extern void EnemyController_OnDie_mD35F1B0D183BC082F6B17120E597B131F841560F ();
// 0x00000054 System.Void EnemyController::OnDrawGizmosSelected()
extern void EnemyController_OnDrawGizmosSelected_m943F95E4D683E73C9CF05E5BB1962C3FFCB47AEC ();
// 0x00000055 System.Void EnemyController::OrientWeaponsTowards(UnityEngine.Vector3)
extern void EnemyController_OrientWeaponsTowards_m36F819FED8EFD6FCBCC0CDC2796B67D157DD3F08 ();
// 0x00000056 System.Boolean EnemyController::TryAtack(UnityEngine.Vector3)
extern void EnemyController_TryAtack_m98B341CBFAEC216B70FC60BFCBDCD17E4798BF15 ();
// 0x00000057 System.Boolean EnemyController::TryDropItem()
extern void EnemyController_TryDropItem_mFC1A9B99F6F13887D22CFF98CF52C10DCD771F6E ();
// 0x00000058 System.Void EnemyController::FindAndInitializeAllWeapons()
extern void EnemyController_FindAndInitializeAllWeapons_m64F01A14DF2AC76164D9916D020DD25F1F75E518 ();
// 0x00000059 WeaponController EnemyController::GetCurrentWeapon()
extern void EnemyController_GetCurrentWeapon_m481B337DE02A3733A4CEBDB8BC1B67C98F28BC66 ();
// 0x0000005A System.Void EnemyController::SetCurrentWeapon(System.Int32)
extern void EnemyController_SetCurrentWeapon_mBA8D3019A360A41266B12E11DA88DFEE0E415B06 ();
// 0x0000005B System.Void EnemyController::.ctor()
extern void EnemyController__ctor_m76A2014F0A5962202971DC4401046D10B7A6FF53 ();
// 0x0000005C System.Collections.Generic.List`1<EnemyController> EnemyManager::get_enemies()
extern void EnemyManager_get_enemies_m0E127A53828C2E612368137A2DE38F4E020A4B65 ();
// 0x0000005D System.Void EnemyManager::set_enemies(System.Collections.Generic.List`1<EnemyController>)
extern void EnemyManager_set_enemies_mA9579AF3F9C3B18B3086B904CCA56B104AB941AA ();
// 0x0000005E System.Int32 EnemyManager::get_numberOfEnemiesTotal()
extern void EnemyManager_get_numberOfEnemiesTotal_m83DF528D712FAA92C17DC96CAE8D9B34E3D3AB05 ();
// 0x0000005F System.Void EnemyManager::set_numberOfEnemiesTotal(System.Int32)
extern void EnemyManager_set_numberOfEnemiesTotal_m7F277B5F032F9BD59EFD44C026919C64ED13F9C3 ();
// 0x00000060 System.Int32 EnemyManager::get_numberOfEnemiesRemaining()
extern void EnemyManager_get_numberOfEnemiesRemaining_mB102B1571ED510298AC0E073BDB6A41CD5BE66EC ();
// 0x00000061 System.Void EnemyManager::Awake()
extern void EnemyManager_Awake_mC6B8B081440E593A8B0D41F11FAED5EBB3831C22 ();
// 0x00000062 System.Void EnemyManager::RegisterEnemy(EnemyController)
extern void EnemyManager_RegisterEnemy_m908591B2BD38191AAF4C272289C014B0F1D12035 ();
// 0x00000063 System.Void EnemyManager::UnregisterEnemy(EnemyController)
extern void EnemyManager_UnregisterEnemy_m1EA1D8C765C4402AE3AB6DDC1306FACB321BF5D7 ();
// 0x00000064 System.Void EnemyManager::.ctor()
extern void EnemyManager__ctor_mD78893BD3335D51F1786538ECA10DA63A21B8DF6 ();
// 0x00000065 EnemyMobile_AIState EnemyMobile::get_aiState()
extern void EnemyMobile_get_aiState_m6F80F34D39247A2279B0A315CD2392FBD6CFA308 ();
// 0x00000066 System.Void EnemyMobile::set_aiState(EnemyMobile_AIState)
extern void EnemyMobile_set_aiState_m3B3E15159A94D203E9A86B1ECE64C47490311453 ();
// 0x00000067 System.Void EnemyMobile::Start()
extern void EnemyMobile_Start_mFAE87DFE308852A6B0884C6CBF8D4978B665853A ();
// 0x00000068 System.Void EnemyMobile::Update()
extern void EnemyMobile_Update_mC54FBF20558CB9CC499C11CD3DC5F0BFD19F3E4D ();
// 0x00000069 System.Void EnemyMobile::UpdateAIStateTransitions()
extern void EnemyMobile_UpdateAIStateTransitions_m2122737280C23CCC930E565E29F70ACB3F1A5F38 ();
// 0x0000006A System.Void EnemyMobile::UpdateCurrentAIState()
extern void EnemyMobile_UpdateCurrentAIState_mC204E25FD2E0E7BD7539ED145140957055D2F42B ();
// 0x0000006B System.Void EnemyMobile::OnAttack()
extern void EnemyMobile_OnAttack_m697E4CFC81D90E2CB80450B792436BA90E5216EB ();
// 0x0000006C System.Void EnemyMobile::OnDetectedTarget()
extern void EnemyMobile_OnDetectedTarget_m66348B0387FC7BEC9FAC7A9F1D37718387C24BE4 ();
// 0x0000006D System.Void EnemyMobile::OnLostTarget()
extern void EnemyMobile_OnLostTarget_mBA0F28FF884637B3CD0B78E96A441FA9AA50C259 ();
// 0x0000006E System.Void EnemyMobile::OnDamaged()
extern void EnemyMobile_OnDamaged_m018F6EE8A9B94FB5C31C797E2432DC89EE20EDB2 ();
// 0x0000006F System.Void EnemyMobile::.ctor()
extern void EnemyMobile__ctor_m14A3C627C27DEE38D29B4E2742DA2497634F5D29 ();
// 0x00000070 EnemyTurret_AIState EnemyTurret::get_aiState()
extern void EnemyTurret_get_aiState_m42C1E86DB3AFB8E1A45F9BD0144B9CCAE467ED6D ();
// 0x00000071 System.Void EnemyTurret::set_aiState(EnemyTurret_AIState)
extern void EnemyTurret_set_aiState_m4E356EEFD9B7367279638FA9231360BBA720C9E8 ();
// 0x00000072 System.Void EnemyTurret::Start()
extern void EnemyTurret_Start_m61B0CB62515DD8CED6811C31E7935FE763D3F317 ();
// 0x00000073 System.Void EnemyTurret::Update()
extern void EnemyTurret_Update_m08E030C70DA708876306FD37068D886A855F0E94 ();
// 0x00000074 System.Void EnemyTurret::LateUpdate()
extern void EnemyTurret_LateUpdate_mF36BB07AF83793703F43FDC74422E851E5FD8FCB ();
// 0x00000075 System.Void EnemyTurret::UpdateCurrentAIState()
extern void EnemyTurret_UpdateCurrentAIState_mAB6D5F0791200B0875B05965F415C0223E344E87 ();
// 0x00000076 System.Void EnemyTurret::UpdateTurretAiming()
extern void EnemyTurret_UpdateTurretAiming_mCFCE236E812C81900199C0305FC34D941C7AD95E ();
// 0x00000077 System.Void EnemyTurret::OnDamaged(System.Single,UnityEngine.GameObject)
extern void EnemyTurret_OnDamaged_mF0ADA8D3C35254788566CD109331E7807E1AC1E3 ();
// 0x00000078 System.Void EnemyTurret::OnDetectedTarget()
extern void EnemyTurret_OnDetectedTarget_m74245D20A910E9BCBA93F9FD4E8502F7335FD1CC ();
// 0x00000079 System.Void EnemyTurret::OnLostTarget()
extern void EnemyTurret_OnLostTarget_mAF1742D8E0B3E7712E7E21965057C35DE745CED4 ();
// 0x0000007A System.Void EnemyTurret::.ctor()
extern void EnemyTurret__ctor_m2BF67D445F9E1DE50E241F49612E7A83A1C3086E ();
// 0x0000007B System.Void FollowPlayer::Start()
extern void FollowPlayer_Start_m3D4944B71D74CF5E99F5032DC85E078F16A977C9 ();
// 0x0000007C System.Void FollowPlayer::LateUpdate()
extern void FollowPlayer_LateUpdate_m2EA9433E8DEDB1E26FDB5C57566B5B97CAAEB02D ();
// 0x0000007D System.Void FollowPlayer::.ctor()
extern void FollowPlayer__ctor_m7A7391BDBACA4FB475491CC3B13D12271C5AE761 ();
// 0x0000007E System.Void FramerateCounter::Update()
extern void FramerateCounter_Update_m5D0E02ADFD513D4178242B0FF1606850D9CA23E8 ();
// 0x0000007F System.Void FramerateCounter::.ctor()
extern void FramerateCounter__ctor_m1F212E7C7383024342DF149A461021C0917ED1CC ();
// 0x00000080 System.Void GameConstants::.ctor()
extern void GameConstants__ctor_m111888F48CBAD817DDB0297AE242A7CD73D10AE9 ();
// 0x00000081 System.Boolean GameFlowManager::get_gameIsEnding()
extern void GameFlowManager_get_gameIsEnding_m5703547651BFFD4660C06237D549F930C28ABFB0 ();
// 0x00000082 System.Void GameFlowManager::set_gameIsEnding(System.Boolean)
extern void GameFlowManager_set_gameIsEnding_mD3583C1B9E56794FA279A2D66FFAB1729285A5A9 ();
// 0x00000083 System.Void GameFlowManager::Start()
extern void GameFlowManager_Start_mD63A93BF2772FEC818E391AFAE9FAFED6DEE2FB0 ();
// 0x00000084 System.Void GameFlowManager::Update()
extern void GameFlowManager_Update_mC637F36E7F04FA30095BAA2CAEFDBE4A986FB5D8 ();
// 0x00000085 System.Void GameFlowManager::EndGame(System.Boolean)
extern void GameFlowManager_EndGame_mDC0A6BCBBB4072D37AF58488DB3BB5A18B8F9428 ();
// 0x00000086 System.Void GameFlowManager::.ctor()
extern void GameFlowManager__ctor_m71A7B0808984589B8930E24D5D469603EC7C6602 ();
// 0x00000087 System.Single Health::get_currentHealth()
extern void Health_get_currentHealth_m3702D4E7A03A5153B3383738CC91CECC14EA5D47 ();
// 0x00000088 System.Void Health::set_currentHealth(System.Single)
extern void Health_set_currentHealth_m2456633D32E1137A8CB406E55EA4F2431EC8C1A6 ();
// 0x00000089 System.Boolean Health::get_invincible()
extern void Health_get_invincible_mC2C333A7ED3093772E8B33CACE19F794B247DC11 ();
// 0x0000008A System.Void Health::set_invincible(System.Boolean)
extern void Health_set_invincible_mB2FD644FB1CAE9AFE2F39A43BEE11FDAEA7775D8 ();
// 0x0000008B System.Boolean Health::canPickup()
extern void Health_canPickup_m256CDDD6ABD420D847AB444A83571FD02D98F27A ();
// 0x0000008C System.Single Health::getRatio()
extern void Health_getRatio_mBE1E48477D95E2783463176157E743D791228B24 ();
// 0x0000008D System.Boolean Health::isCritical()
extern void Health_isCritical_m800B41C15A66973EFA2982E978BF8EE8214D51FB ();
// 0x0000008E System.Void Health::Start()
extern void Health_Start_mB4EBE77597318321F1F745E9A192DCF0CE601A98 ();
// 0x0000008F System.Void Health::Heal(System.Single)
extern void Health_Heal_mCE7E1A95E560F7DC122AF651ECAAD10456E6C445 ();
// 0x00000090 System.Void Health::TakeDamage(System.Single,UnityEngine.GameObject)
extern void Health_TakeDamage_mC6EB544A26405EB731E7207623C34D909ADE217D ();
// 0x00000091 System.Void Health::Kill()
extern void Health_Kill_m2CB79E658731C38B96347639D0C5B7302604F086 ();
// 0x00000092 System.Void Health::HandleDeath()
extern void Health_HandleDeath_m1BB2061FBD1104DB8A858546C2D50265E7C59F47 ();
// 0x00000093 System.Void Health::.ctor()
extern void Health__ctor_m5EEFBF1CB89A5255DD6DF527EF268C351F56FB16 ();
// 0x00000094 System.Void HealthPickup::Start()
extern void HealthPickup_Start_m3F86BDD65577D5C98907AA8BA8A7F014F02A3DCE ();
// 0x00000095 System.Void HealthPickup::OnPicked(PlayerCharacterController)
extern void HealthPickup_OnPicked_mBDDEB3BE08BAB6AB37A3620F587D48DE27E704A4 ();
// 0x00000096 System.Void HealthPickup::.ctor()
extern void HealthPickup__ctor_m6AFD4A1A60F44EFA2FA80451FE6951C009B9C2A6 ();
// 0x00000097 System.Void IgnoreHeatMap::.ctor()
extern void IgnoreHeatMap__ctor_m228187BA199FA9A02A8EF339339D3612F3728D80 ();
// 0x00000098 System.Void IgnoreHitDetection::.ctor()
extern void IgnoreHitDetection__ctor_mE454D2F2FE4CFD0D272C47295A1861EF4AED8B65 ();
// 0x00000099 System.Void InGameMenuManager::Start()
extern void InGameMenuManager_Start_mC56B90D83BA53A972D97708806E46A585C6A4FD8 ();
// 0x0000009A System.Void InGameMenuManager::Update()
extern void InGameMenuManager_Update_m38CEC8995AF54965E40EE63815FBC85F39855067 ();
// 0x0000009B System.Void InGameMenuManager::ClosePauseMenu()
extern void InGameMenuManager_ClosePauseMenu_m0F793A3FEE4274D7EE6F232B731C0CFC7B2F1EBA ();
// 0x0000009C System.Void InGameMenuManager::SetPauseMenuActivation(System.Boolean)
extern void InGameMenuManager_SetPauseMenuActivation_mF96E328C9D70F2964A2BF76536BB3DEF3307ADA8 ();
// 0x0000009D System.Void InGameMenuManager::OnMouseSensitivityChanged(System.Single)
extern void InGameMenuManager_OnMouseSensitivityChanged_mD3353F140F19E7E8EFD9CF917E88C4922406C2B1 ();
// 0x0000009E System.Void InGameMenuManager::OnShadowsChanged(System.Boolean)
extern void InGameMenuManager_OnShadowsChanged_mC17AEF26AFB653EB8DC3076BFEEA0FBC468F4708 ();
// 0x0000009F System.Void InGameMenuManager::OnInvincibilityChanged(System.Boolean)
extern void InGameMenuManager_OnInvincibilityChanged_m6C049E2A1CCDD89C8307FF5CEA6C5C39CC89E3EB ();
// 0x000000A0 System.Void InGameMenuManager::OnFramerateCounterChanged(System.Boolean)
extern void InGameMenuManager_OnFramerateCounterChanged_m1B6E22A542D5111975F24848AEFEDF91215EE2FB ();
// 0x000000A1 System.Void InGameMenuManager::OnShowControlButtonClicked(System.Boolean)
extern void InGameMenuManager_OnShowControlButtonClicked_m6F832C99C77C4B745B2693D1E4322E0B47E0B3A1 ();
// 0x000000A2 System.Void InGameMenuManager::.ctor()
extern void InGameMenuManager__ctor_m01917B747BF82159104B329F15E5F9AC77B37A49 ();
// 0x000000A3 System.Single Jetpack::get_currentFillRatio()
extern void Jetpack_get_currentFillRatio_m62AF7D1177839391A8EF63A97F9C2297FDF2B9BA ();
// 0x000000A4 System.Void Jetpack::set_currentFillRatio(System.Single)
extern void Jetpack_set_currentFillRatio_m5C250952557C4D6EC1EB75CA129371CC14AB9913 ();
// 0x000000A5 System.Boolean Jetpack::get_isJetpackUnlocked()
extern void Jetpack_get_isJetpackUnlocked_mC67FFE3AE7221A4F00B30E5A976BEB927AAA7248 ();
// 0x000000A6 System.Void Jetpack::set_isJetpackUnlocked(System.Boolean)
extern void Jetpack_set_isJetpackUnlocked_mBE60918EF56E76640EE231A2BBBC1877FF509059 ();
// 0x000000A7 System.Boolean Jetpack::isPlayergrounded()
extern void Jetpack_isPlayergrounded_m4887B7B653E4B0DD6B16C07DEC2D7B04240E48F9 ();
// 0x000000A8 System.Void Jetpack::Start()
extern void Jetpack_Start_mAF11D818D0C79460574487D6E66652F8DE2436AE ();
// 0x000000A9 System.Void Jetpack::Update()
extern void Jetpack_Update_mF2CCB7F781FC00DF70C42571A059E9A7A43A0AAA ();
// 0x000000AA System.Boolean Jetpack::TryUnlock()
extern void Jetpack_TryUnlock_m5D8F5AA374FDD0DC9A9597926316688ABF6DC81F ();
// 0x000000AB System.Void Jetpack::.ctor()
extern void Jetpack__ctor_m91E09FFF1621AE20F6D935B75A22A31EEC92F9EE ();
// 0x000000AC System.Void JetpackPickup::Start()
extern void JetpackPickup_Start_m083DF2E95F0E9CB6912B2FE9636FA9084ADF8C29 ();
// 0x000000AD System.Void JetpackPickup::OnPicked(PlayerCharacterController)
extern void JetpackPickup_OnPicked_mD04E1001E8DD4C30BC530D87B2362A97AA459011 ();
// 0x000000AE System.Void JetpackPickup::.ctor()
extern void JetpackPickup__ctor_m5FC8702833D54EDECBD7116EA06D3453EC666616 ();
// 0x000000AF System.Void MeshCombineUtility::Combine(System.Collections.Generic.List`1<UnityEngine.MeshRenderer>,MeshCombineUtility_RendererDisposeMethod,System.String)
extern void MeshCombineUtility_Combine_m5B6AA5A11B6E0A664670C5B17F9BEA4C21A1E66A ();
// 0x000000B0 System.Int32 MeshCombineUtility::GetExistingRenderBatch(System.Collections.Generic.List`1<MeshCombineUtility_RenderBatchData>,UnityEngine.Material,UnityEngine.MeshRenderer,System.Int32)
extern void MeshCombineUtility_GetExistingRenderBatch_m1614EEE1B467DE249A8605F3A58ECBEDA7251D02 ();
// 0x000000B1 System.Void MeshCombiner::Start()
extern void MeshCombiner_Start_mEF6C88CF06CB60750580B21FA0AAE7DF3708CBB4 ();
// 0x000000B2 System.Void MeshCombiner::Combine()
extern void MeshCombiner_Combine_m2D8105FC0E01A0C3ACB59A88CB8383F690DEC980 ();
// 0x000000B3 System.Void MeshCombiner::CombineAllInBounds(UnityEngine.Bounds,System.Collections.Generic.List`1<UnityEngine.MeshRenderer>)
extern void MeshCombiner_CombineAllInBounds_m69A238068B90D55CE16FE2512C03535F00DE9083 ();
// 0x000000B4 System.Int32 MeshCombiner::GetGridCellCount()
extern void MeshCombiner_GetGridCellCount_mC2C7424C04EFD97BBBAFC0BBFA189DCF3E8D538F ();
// 0x000000B5 System.Boolean MeshCombiner::GetGridCellBounds(System.Int32,UnityEngine.Bounds&)
extern void MeshCombiner_GetGridCellBounds_m62508E4B7CD21D7540EDD29550F3AE7B8D3BD829 ();
// 0x000000B6 System.Void MeshCombiner::OnDrawGizmosSelected()
extern void MeshCombiner_OnDrawGizmosSelected_m3DDB558F637D0968F7FE9FEE948423C135B3D695 ();
// 0x000000B7 System.Void MeshCombiner::.ctor()
extern void MeshCombiner__ctor_mC781E88512B38203ADF37968802F45875C259DC1 ();
// 0x000000B8 System.Single MinMaxFloat::GetValueFromRatio(System.Single)
extern void MinMaxFloat_GetValueFromRatio_mD97564073C90866765E36F13D6460E7A7D4033D5_AdjustorThunk ();
// 0x000000B9 UnityEngine.Color MinMaxColor::GetValueFromRatio(System.Single)
extern void MinMaxColor_GetValueFromRatio_mF84EF5E0C91CB9DF8393B4F3B6DA980CF614B718_AdjustorThunk ();
// 0x000000BA UnityEngine.Vector3 MinMaxVector3::GetValueFromRatio(System.Single)
extern void MinMaxVector3_GetValueFromRatio_mE44BE0E31AE7500B08D7C486E9CA4B3858F37F87_AdjustorThunk ();
// 0x000000BB System.Void NavigationModule::.ctor()
extern void NavigationModule__ctor_m8E1F4150B0049E7B5B7F67C723B5EB62BF531EA1 ();
// 0x000000BC System.Boolean Objective::get_isCompleted()
extern void Objective_get_isCompleted_m3EAA117894FE7828C6B1645C3BE836D20F2F7D3B ();
// 0x000000BD System.Void Objective::set_isCompleted(System.Boolean)
extern void Objective_set_isCompleted_m7120EFA189D7A6DDE7AC8DAA7E57582226B0F7EF ();
// 0x000000BE System.Boolean Objective::isBlocking()
extern void Objective_isBlocking_m0F7B9F234C0FCDDB740A4723532899EFE7C547FB ();
// 0x000000BF System.Void Objective::Start()
extern void Objective_Start_m7B028AD27102FDBD7B17E1FC8226861C762C9BD9 ();
// 0x000000C0 System.Void Objective::UpdateObjective(System.String,System.String,System.String)
extern void Objective_UpdateObjective_mD7DC674113C559AEFE74C1AC7BB6398372D89B94 ();
// 0x000000C1 System.Void Objective::CompleteObjective(System.String,System.String,System.String)
extern void Objective_CompleteObjective_mB0EAC46C472144FAC89C8B210358CD71FD3B1990 ();
// 0x000000C2 System.Void Objective::.ctor()
extern void Objective__ctor_m9BE24CDE8FFB146C50CD147DCF92A5F99A40E0C8 ();
// 0x000000C3 System.Void UnityActionUpdateObjective::.ctor(Objective,System.String,System.String,System.Boolean,System.String)
extern void UnityActionUpdateObjective__ctor_mBF121C52844E8BFA76A7AA31D79005E852B20162 ();
// 0x000000C4 System.Void ObjectiveKillEnemies::Start()
extern void ObjectiveKillEnemies_Start_mC415D80A91693A8CC45C54D510E7B54B866136B8 ();
// 0x000000C5 System.Void ObjectiveKillEnemies::OnKillEnemy(EnemyController,System.Int32)
extern void ObjectiveKillEnemies_OnKillEnemy_mF0BE43F964977823F1D4B27B507339A92E3C2137 ();
// 0x000000C6 System.String ObjectiveKillEnemies::GetUpdatedCounterAmount()
extern void ObjectiveKillEnemies_GetUpdatedCounterAmount_mA3D5DAF1A61235F436399B43359369CFCF286771 ();
// 0x000000C7 System.Void ObjectiveKillEnemies::.ctor()
extern void ObjectiveKillEnemies__ctor_mE920BE26CABFEA648857E1E08C7DB55DC0D3470D ();
// 0x000000C8 System.Boolean ObjectiveManager::AreAllObjectivesCompleted()
extern void ObjectiveManager_AreAllObjectivesCompleted_m12749CB1409F4608579E2CC3196B21ADC325D7A6 ();
// 0x000000C9 System.Void ObjectiveManager::RegisterObjective(Objective)
extern void ObjectiveManager_RegisterObjective_m902D3BDB9EAF0D7A016C9E116B80BF788ED52A7E ();
// 0x000000CA System.Void ObjectiveManager::.ctor()
extern void ObjectiveManager__ctor_m09D7C5CFC9760D1BB93B3F01278F16C94D6BFB9F ();
// 0x000000CB System.Void ObjectivePickupItem::Awake()
extern void ObjectivePickupItem_Awake_mCBAF859FDBDDF46A8EA1B20CAABCFB799018E408 ();
// 0x000000CC System.Void ObjectivePickupItem::OnPickup(PlayerCharacterController)
extern void ObjectivePickupItem_OnPickup_m5764D4C775E5C82A1926A4F5AEF318D11A0EB21A ();
// 0x000000CD System.Void ObjectivePickupItem::.ctor()
extern void ObjectivePickupItem__ctor_m5C725C3B268E18248727752CA17CDA901340F221 ();
// 0x000000CE System.Void ObjectiveReachPoint::Awake()
extern void ObjectiveReachPoint_Awake_m6CEDBCD4390656772D4FB563074D32CC2C41FB7D ();
// 0x000000CF System.Void ObjectiveReachPoint::OnTriggerEnter(UnityEngine.Collider)
extern void ObjectiveReachPoint_OnTriggerEnter_m89F95D81CAB2B50052C8508A0870B77E4BE1DE1D ();
// 0x000000D0 System.Void ObjectiveReachPoint::.ctor()
extern void ObjectiveReachPoint__ctor_m70C2818350FFCFF4F98FE3E2977B4A7B7EEEB03D ();
// 0x000000D1 System.Void OverheatBehavior::Awake()
extern void OverheatBehavior_Awake_m88CC1B12038FA1ECDD4374CE18F3ACDE70E4D31B ();
// 0x000000D2 System.Void OverheatBehavior::Update()
extern void OverheatBehavior_Update_m02941BE81C1530D1849538F1FD0B7E7541BF1DCD ();
// 0x000000D3 System.Void OverheatBehavior::.ctor()
extern void OverheatBehavior__ctor_m222606A153EC33AEE969E20E2610BC7C0954926F ();
// 0x000000D4 System.Void PatrolPath::Start()
extern void PatrolPath_Start_m5C0E90ED1C3607FE3174D05BE84DB6F1C1FAD771 ();
// 0x000000D5 System.Single PatrolPath::GetDistanceToNode(UnityEngine.Vector3,System.Int32)
extern void PatrolPath_GetDistanceToNode_m4D4D361354219A81A827B5E47BC3D4277F1A77B0 ();
// 0x000000D6 UnityEngine.Vector3 PatrolPath::GetPositionOfPathNode(System.Int32)
extern void PatrolPath_GetPositionOfPathNode_mE20A3C9887678E96D314BBCB935138F7FFF55FB1 ();
// 0x000000D7 System.Void PatrolPath::OnDrawGizmosSelected()
extern void PatrolPath_OnDrawGizmosSelected_mE8EE2932EE3CB1FF9036E8BF4D496FD3FC14C523 ();
// 0x000000D8 System.Void PatrolPath::.ctor()
extern void PatrolPath__ctor_m4958B22E3F594B6698BDA3C302BAF84DE8AC720A ();
// 0x000000D9 UnityEngine.Rigidbody Pickup::get_pickupRigidbody()
extern void Pickup_get_pickupRigidbody_mF7725C48939A25BD25AA4809D55EE7B5EC880BB6 ();
// 0x000000DA System.Void Pickup::set_pickupRigidbody(UnityEngine.Rigidbody)
extern void Pickup_set_pickupRigidbody_m82B725BE80A6565FE573D445CF899B73029F5276 ();
// 0x000000DB System.Void Pickup::Start()
extern void Pickup_Start_m486A5E3031CC34D41A9A087669D367AE14B129E7 ();
// 0x000000DC System.Void Pickup::Update()
extern void Pickup_Update_mAF9F8DE49E77332FB0F423A9F1554F2194FE2C17 ();
// 0x000000DD System.Void Pickup::OnTriggerEnter(UnityEngine.Collider)
extern void Pickup_OnTriggerEnter_m65D9DAB9B1519B0EABA8DF5799B4B6932C39D111 ();
// 0x000000DE System.Void Pickup::PlayPickupFeedback()
extern void Pickup_PlayPickupFeedback_mD48A3AE0650E0A46BE9490336496B59741B0F1FC ();
// 0x000000DF System.Void Pickup::.ctor()
extern void Pickup__ctor_mD95E5208BC0FF094E8BFA21084232C03DA22AEA8 ();
// 0x000000E0 UnityEngine.Vector3 PlayerCharacterController::get_characterVelocity()
extern void PlayerCharacterController_get_characterVelocity_m8663C69E75E5D8FC4DF1DCEAA5989102FCC5FB14 ();
// 0x000000E1 System.Void PlayerCharacterController::set_characterVelocity(UnityEngine.Vector3)
extern void PlayerCharacterController_set_characterVelocity_m72065DCE98A9E5C5566BCC9F81E1ECF03017451D ();
// 0x000000E2 System.Boolean PlayerCharacterController::get_isGrounded()
extern void PlayerCharacterController_get_isGrounded_m9F0527F2983A16F97837EAE53359783DE1325E55 ();
// 0x000000E3 System.Void PlayerCharacterController::set_isGrounded(System.Boolean)
extern void PlayerCharacterController_set_isGrounded_m86D0907A844B9131C0430C55CCFD906A6401FBA1 ();
// 0x000000E4 System.Boolean PlayerCharacterController::get_hasJumpedThisFrame()
extern void PlayerCharacterController_get_hasJumpedThisFrame_m61C38C4DC49640CE60C1E2B969691D715FC577B9 ();
// 0x000000E5 System.Void PlayerCharacterController::set_hasJumpedThisFrame(System.Boolean)
extern void PlayerCharacterController_set_hasJumpedThisFrame_m70F6273684C020E6A3D2B980C526CEEA9349F14E ();
// 0x000000E6 System.Boolean PlayerCharacterController::get_isDead()
extern void PlayerCharacterController_get_isDead_m4207FC2D3647D4322683B7C8534E8622AA6B409B ();
// 0x000000E7 System.Void PlayerCharacterController::set_isDead(System.Boolean)
extern void PlayerCharacterController_set_isDead_m3BFF6065B90479C6E15D45CEE5B3B2E1D2E99385 ();
// 0x000000E8 System.Boolean PlayerCharacterController::get_isCrouching()
extern void PlayerCharacterController_get_isCrouching_m644FA07DD2228B9A45DCBDA0404C99D220696C22 ();
// 0x000000E9 System.Void PlayerCharacterController::set_isCrouching(System.Boolean)
extern void PlayerCharacterController_set_isCrouching_m82743293FAF8695BBFF43C5539C2D9BE84BD5B57 ();
// 0x000000EA System.Single PlayerCharacterController::get_RotationMultiplier()
extern void PlayerCharacterController_get_RotationMultiplier_m2AA73E497CD0D0A094A4188EB2AA56F18A8F6B0C ();
// 0x000000EB System.Void PlayerCharacterController::Start()
extern void PlayerCharacterController_Start_m72E382538592CD782BF9F09FE29CD59B40403776 ();
// 0x000000EC System.Void PlayerCharacterController::Update()
extern void PlayerCharacterController_Update_mAB277B45C69CED773619CCF8FEF3C0D2FB6B0917 ();
// 0x000000ED System.Void PlayerCharacterController::OnDie()
extern void PlayerCharacterController_OnDie_m8171C9B0CB00F32491CB2852C0E88B6452A1B329 ();
// 0x000000EE System.Void PlayerCharacterController::GroundCheck()
extern void PlayerCharacterController_GroundCheck_m66295DEFEADA7AA10A74648C343411FD86CD3281 ();
// 0x000000EF System.Void PlayerCharacterController::HandleCharacterMovement()
extern void PlayerCharacterController_HandleCharacterMovement_m48CF8C50B0BD075BA617BB8DEAD258C6E58EE596 ();
// 0x000000F0 System.Boolean PlayerCharacterController::IsNormalUnderSlopeLimit(UnityEngine.Vector3)
extern void PlayerCharacterController_IsNormalUnderSlopeLimit_mA6F88524DAA67463EFB4E06DBFB64E4FEF5F5CF7 ();
// 0x000000F1 UnityEngine.Vector3 PlayerCharacterController::GetCapsuleBottomHemisphere()
extern void PlayerCharacterController_GetCapsuleBottomHemisphere_m33199FF9B6683A6C05FBE76C4012390342E2C5C3 ();
// 0x000000F2 UnityEngine.Vector3 PlayerCharacterController::GetCapsuleTopHemisphere(System.Single)
extern void PlayerCharacterController_GetCapsuleTopHemisphere_mEC47D28A96A7F28256777721596FE5F5AB4AED5C ();
// 0x000000F3 UnityEngine.Vector3 PlayerCharacterController::GetDirectionReorientedOnSlope(UnityEngine.Vector3,UnityEngine.Vector3)
extern void PlayerCharacterController_GetDirectionReorientedOnSlope_m2BE0B880C7BBDE4D6D57B651B23C24C291F92FA5 ();
// 0x000000F4 System.Void PlayerCharacterController::UpdateCharacterHeight(System.Boolean)
extern void PlayerCharacterController_UpdateCharacterHeight_m4169751150D07082AFB2B68D7EBEBB454559F589 ();
// 0x000000F5 System.Boolean PlayerCharacterController::SetCrouchingState(System.Boolean,System.Boolean)
extern void PlayerCharacterController_SetCrouchingState_m17F7E0AC222AC65ADE978612F4DCB5776DF0EA40 ();
// 0x000000F6 System.Void PlayerCharacterController::.ctor()
extern void PlayerCharacterController__ctor_m020FB80AEE5269F9B4DD50F14C1013FF5290F549 ();
// 0x000000F7 System.Void PlayerInputHandler::Start()
extern void PlayerInputHandler_Start_mB37ADFEFE34779CE0DA3264EAE85AB6810C036EA ();
// 0x000000F8 System.Void PlayerInputHandler::LateUpdate()
extern void PlayerInputHandler_LateUpdate_mC262DB22852296FB69887E85DB088679C46B47E0 ();
// 0x000000F9 System.Boolean PlayerInputHandler::CanProcessInput()
extern void PlayerInputHandler_CanProcessInput_m71D2D4DF35B349B352E4883514C4AD3C3E5ECD06 ();
// 0x000000FA UnityEngine.Vector3 PlayerInputHandler::GetMoveInput()
extern void PlayerInputHandler_GetMoveInput_m0874907ACDB98A02D6D5AD827D22507AD7A0A4AC ();
// 0x000000FB System.Single PlayerInputHandler::GetLookInputsHorizontal()
extern void PlayerInputHandler_GetLookInputsHorizontal_mC286BDDC5BB63A7121162B81BAFAD2556F404EEF ();
// 0x000000FC System.Single PlayerInputHandler::GetLookInputsVertical()
extern void PlayerInputHandler_GetLookInputsVertical_m8B79693D83813662238A44DA2F4A070B1C00EF6B ();
// 0x000000FD System.Boolean PlayerInputHandler::GetJumpInputDown()
extern void PlayerInputHandler_GetJumpInputDown_m1D3389B9ED931B44BA34C0604E1AB1CB3DC7B8CD ();
// 0x000000FE System.Boolean PlayerInputHandler::GetJumpInputHeld()
extern void PlayerInputHandler_GetJumpInputHeld_m0822079C30EA025CCC1DA6E919589E6836FC9C65 ();
// 0x000000FF System.Boolean PlayerInputHandler::GetFireInputDown()
extern void PlayerInputHandler_GetFireInputDown_m3AA7DA6E4C4E33A2924EEABFD75E49327135BBEE ();
// 0x00000100 System.Boolean PlayerInputHandler::GetFireInputReleased()
extern void PlayerInputHandler_GetFireInputReleased_mDF51AD29D7341784CA0C7117215B8D7144504D1B ();
// 0x00000101 System.Boolean PlayerInputHandler::GetFireInputHeld()
extern void PlayerInputHandler_GetFireInputHeld_mE7D176D4FD0E011E69BC6FA83C0C5B144421B5CB ();
// 0x00000102 System.Boolean PlayerInputHandler::GetAimInputHeld()
extern void PlayerInputHandler_GetAimInputHeld_m67265974BFEA58BF3C4F10D108D3FEE0CBD78737 ();
// 0x00000103 System.Boolean PlayerInputHandler::GetSprintInputHeld()
extern void PlayerInputHandler_GetSprintInputHeld_mB06B18A97B10D9774E6DE58A819AD79DF8C3B4A7 ();
// 0x00000104 System.Boolean PlayerInputHandler::GetCrouchInputDown()
extern void PlayerInputHandler_GetCrouchInputDown_m581075CAF043220C6D9F08CEC3ECF1A885A92174 ();
// 0x00000105 System.Boolean PlayerInputHandler::GetCrouchInputReleased()
extern void PlayerInputHandler_GetCrouchInputReleased_m5FAB8C928AACDC2755AD57DE723315194E02D1FD ();
// 0x00000106 System.Int32 PlayerInputHandler::GetSwitchWeaponInput()
extern void PlayerInputHandler_GetSwitchWeaponInput_mB5F957A1844B7E2A4DD132024D11005FBB1574C6 ();
// 0x00000107 System.Int32 PlayerInputHandler::GetSelectWeaponInput()
extern void PlayerInputHandler_GetSelectWeaponInput_mC9144A22F02E66E7789C1A76BED19D0C683FEB85 ();
// 0x00000108 System.Single PlayerInputHandler::GetMouseOrStickLookAxis(System.String,System.String)
extern void PlayerInputHandler_GetMouseOrStickLookAxis_mF5AB346B44FBBC610380D37B0D3AC0DD06415B62 ();
// 0x00000109 System.Void PlayerInputHandler::.ctor()
extern void PlayerInputHandler__ctor_m57C20066B300D46827395B6EF8C0BBF16963FFF9 ();
// 0x0000010A System.Boolean PlayerWeaponsManager::get_isAiming()
extern void PlayerWeaponsManager_get_isAiming_mFB413FD2D4DA949F99AA02B1CEDFBCD143285142 ();
// 0x0000010B System.Void PlayerWeaponsManager::set_isAiming(System.Boolean)
extern void PlayerWeaponsManager_set_isAiming_m98566CFF8314A968317F8A8A5D5AB04264EF948A ();
// 0x0000010C System.Boolean PlayerWeaponsManager::get_isPointingAtEnemy()
extern void PlayerWeaponsManager_get_isPointingAtEnemy_m3A1DA9E73D8926D01C60DF64C84D628D1219F057 ();
// 0x0000010D System.Void PlayerWeaponsManager::set_isPointingAtEnemy(System.Boolean)
extern void PlayerWeaponsManager_set_isPointingAtEnemy_m65F60C3E6B8843368F52DA0230F892AE4B58E6F8 ();
// 0x0000010E System.Int32 PlayerWeaponsManager::get_activeWeaponIndex()
extern void PlayerWeaponsManager_get_activeWeaponIndex_mC60445132555E8ABA60E7B70B18C05FC15426764 ();
// 0x0000010F System.Void PlayerWeaponsManager::set_activeWeaponIndex(System.Int32)
extern void PlayerWeaponsManager_set_activeWeaponIndex_m26B271D865F2D37982BA1A39B159D248F08222A9 ();
// 0x00000110 System.Void PlayerWeaponsManager::Start()
extern void PlayerWeaponsManager_Start_mE7E930BF3D43B68CFD1D11FCB7149C14EE0D09F0 ();
// 0x00000111 System.Void PlayerWeaponsManager::Update()
extern void PlayerWeaponsManager_Update_m0591AC8C2E84B00421EEB35EF90698A244C9D79F ();
// 0x00000112 System.Void PlayerWeaponsManager::LateUpdate()
extern void PlayerWeaponsManager_LateUpdate_m942B337CD3E02A3ACE5D029AAA7DB982AC669676 ();
// 0x00000113 System.Void PlayerWeaponsManager::SetFOV(System.Single)
extern void PlayerWeaponsManager_SetFOV_m59C9A88B42112BBCF600641E5F7E29EE75073979 ();
// 0x00000114 System.Void PlayerWeaponsManager::SwitchWeapon(System.Boolean)
extern void PlayerWeaponsManager_SwitchWeapon_m08848BFC67A6310BEEC3D2717525745A25997DA2 ();
// 0x00000115 System.Void PlayerWeaponsManager::SwitchToWeaponIndex(System.Int32,System.Boolean)
extern void PlayerWeaponsManager_SwitchToWeaponIndex_m90E8F7B77D6A5B4F22C30EE1B0A21EA70541218C ();
// 0x00000116 System.Boolean PlayerWeaponsManager::HasWeapon(WeaponController)
extern void PlayerWeaponsManager_HasWeapon_mC35C97D29DDCFD27EEFE9E6694D407AD6BD2CE50 ();
// 0x00000117 System.Void PlayerWeaponsManager::UpdateWeaponAiming()
extern void PlayerWeaponsManager_UpdateWeaponAiming_mD6D59E18110591B41DDB05D7D5B0E4ECFE66E2FD ();
// 0x00000118 System.Void PlayerWeaponsManager::UpdateWeaponBob()
extern void PlayerWeaponsManager_UpdateWeaponBob_mA595B2F9A5A50ED62610D551A3DF9985D514A642 ();
// 0x00000119 System.Void PlayerWeaponsManager::UpdateWeaponRecoil()
extern void PlayerWeaponsManager_UpdateWeaponRecoil_m03D1298F23D64A1EAEF9A818410AB77B20B985E0 ();
// 0x0000011A System.Void PlayerWeaponsManager::UpdateWeaponSwitching()
extern void PlayerWeaponsManager_UpdateWeaponSwitching_mE3328068C922D4F6D4B357979C5DD3E74BA8B9A4 ();
// 0x0000011B System.Boolean PlayerWeaponsManager::AddWeapon(WeaponController)
extern void PlayerWeaponsManager_AddWeapon_m6687BD9B7E34BB41035B0CB80BD14EF2488B5761 ();
// 0x0000011C System.Boolean PlayerWeaponsManager::RemoveWeapon(WeaponController)
extern void PlayerWeaponsManager_RemoveWeapon_m21B2D70F94961010CBBF7D06EFAB733B43E3FCF3 ();
// 0x0000011D WeaponController PlayerWeaponsManager::GetActiveWeapon()
extern void PlayerWeaponsManager_GetActiveWeapon_m2E9EF7BAE1D06DB5CCC560272A3FC7754FBC3ABD ();
// 0x0000011E WeaponController PlayerWeaponsManager::GetWeaponAtSlotIndex(System.Int32)
extern void PlayerWeaponsManager_GetWeaponAtSlotIndex_mA77611532B28181F22CECA69C018070F3E01C43E ();
// 0x0000011F System.Int32 PlayerWeaponsManager::GetDistanceBetweenWeaponSlots(System.Int32,System.Int32,System.Boolean)
extern void PlayerWeaponsManager_GetDistanceBetweenWeaponSlots_m21934D486B1D51E89120697165D8C2BF4D691E26 ();
// 0x00000120 System.Void PlayerWeaponsManager::OnWeaponSwitched(WeaponController)
extern void PlayerWeaponsManager_OnWeaponSwitched_m8A6C57AD2B79B9AF6292A6EA3FF7720109C0BE3F ();
// 0x00000121 System.Void PlayerWeaponsManager::.ctor()
extern void PlayerWeaponsManager__ctor_m832E538F3F10E830DD25344E7C6FD7A0E02C0DCA ();
// 0x00000122 System.Void PositionBobbing::Start()
extern void PositionBobbing_Start_mCA16C5241D5CB78AC48BF2A1C24C682F5C5BECFC ();
// 0x00000123 System.Void PositionBobbing::Update()
extern void PositionBobbing_Update_m3E00106DD5B039EE133EFCF5D016E797866EC0B3 ();
// 0x00000124 System.Void PositionBobbing::.ctor()
extern void PositionBobbing__ctor_mD736F650C3500042A22B5C81594BABE6DCF5DD13 ();
// 0x00000125 System.Void PrefabReplacer::.ctor()
extern void PrefabReplacer__ctor_mF7077444EBA5EA319691AEE04B91F5DB3346A4D6 ();
// 0x00000126 System.Void PrefabReplacerOnInstance::Awake()
extern void PrefabReplacerOnInstance_Awake_mDEA1BDDE19B93C319A716055AE9F3D17AA3B43EE ();
// 0x00000127 System.Void PrefabReplacerOnInstance::.ctor()
extern void PrefabReplacerOnInstance__ctor_mEBB4CF25658DA9D66512DC953BA3700AF11BF87F ();
// 0x00000128 UnityEngine.GameObject ProjectileBase::get_owner()
extern void ProjectileBase_get_owner_m0D8944A73FA74A300254F220E3DE3665808D939C ();
// 0x00000129 System.Void ProjectileBase::set_owner(UnityEngine.GameObject)
extern void ProjectileBase_set_owner_m0E90ACDDB16FC31FF73AC5A1D5E0057B919F1F7E ();
// 0x0000012A UnityEngine.Vector3 ProjectileBase::get_initialPosition()
extern void ProjectileBase_get_initialPosition_m096583D273B9235CA596B4D39D3C15B566494DEE ();
// 0x0000012B System.Void ProjectileBase::set_initialPosition(UnityEngine.Vector3)
extern void ProjectileBase_set_initialPosition_m440F9E9B16B35DC1F45547C75F2AD6E91EDF2005 ();
// 0x0000012C UnityEngine.Vector3 ProjectileBase::get_initialDirection()
extern void ProjectileBase_get_initialDirection_mC654160B1F74E30F794B4BBBAFB6AB5BC897D080 ();
// 0x0000012D System.Void ProjectileBase::set_initialDirection(UnityEngine.Vector3)
extern void ProjectileBase_set_initialDirection_mEBE01C46D9C31C10DB2E7B1556092307ADA3CB56 ();
// 0x0000012E UnityEngine.Vector3 ProjectileBase::get_inheritedMuzzleVelocity()
extern void ProjectileBase_get_inheritedMuzzleVelocity_m9D123BE639A3E43C3D9E36481BA7CDF50F2503FC ();
// 0x0000012F System.Void ProjectileBase::set_inheritedMuzzleVelocity(UnityEngine.Vector3)
extern void ProjectileBase_set_inheritedMuzzleVelocity_m1BD5922A4515C89AFCCB22EFB49B2D7EE0B6D140 ();
// 0x00000130 System.Single ProjectileBase::get_initialCharge()
extern void ProjectileBase_get_initialCharge_m0744E2064FAD08E27215AB586DE16DE9FABB2280 ();
// 0x00000131 System.Void ProjectileBase::set_initialCharge(System.Single)
extern void ProjectileBase_set_initialCharge_m0933C15ED25C0A8D3C0C75D9E21C59F4AC9E829B ();
// 0x00000132 System.Void ProjectileBase::Shoot(WeaponController)
extern void ProjectileBase_Shoot_m335907E7139D7DD9185EF5BCF24007F38BF7BF08 ();
// 0x00000133 System.Void ProjectileBase::.ctor()
extern void ProjectileBase__ctor_mA5383CE6E44783561078E5FCEFA95429F4582F58 ();
// 0x00000134 System.Void ProjectileChargeParameters::OnEnable()
extern void ProjectileChargeParameters_OnEnable_m42640373A6D79BA7D5BD14F96497A8C13874DD15 ();
// 0x00000135 System.Void ProjectileChargeParameters::OnShoot()
extern void ProjectileChargeParameters_OnShoot_m23F2BC2222323634AFC18FBDD6ECBFA6D1B78CB0 ();
// 0x00000136 System.Void ProjectileChargeParameters::.ctor()
extern void ProjectileChargeParameters__ctor_mFDB6FD8B7845D817AFD8927C4D7E57366C36B39E ();
// 0x00000137 System.Void ProjectileStandard::OnEnable()
extern void ProjectileStandard_OnEnable_mF2AFA986D8F0315C686F74247E613FDEB6B8BE77 ();
// 0x00000138 System.Void ProjectileStandard::OnShoot()
extern void ProjectileStandard_OnShoot_mABB31453B22ABF9279BEE931C5F8E89CEFBF990B ();
// 0x00000139 System.Void ProjectileStandard::Update()
extern void ProjectileStandard_Update_m45EE6941B5A6805A3592393E0989CFF7C36EE2E1 ();
// 0x0000013A System.Boolean ProjectileStandard::IsHitValid(UnityEngine.RaycastHit)
extern void ProjectileStandard_IsHitValid_m0B69A299205AA8A823E051766106B86A40BD5BC3 ();
// 0x0000013B System.Void ProjectileStandard::OnHit(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider)
extern void ProjectileStandard_OnHit_m2082385E2E0B3B99D108E331F4D05129762272E8 ();
// 0x0000013C System.Void ProjectileStandard::OnDrawGizmosSelected()
extern void ProjectileStandard_OnDrawGizmosSelected_m7CEB7D2AC125357E8AA3DDEA61D62BC5876052AB ();
// 0x0000013D System.Void ProjectileStandard::.ctor()
extern void ProjectileStandard__ctor_m56478F25CDF49B7BCC1439C516CCE6980EAA9027 ();
// 0x0000013E System.String TakeScreenshot::getPath()
extern void TakeScreenshot_getPath_m0ED442178774BC69B513CA8C7B0AC69E55164471 ();
// 0x0000013F System.Void TakeScreenshot::Awake()
extern void TakeScreenshot_Awake_m8568E10F72FA9C9C01822086C7A4DB3309236039 ();
// 0x00000140 System.Void TakeScreenshot::Update()
extern void TakeScreenshot_Update_m3229FBCB16C43ACF9D2D8CCC5568FCBF66879601 ();
// 0x00000141 System.Void TakeScreenshot::OnTakeScreenshotButtonPressed()
extern void TakeScreenshot_OnTakeScreenshotButtonPressed_m041F5F9B6135B9D9F69E471E6C444C8AFC94BCAD ();
// 0x00000142 System.Void TakeScreenshot::LoadScreenshot()
extern void TakeScreenshot_LoadScreenshot_mDF1BA948BCCA32102807D2FDD1567CE4E7C5BDA2 ();
// 0x00000143 System.Void TakeScreenshot::.ctor()
extern void TakeScreenshot__ctor_m78BAD2A0CEC02FE3F379CF8D4E9AFB31EA8AA0A3 ();
// 0x00000144 System.Void TeleportPlayer::Awake()
extern void TeleportPlayer_Awake_m6669BD20DC7DA542120776A256F933618F1FC13F ();
// 0x00000145 System.Void TeleportPlayer::Update()
extern void TeleportPlayer_Update_mAA1EC30FC2C20D736FCF4BDF8BDB61ED7906A4DA ();
// 0x00000146 System.Void TeleportPlayer::.ctor()
extern void TeleportPlayer__ctor_mF8D41A4C761A5048189B87E45AF53ABB07287DCD ();
// 0x00000147 System.Void TemplateObject::Update()
extern void TemplateObject_Update_m6A4FC11E8BA9014440AEAD83A477727928A03006 ();
// 0x00000148 System.Void TemplateObject::.ctor()
extern void TemplateObject__ctor_mD98217FCAFD6FE79075042484FA36699E93A25CF ();
// 0x00000149 System.Void TimedSelfDestruct::Awake()
extern void TimedSelfDestruct_Awake_mDAE061EBCEF2F199837B76B3B76F68FBD35F10A3 ();
// 0x0000014A System.Void TimedSelfDestruct::Update()
extern void TimedSelfDestruct_Update_mE6FDC9D11CA8FBF8641E33699524E11A9687C021 ();
// 0x0000014B System.Void TimedSelfDestruct::.ctor()
extern void TimedSelfDestruct__ctor_mB65A7D642EFD0DA0580F8E94AE83F5F8BD42A6F9 ();
// 0x0000014C System.Int32 AmmoCounter::get_weaponCounterIndex()
extern void AmmoCounter_get_weaponCounterIndex_mB3100D3B5713CA73962911C3D47D0E84BD4584B6 ();
// 0x0000014D System.Void AmmoCounter::set_weaponCounterIndex(System.Int32)
extern void AmmoCounter_set_weaponCounterIndex_mA45DE6A1F4EF1860A94F65EDA63CD49E6E924758 ();
// 0x0000014E System.Void AmmoCounter::Initialize(WeaponController,System.Int32)
extern void AmmoCounter_Initialize_m8213538E0F5CCEF79F8FF69501026966062B9FAB ();
// 0x0000014F System.Void AmmoCounter::Update()
extern void AmmoCounter_Update_m7900C8AE682D18C4BC181FB697F559027B2C8C7E ();
// 0x00000150 System.Void AmmoCounter::.ctor()
extern void AmmoCounter__ctor_mC0ED0E5878DED33D8D8BAE4E2F6A1629FA6827C0 ();
// 0x00000151 System.Void Compass::Awake()
extern void Compass_Awake_m305AC7675BE6B58252E3742C6FDDBE08009CCDF8 ();
// 0x00000152 System.Void Compass::Update()
extern void Compass_Update_m8E250BBFD26E03985A45673212E543971EA6E234 ();
// 0x00000153 System.Void Compass::RegisterCompassElement(UnityEngine.Transform,CompassMarker)
extern void Compass_RegisterCompassElement_m745267F45655F5E8EBCCB026EE4ECA5382491BAA ();
// 0x00000154 System.Void Compass::UnregisterCompassElement(UnityEngine.Transform)
extern void Compass_UnregisterCompassElement_mDE678F73CAB089285882780485C784DB982896AC ();
// 0x00000155 System.Void Compass::.ctor()
extern void Compass__ctor_mBAF5AD608A7C804C96BB5874F61606789B0C2B3F ();
// 0x00000156 System.Void CompassElement::Awake()
extern void CompassElement_Awake_m1E05A93653775FA76B561B82A934B573F0A19183 ();
// 0x00000157 System.Void CompassElement::OnDestroy()
extern void CompassElement_OnDestroy_mF8F5A47FC58A88888A82BC9658C5AB34EB1B9867 ();
// 0x00000158 System.Void CompassElement::.ctor()
extern void CompassElement__ctor_m460AF29D4D3B9ABC0921035C988E823E8B9473A6 ();
// 0x00000159 System.Void CompassMarker::Initialize(CompassElement,System.String)
extern void CompassMarker_Initialize_m7105288E83CBC8975FD49217D771C8FDD23469E5 ();
// 0x0000015A System.Void CompassMarker::DetectTarget()
extern void CompassMarker_DetectTarget_mB2F4469725C370B56C2A643C873150DE37BDF15D ();
// 0x0000015B System.Void CompassMarker::LostTarget()
extern void CompassMarker_LostTarget_mBEAFF72693D333B1218030D0D82ABAF573DA7EDC ();
// 0x0000015C System.Void CompassMarker::.ctor()
extern void CompassMarker__ctor_m0E60B5B6AA329B60D6AAB01D68C426400C0F4774 ();
// 0x0000015D System.Void CrosshairManager::Start()
extern void CrosshairManager_Start_mDC5EAD0555029FC48547C70080088A8BA1879C81 ();
// 0x0000015E System.Void CrosshairManager::Update()
extern void CrosshairManager_Update_mDB80E5FB668CE7FAB8EC7D5451EDCB7DD8B55A3C ();
// 0x0000015F System.Void CrosshairManager::UpdateCrosshairPointingAtEnemy(System.Boolean)
extern void CrosshairManager_UpdateCrosshairPointingAtEnemy_m37048969418927A30A9A7D3F6E446FD0E0507E4F ();
// 0x00000160 System.Void CrosshairManager::OnWeaponChanged(WeaponController)
extern void CrosshairManager_OnWeaponChanged_m0BE29994ACF2A98ACBC4AB3F2B772D04F5A0E7E7 ();
// 0x00000161 System.Void CrosshairManager::.ctor()
extern void CrosshairManager__ctor_mBBDEF19E2F95C020D43B16DACEE2A84B21B91E17 ();
// 0x00000162 System.Void DisplayMessage::Start()
extern void DisplayMessage_Start_m5B3FC0C2BB5875C621F39BE5B70B0AF87C44AE41 ();
// 0x00000163 System.Void DisplayMessage::Update()
extern void DisplayMessage_Update_m151CB05BB8024D49F65B9ABA1F58E9F1C49DD836 ();
// 0x00000164 System.Void DisplayMessage::.ctor()
extern void DisplayMessage__ctor_m22ED3CA685FB2C57B21732FD411C01DCA759E64B ();
// 0x00000165 System.Void DisplayMessageManager::.ctor()
extern void DisplayMessageManager__ctor_m690CC30984FCF0B2499C84D8015C3D61BBCEE5AA ();
// 0x00000166 System.Void EnemyCounter::Awake()
extern void EnemyCounter_Awake_mB5A95CF9ACD40A0B05F626268F9EFE194CD9B29A ();
// 0x00000167 System.Void EnemyCounter::Update()
extern void EnemyCounter_Update_mB279D0E020CC58E826C37D02E97EE5C13D9B335A ();
// 0x00000168 System.Void EnemyCounter::.ctor()
extern void EnemyCounter__ctor_m1079EEC6381D6C35CCFFB8718E9E31CD72D2E379 ();
// 0x00000169 System.Void FeedbackFlashHUD::Start()
extern void FeedbackFlashHUD_Start_m122C035796C6E5DFC466AE320822A2812330C2B7 ();
// 0x0000016A System.Void FeedbackFlashHUD::Update()
extern void FeedbackFlashHUD_Update_m977B90332CDC31531B6CE1C29F1919BE09610CE8 ();
// 0x0000016B System.Void FeedbackFlashHUD::ResetFlash()
extern void FeedbackFlashHUD_ResetFlash_m35DBBCA88523D88897646DA8E1A1F4D4D0887795 ();
// 0x0000016C System.Void FeedbackFlashHUD::OnTakeDamage(System.Single,UnityEngine.GameObject)
extern void FeedbackFlashHUD_OnTakeDamage_mC6E5F4FC013BC131FD8E6E2AF1178B4AD4041D55 ();
// 0x0000016D System.Void FeedbackFlashHUD::OnHealed(System.Single)
extern void FeedbackFlashHUD_OnHealed_mABF624B767DB393577A63E55CB44AA00DB5DBA4E ();
// 0x0000016E System.Void FeedbackFlashHUD::.ctor()
extern void FeedbackFlashHUD__ctor_mD3C0C0F5746097D1B362E83C01DC13C20B203481 ();
// 0x0000016F System.Void FillBarColorChange::Initialize(System.Single,System.Single)
extern void FillBarColorChange_Initialize_m9B1B1C0CE415AA4862C7C5F19B66B76C32EDB1ED ();
// 0x00000170 System.Void FillBarColorChange::UpdateVisual(System.Single)
extern void FillBarColorChange_UpdateVisual_m1B35DF56805D89C6D0BF43E0D4346AA0BF7E8830 ();
// 0x00000171 System.Void FillBarColorChange::.ctor()
extern void FillBarColorChange__ctor_mF5E65AC21CE36C375D52BD6004FE1BCCCE832BB7 ();
// 0x00000172 System.Void JetpackCounter::Awake()
extern void JetpackCounter_Awake_mC5A4375272BBEBE819686708BC380A2F6B00E92B ();
// 0x00000173 System.Void JetpackCounter::Update()
extern void JetpackCounter_Update_m8194C740C674F40A453FA7FF1301FCEF74ED6A97 ();
// 0x00000174 System.Void JetpackCounter::.ctor()
extern void JetpackCounter__ctor_mD751CE07F2BFA2389BDC1C0E12027F2CE4C47C4A ();
// 0x00000175 System.Void LoadSceneButton::Update()
extern void LoadSceneButton_Update_mC7A1DDD605F49C7394A7C06D4292DE00175D1289 ();
// 0x00000176 System.Void LoadSceneButton::LoadTargetScene()
extern void LoadSceneButton_LoadTargetScene_m030672AC4E2107C510F44DA0F39049B6C24D2776 ();
// 0x00000177 System.Void LoadSceneButton::.ctor()
extern void LoadSceneButton__ctor_m3E912B5D57F0C196F6A4AB7DB8F9A527BFFD61F5 ();
// 0x00000178 System.Void MenuNavigation::Start()
extern void MenuNavigation_Start_mF31F57B5C995FAB7D18CCDEE22C2333742BFC871 ();
// 0x00000179 System.Void MenuNavigation::LateUpdate()
extern void MenuNavigation_LateUpdate_m41F4C71AFF6114D8D938D6B4A15CFD7C19BA8B7E ();
// 0x0000017A System.Void MenuNavigation::.ctor()
extern void MenuNavigation__ctor_mE1485B5092DF6AFEFE5D34BB328320633B988047 ();
// 0x0000017B System.Void NotificationHUDManager::Awake()
extern void NotificationHUDManager_Awake_m1638F07707309F5B2B3C6EB638DEBCCD08F03A68 ();
// 0x0000017C System.Void NotificationHUDManager::OnUpdateObjective(UnityActionUpdateObjective)
extern void NotificationHUDManager_OnUpdateObjective_m4CAAC870601FF5062D8C3FE25A525077847CB1B9 ();
// 0x0000017D System.Void NotificationHUDManager::OnPickupWeapon(WeaponController,System.Int32)
extern void NotificationHUDManager_OnPickupWeapon_m5B44AD36A993F6313A100AC5D232E41ED1C3A58E ();
// 0x0000017E System.Void NotificationHUDManager::OnUnlockJetpack(System.Boolean)
extern void NotificationHUDManager_OnUnlockJetpack_mC7AECB1A7E9AA0E6213CC2BF9BAE689C6E343B61 ();
// 0x0000017F System.Void NotificationHUDManager::CreateNotification(System.String)
extern void NotificationHUDManager_CreateNotification_mA49A3AB6182D005997318F39BCFF34A7EDA1A38E ();
// 0x00000180 System.Void NotificationHUDManager::RegisterObjective(Objective)
extern void NotificationHUDManager_RegisterObjective_mAED484DCF0731E6506EF9C3AC117A283CB2443B5 ();
// 0x00000181 System.Void NotificationHUDManager::UnregisterObjective(Objective)
extern void NotificationHUDManager_UnregisterObjective_mCE4E5799A4D970BD24C95FD93515EAE947093623 ();
// 0x00000182 System.Void NotificationHUDManager::.ctor()
extern void NotificationHUDManager__ctor_m7163EA3A42A92C5102DF649734C4449DF36F6502 ();
// 0x00000183 System.Void NotificationToast::Initialize(System.String)
extern void NotificationToast_Initialize_m9E521B69E8806DC18EF0073CE9994E9C327CF912 ();
// 0x00000184 System.Void NotificationToast::Update()
extern void NotificationToast_Update_m0260AFB121B72BCC83CFD149E28EE9B12AA23CCF ();
// 0x00000185 System.Void NotificationToast::.ctor()
extern void NotificationToast__ctor_m2D2B8F4FEFD22C3CEC3305C426952C9042463204 ();
// 0x00000186 System.Void ObjectiveHUDManger::Awake()
extern void ObjectiveHUDManger_Awake_mF234DEB699B1548C5364FC49A73ABC905C72F4E7 ();
// 0x00000187 System.Void ObjectiveHUDManger::RegisterObjective(Objective)
extern void ObjectiveHUDManger_RegisterObjective_m0C18B94122EBA9CCAFB3DC1C2194953CF932801B ();
// 0x00000188 System.Void ObjectiveHUDManger::UnregisterObjective(Objective)
extern void ObjectiveHUDManger_UnregisterObjective_m241D7B3A88A8D2D3D542AB78479C700537C39656 ();
// 0x00000189 System.Void ObjectiveHUDManger::OnUpdateObjective(UnityActionUpdateObjective)
extern void ObjectiveHUDManger_OnUpdateObjective_mD32BD0EA9F81EA7A85AA0CF52F983B542FC6AD4E ();
// 0x0000018A System.Void ObjectiveHUDManger::.ctor()
extern void ObjectiveHUDManger__ctor_m5179F38AF0E9997D8EF37891F4D6293AF7090608 ();
// 0x0000018B System.Void ObjectiveToast::Initialize(System.String,System.String,System.String,System.Boolean,System.Single)
extern void ObjectiveToast_Initialize_mBA94C5A27D955591494ED878F5248BA02F950BBC ();
// 0x0000018C System.Void ObjectiveToast::Complete()
extern void ObjectiveToast_Complete_m28F479DDCA0F4346B1638DBED4CADD12B00BA0F4 ();
// 0x0000018D System.Void ObjectiveToast::Update()
extern void ObjectiveToast_Update_mF394491E029329DEBFA82E4C4FCC4CE9CD818026 ();
// 0x0000018E System.Void ObjectiveToast::PlaySound(UnityEngine.AudioClip)
extern void ObjectiveToast_PlaySound_m2DC26ADEA26B22A58915050ECADB6717946763A3 ();
// 0x0000018F System.Void ObjectiveToast::.ctor()
extern void ObjectiveToast__ctor_m28F49E032867B2084A5103184B1B308191B39106 ();
// 0x00000190 System.Void PlayerHealthBar::Start()
extern void PlayerHealthBar_Start_mCF5EACA3F6C7B2074B55001FF47CCC13EACE47E4 ();
// 0x00000191 System.Void PlayerHealthBar::Update()
extern void PlayerHealthBar_Update_mB81AC7EDB185CEBA07A859DF872DC67086AF24CD ();
// 0x00000192 System.Void PlayerHealthBar::.ctor()
extern void PlayerHealthBar__ctor_m0F2FA58075B89822E044627CC28DC6B2B860D94C ();
// 0x00000193 System.Void StanceHUD::Start()
extern void StanceHUD_Start_m814DF36C577F00B1EE8287AFFFC7672E98C91360 ();
// 0x00000194 System.Void StanceHUD::OnStanceChanged(System.Boolean)
extern void StanceHUD_OnStanceChanged_m297398277ABA475A4A25B5B5676580F0E1EE58B0 ();
// 0x00000195 System.Void StanceHUD::.ctor()
extern void StanceHUD__ctor_m3BEAB585F0B255C086E26580A5247D1671CE15CD ();
// 0x00000196 System.Void ToggleGameObjectButton::Update()
extern void ToggleGameObjectButton_Update_m4B4A607C06471F694EDCB9CFDF43D2738A6AB8DE ();
// 0x00000197 System.Void ToggleGameObjectButton::SetGameObjectActive(System.Boolean)
extern void ToggleGameObjectButton_SetGameObjectActive_m69EC0360AFD97A4CF1DADFE5A6B754A4189B6431 ();
// 0x00000198 System.Void ToggleGameObjectButton::.ctor()
extern void ToggleGameObjectButton__ctor_m5856E6CBB56BE7A64AE6EB112F25A316CEFBB74E ();
// 0x00000199 System.Void WeaponHUDManager::Start()
extern void WeaponHUDManager_Start_mB3691A6FADA9D605A53BD1B56E24442CBF7B97CC ();
// 0x0000019A System.Void WeaponHUDManager::AddWeapon(WeaponController,System.Int32)
extern void WeaponHUDManager_AddWeapon_m40F4058D81FFCA0640432F656F1506825190559D ();
// 0x0000019B System.Void WeaponHUDManager::RemoveWeapon(WeaponController,System.Int32)
extern void WeaponHUDManager_RemoveWeapon_m01F57E6BDDB655C20A2308D7D15FD8201AE58F15 ();
// 0x0000019C System.Void WeaponHUDManager::ChangeWeapon(WeaponController)
extern void WeaponHUDManager_ChangeWeapon_mF6F576F5B15E561DF5D09CCE7417BA699E072683 ();
// 0x0000019D System.Void WeaponHUDManager::.ctor()
extern void WeaponHUDManager__ctor_m1B5D93240B1B0EDD834D7294AD637F936C66D180 ();
// 0x0000019E System.Void WorldspaceHealthBar::Update()
extern void WorldspaceHealthBar_Update_mE1B2CE097FF63BDA7D2EA4A5D538FBF65BF676B8 ();
// 0x0000019F System.Void WorldspaceHealthBar::.ctor()
extern void WorldspaceHealthBar__ctor_m9DD18744523EDD803B3A75898E4EDEF38DFC17FE ();
// 0x000001A0 System.Single WeaponController::get_LastChargeTriggerTimestamp()
extern void WeaponController_get_LastChargeTriggerTimestamp_mC77917468FADC1A568BD9E2A4D378028B1765DF3 ();
// 0x000001A1 System.Void WeaponController::set_LastChargeTriggerTimestamp(System.Single)
extern void WeaponController_set_LastChargeTriggerTimestamp_mB3ECE4004948C0D4C3E11CCA701E269F95A7AF06 ();
// 0x000001A2 UnityEngine.GameObject WeaponController::get_owner()
extern void WeaponController_get_owner_mB5CA66395D4AA5B45E4FFF61DA49BB70F487281D ();
// 0x000001A3 System.Void WeaponController::set_owner(UnityEngine.GameObject)
extern void WeaponController_set_owner_m5C399CEA370A39BFB2AA8564276F43BA92464BB2 ();
// 0x000001A4 UnityEngine.GameObject WeaponController::get_sourcePrefab()
extern void WeaponController_get_sourcePrefab_m9B0A30B7ADE6CBA36E7CE2388380C32D91E56BA6 ();
// 0x000001A5 System.Void WeaponController::set_sourcePrefab(UnityEngine.GameObject)
extern void WeaponController_set_sourcePrefab_m7FA17C97D95A300D980081F26E3013BB4C8964CA ();
// 0x000001A6 System.Boolean WeaponController::get_isCharging()
extern void WeaponController_get_isCharging_m62B0A4EC535BDD25071F76C653DBE76ED73DA0D9 ();
// 0x000001A7 System.Void WeaponController::set_isCharging(System.Boolean)
extern void WeaponController_set_isCharging_m959B528A094C572AA6F8AC5852699F3A17795943 ();
// 0x000001A8 System.Single WeaponController::get_currentAmmoRatio()
extern void WeaponController_get_currentAmmoRatio_mF7180E6799FB3AD19BE14019CE98B6A12125C842 ();
// 0x000001A9 System.Void WeaponController::set_currentAmmoRatio(System.Single)
extern void WeaponController_set_currentAmmoRatio_mAD3632A3E72BFCC9A2D3718C08FB485F325A28E3 ();
// 0x000001AA System.Boolean WeaponController::get_isWeaponActive()
extern void WeaponController_get_isWeaponActive_mAA00F2995C9212A16626E57B1BCC0BD561E0214F ();
// 0x000001AB System.Void WeaponController::set_isWeaponActive(System.Boolean)
extern void WeaponController_set_isWeaponActive_mCC1C68A19A675E4D9E2389F239F1EE22FDF04B6F ();
// 0x000001AC System.Boolean WeaponController::get_isCooling()
extern void WeaponController_get_isCooling_m943D42CD96EC99A45FBD6756D523D06B4D3BA99F ();
// 0x000001AD System.Void WeaponController::set_isCooling(System.Boolean)
extern void WeaponController_set_isCooling_m2C9857D7145DBFA34B40B8214EBB0C83B8963AE5 ();
// 0x000001AE System.Single WeaponController::get_currentCharge()
extern void WeaponController_get_currentCharge_m7EA8715BC01C38F1117870CE39BE125435D0A814 ();
// 0x000001AF System.Void WeaponController::set_currentCharge(System.Single)
extern void WeaponController_set_currentCharge_m93D527A49E4A95A9221B4638BEEFBDEC0830F188 ();
// 0x000001B0 UnityEngine.Vector3 WeaponController::get_muzzleWorldVelocity()
extern void WeaponController_get_muzzleWorldVelocity_mA018C743F8C5C5E327E7461F8E8B90FB33054875 ();
// 0x000001B1 System.Void WeaponController::set_muzzleWorldVelocity(UnityEngine.Vector3)
extern void WeaponController_set_muzzleWorldVelocity_m91E8B1C8BD47AAFE9C6DA5F6A3AEB16B86E6D929 ();
// 0x000001B2 System.Single WeaponController::GetAmmoNeededToShoot()
extern void WeaponController_GetAmmoNeededToShoot_mE7B0C102AB5B87D7A0300F26374905AB5A788947 ();
// 0x000001B3 System.Void WeaponController::Awake()
extern void WeaponController_Awake_m05B2535AD72C22C7AFCE1496517B79F7F588C570 ();
// 0x000001B4 System.Void WeaponController::Update()
extern void WeaponController_Update_mDCF4F60C5E354DE8062B0FC860E0ED5AB36B1525 ();
// 0x000001B5 System.Void WeaponController::UpdateAmmo()
extern void WeaponController_UpdateAmmo_mAC4A086C81686FB4C5B20412207B2DA2559C4905 ();
// 0x000001B6 System.Void WeaponController::UpdateCharge()
extern void WeaponController_UpdateCharge_mC93687464F57F5F5C1D025FCEE804F8D2DEDC164 ();
// 0x000001B7 System.Void WeaponController::UpdateContinuousShootSound()
extern void WeaponController_UpdateContinuousShootSound_mCC5EDFC5C83B465E54851ADE78C282A4974F0B3B ();
// 0x000001B8 System.Void WeaponController::ShowWeapon(System.Boolean)
extern void WeaponController_ShowWeapon_m8E2AB0EC2D103F7C5500F646FC84BAFC4E99BDFC ();
// 0x000001B9 System.Void WeaponController::UseAmmo(System.Single)
extern void WeaponController_UseAmmo_m869CCDD903D481785BCCD76FFF0E4347E3287D44 ();
// 0x000001BA System.Boolean WeaponController::HandleShootInputs(System.Boolean,System.Boolean,System.Boolean)
extern void WeaponController_HandleShootInputs_m0F1DE552B0C1155C55B95A703017FF0465A7F01C ();
// 0x000001BB System.Boolean WeaponController::TryShoot()
extern void WeaponController_TryShoot_m5A9B3341B62A4E46E7B3CCF6C12FAD9D93DED291 ();
// 0x000001BC System.Boolean WeaponController::TryBeginCharge()
extern void WeaponController_TryBeginCharge_m5D39B03E72C21C4C741A5B125D8A486F5435D014 ();
// 0x000001BD System.Boolean WeaponController::TryReleaseCharge()
extern void WeaponController_TryReleaseCharge_mDECC76CD65B14EAC78E937E40E78AF5DF1348654 ();
// 0x000001BE System.Void WeaponController::HandleShoot()
extern void WeaponController_HandleShoot_m0C5F723C040002F4CC6E6BDE3EF634313678B018 ();
// 0x000001BF UnityEngine.Vector3 WeaponController::GetShotDirectionWithinSpread(UnityEngine.Transform)
extern void WeaponController_GetShotDirectionWithinSpread_mA1A165FB9F62FBDDB906CA9C907129EA9D7D54CA ();
// 0x000001C0 System.Void WeaponController::.ctor()
extern void WeaponController__ctor_mEF2EFCA70472BEE760C56586F4B3C7637BDB8512 ();
// 0x000001C1 System.Void WeaponFuelCellHandler::Start()
extern void WeaponFuelCellHandler_Start_mC5BDF5D098F7138A800B85BA4D934514C8DDD3FC ();
// 0x000001C2 System.Void WeaponFuelCellHandler::Update()
extern void WeaponFuelCellHandler_Update_mDB4277536619C89299A9072410D1462BADEC4A47 ();
// 0x000001C3 System.Void WeaponFuelCellHandler::.ctor()
extern void WeaponFuelCellHandler__ctor_m5D0C4964CB7F4C149BFEE22852F4FB224F4BF4AA ();
// 0x000001C4 System.Void WeaponPickup::Start()
extern void WeaponPickup_Start_mDD98BD5363E0B4D89AA720E4BD6D942924A5A7DB ();
// 0x000001C5 System.Void WeaponPickup::OnPicked(PlayerCharacterController)
extern void WeaponPickup_OnPicked_m19C08F01C850D8A2FA89CC1A961006A541A2AF6C ();
// 0x000001C6 System.Void WeaponPickup::.ctor()
extern void WeaponPickup__ctor_m1ADFB171B8333C4EA16C3FB0B09F861548EB206E ();
// 0x000001C7 System.Void EnemyController_RendererIndexData::.ctor(UnityEngine.Renderer,System.Int32)
extern void RendererIndexData__ctor_m74AF941246DC215455016DBAD4A6C1B235780F2F_AdjustorThunk ();
// 0x000001C8 System.Void MeshCombineUtility_RenderBatchData::.ctor()
extern void RenderBatchData__ctor_m1FE81D444573AFA9AE4D37FC12A38AFA31E4C2A3 ();
// 0x000001C9 System.Void OverheatBehavior_RendererIndexData::.ctor(UnityEngine.Renderer,System.Int32)
extern void RendererIndexData__ctor_m0E73078E1BC53CC52A683EC27A8D83CB0862AC3E_AdjustorThunk ();
// 0x000001CA System.Void MeshCombineUtility_RenderBatchData_MeshAndTRS::.ctor(UnityEngine.Mesh,UnityEngine.Matrix4x4)
extern void MeshAndTRS__ctor_m3CD5F70F5E811604883E3A0A6B23C7C441DB83E6 ();
static Il2CppMethodPointer s_methodPointers[458] = 
{
	Actor_Start_m1CFDC1005F8A86658B8C3EB9A6C86ABF02C3E265,
	Actor_OnDestroy_m490DA289265E3BC6984CE42330699CAB75C0433E,
	Actor__ctor_m80C1F3A5D7941420C895B29FF5B4930EE74AAC98,
	ActorsManager_get_actors_m6499CE3048819DF26F7DA45E040FFBD01B38A6EB,
	ActorsManager_set_actors_mAB53F8E386254363CA260817280FF1775000DCE1,
	ActorsManager_Awake_mEBE95BC11E7357D2EBC9D155BB406F8C8EF4CFCB,
	ActorsManager__ctor_mF1732900B4501292F24D047D975ECED1ACD42D3B,
	AudioManager_FindMatchingGroups_m29D056E41D326C21D1679C8432A60A73C2280079,
	AudioManager_SetFloat_mF3246EC004ED0D48CED84509978FECA38B1C5F92,
	AudioManager_GetFloat_mE5520ACFE8A93DACC04D968C1195FDC2BADFD968,
	AudioManager__ctor_m9FC47B3998693846C091FB4C3731ACEF0359AC48,
	AudioUtility_CreateSFX_mE48AA1558D57DC802A15D0DA9CF515CC950B8878,
	AudioUtility_GetAudioGroup_mA3EC513FB52E381B812EFCA7AD9836C71752E8E6,
	AudioUtility_SetMasterVolume_m9887F1897BF130E71C0AE14967E9BEEB145A9F89,
	AudioUtility_GetMasterVolume_m02B43342A8ADBDE81F13947ECF68160400103CFF,
	AudioUtility__ctor_m18E244FB9EF75C53F8390179A4C77F4E0534F7BE,
	ChargedProjectileEffectsHandler_OnEnable_m9E47C18396BE334A32858D2C64B1B93108AB2A80,
	ChargedProjectileEffectsHandler_OnShoot_mE2033D78B169013C97E3D828FB4D92E1199678A5,
	ChargedProjectileEffectsHandler__ctor_m7C12E5B48B719359CDAC871E5918E27CCBC0B393,
	ChargedWeaponEffectsHandler_get_particleInstance_m2E8CB349320F9D6666F4805F1FBEABD6D45802C1,
	ChargedWeaponEffectsHandler_set_particleInstance_mEF0BF733A1A399CDCA53CA9457592FE54EB95218,
	ChargedWeaponEffectsHandler_Awake_m7D50E318D3176CA9620E3CEA80C92CAD3057B1C3,
	ChargedWeaponEffectsHandler_SpawnParticleSystem_m81B98D965A9DA16C6F6DB339072848B10BD14F49,
	ChargedWeaponEffectsHandler_FindReferences_mF8B5217D0EB36597719419971DA2CC9D104B6EA3,
	ChargedWeaponEffectsHandler_Update_m5CBD6ED1539B1F7E10D57CAAECC966A8F72FAE7A,
	ChargedWeaponEffectsHandler__ctor_m2A6FD0736D8CAA5765D21CF13439E766D407BF3B,
	ConstantRotation_Update_mE0FA9F3E254F9A9C5AD62BFB216C6B0BFAB47EEC,
	ConstantRotation__ctor_m75B3E5212B6CD5EFF6C665A3F69174BB2DB441EC,
	DamageArea_InflictDamageInArea_mA630415A77BBFC9FBAE48732DD882B4F6198D0B1,
	DamageArea_OnDrawGizmosSelected_m7CCE155FA01977FE894EFDE59D604E0045269CDE,
	DamageArea__ctor_m128D8CD2252A50D4AC2C0AC07335F59488758081,
	Damageable_get_health_m5C73D88A893B8AC146904E41CA8903A71F222BC7,
	Damageable_set_health_mF166A391D435846ABCA8BD65FE164E2FB70CE454,
	Damageable_Awake_m69DFF61C30925599331F15D993B98D59B3FE9342,
	Damageable_InflictDamage_mDECE304C922F4499293C79E658DBA86723E760FF,
	Damageable__ctor_m7EFFA40D796018855099921A46D5AEBDE014251E,
	NULL,
	NULL,
	NULL,
	NULL,
	Destructable_Start_m1A4B6A76EE25D66D58F786B691FA081EF0417CA8,
	Destructable_OnDamaged_m5866FC6387B25C031B281EB93D0CD541BF6934D2,
	Destructable_OnDie_mEC5CB14267BFFAF5413499FDB228DE7F8AD4D901,
	Destructable__ctor_m1886A052EFD67D1EFC6F56DEA3D68F63C278C8C0,
	DetectionModule_get_knownDetectedTarget_mF2B3DDC05AA1C5154FC3F32FEBA6CB8DE97E23AD,
	DetectionModule_set_knownDetectedTarget_mA07CFCD0C355AE580CF4FE2B66C7C1D4A2CD6831,
	DetectionModule_get_isTargetInAttackRange_m630F202D215C543946E51E216F5DE94B07B00571,
	DetectionModule_set_isTargetInAttackRange_mE68C945BEE3EF28DCA74E90DB22C4F8011768EF2,
	DetectionModule_get_isSeeingTarget_m369B43689EEF7C25F516538704E424348D21B881,
	DetectionModule_set_isSeeingTarget_mD701C5BF6EC183D03859A7C81B95C561028DD3CD,
	DetectionModule_get_hadKnownTarget_mDAC73A0A56401B4BE42EF6024C530E08B18D26DC,
	DetectionModule_set_hadKnownTarget_m26EDE8CE72FBF513EED0316CEA2CDBE63CA5114A,
	DetectionModule_Start_mC3DA4C42100A44284145E3547B876D75AFBC64C5,
	DetectionModule_HandleTargetDetection_m77D4628FCDA27C92A41997A17B0157F94BEE33F6,
	DetectionModule_OnLostTarget_mF223DF8489B168C75F5FAC07E685D23DFC1FC725,
	DetectionModule_OnDetect_mF7D947CF03D022722636A3E33642D1EB2965DCA8,
	DetectionModule_OnDamaged_mD6282F65BCF6D26EDF2A18292C59C168643B16CD,
	DetectionModule_OnAttack_m588177C79552DD150A65C84AB1EE04A7DAF55B86,
	DetectionModule__ctor_m40CB2DED8B3C7650BE1CB8775107BF242D371718,
	EnemyController_get_patrolPath_m909B79B2E017DD968BB24FB1DCD71E8D94E387C2,
	EnemyController_set_patrolPath_m07BD19F5E0DC7807E84301C2F52D661404297BDD,
	EnemyController_get_knownDetectedTarget_mFA217FA349A99CD6D6A9762BEDF9D3758BB2D22A,
	EnemyController_get_isTargetInAttackRange_m254A594C8BD65E49944969A61EA901759D04FEC5,
	EnemyController_get_isSeeingTarget_mA43D131E74DA340E4709F7840A7FF46F95CC131D,
	EnemyController_get_hadKnownTarget_m47499D1E886C3B79EFF21D024B741DA34CC71DA5,
	EnemyController_get_m_NavMeshAgent_m4EC4520151B335660DDF076EDA24D8EA177A7738,
	EnemyController_set_m_NavMeshAgent_m1DC3730A0C94361703E417DF57BED8C8A02A2128,
	EnemyController_get_m_DetectionModule_m621A35F8A2C9EBF83EC69E2990EECF682655435C,
	EnemyController_set_m_DetectionModule_mF57368D3BA35C2AC42A5D78FAC827F2BD3D0C1F0,
	EnemyController_Start_m098A57DECB6D20D8A8CF100D55F3814A033BEDD0,
	EnemyController_Update_mB42FF5202BE5953ED1E726A86BF4B61F19F8BB3B,
	EnemyController_EnsureIsWithinLevelBounds_mA1D24544900E9EE795FCA045A1CC407EC8BA6066,
	EnemyController_OnLostTarget_mC6FD8B776CFB30E57A468B4761B20026A223F75F,
	EnemyController_OnDetectedTarget_m564BA886AD29A59E8DD7AEA1CF6000BE62289BD7,
	EnemyController_OrientTowards_mA278077A975C907560257782794863641CD2DD93,
	EnemyController_IsPathValid_mDECE3B0A6087522A1A9DC8608E2D84B987BB331D,
	EnemyController_ResetPathDestination_m58F8EE6A1B56A4555A2C153A3261BF3EED38EDD2,
	EnemyController_SetPathDestinationToClosestNode_m00560583B1D792D9303D3F06F65506DDF7AA9548,
	EnemyController_GetDestinationOnPath_m3DA6DF076D1B2EA57C07CC6CF3B1E830B027BB9A,
	EnemyController_SetNavDestination_mBD072B5C7399FF79BB1ECC744D571C1E5DA07882,
	EnemyController_UpdatePathDestination_m1D0BABFB9135E76ED421677C699768B67F9E113B,
	EnemyController_OnDamaged_m8798AEEB37B85FDD2EA8FE4114F4CE347C923164,
	EnemyController_OnDie_mD35F1B0D183BC082F6B17120E597B131F841560F,
	EnemyController_OnDrawGizmosSelected_m943F95E4D683E73C9CF05E5BB1962C3FFCB47AEC,
	EnemyController_OrientWeaponsTowards_m36F819FED8EFD6FCBCC0CDC2796B67D157DD3F08,
	EnemyController_TryAtack_m98B341CBFAEC216B70FC60BFCBDCD17E4798BF15,
	EnemyController_TryDropItem_mFC1A9B99F6F13887D22CFF98CF52C10DCD771F6E,
	EnemyController_FindAndInitializeAllWeapons_m64F01A14DF2AC76164D9916D020DD25F1F75E518,
	EnemyController_GetCurrentWeapon_m481B337DE02A3733A4CEBDB8BC1B67C98F28BC66,
	EnemyController_SetCurrentWeapon_mBA8D3019A360A41266B12E11DA88DFEE0E415B06,
	EnemyController__ctor_m76A2014F0A5962202971DC4401046D10B7A6FF53,
	EnemyManager_get_enemies_m0E127A53828C2E612368137A2DE38F4E020A4B65,
	EnemyManager_set_enemies_mA9579AF3F9C3B18B3086B904CCA56B104AB941AA,
	EnemyManager_get_numberOfEnemiesTotal_m83DF528D712FAA92C17DC96CAE8D9B34E3D3AB05,
	EnemyManager_set_numberOfEnemiesTotal_m7F277B5F032F9BD59EFD44C026919C64ED13F9C3,
	EnemyManager_get_numberOfEnemiesRemaining_mB102B1571ED510298AC0E073BDB6A41CD5BE66EC,
	EnemyManager_Awake_mC6B8B081440E593A8B0D41F11FAED5EBB3831C22,
	EnemyManager_RegisterEnemy_m908591B2BD38191AAF4C272289C014B0F1D12035,
	EnemyManager_UnregisterEnemy_m1EA1D8C765C4402AE3AB6DDC1306FACB321BF5D7,
	EnemyManager__ctor_mD78893BD3335D51F1786538ECA10DA63A21B8DF6,
	EnemyMobile_get_aiState_m6F80F34D39247A2279B0A315CD2392FBD6CFA308,
	EnemyMobile_set_aiState_m3B3E15159A94D203E9A86B1ECE64C47490311453,
	EnemyMobile_Start_mFAE87DFE308852A6B0884C6CBF8D4978B665853A,
	EnemyMobile_Update_mC54FBF20558CB9CC499C11CD3DC5F0BFD19F3E4D,
	EnemyMobile_UpdateAIStateTransitions_m2122737280C23CCC930E565E29F70ACB3F1A5F38,
	EnemyMobile_UpdateCurrentAIState_mC204E25FD2E0E7BD7539ED145140957055D2F42B,
	EnemyMobile_OnAttack_m697E4CFC81D90E2CB80450B792436BA90E5216EB,
	EnemyMobile_OnDetectedTarget_m66348B0387FC7BEC9FAC7A9F1D37718387C24BE4,
	EnemyMobile_OnLostTarget_mBA0F28FF884637B3CD0B78E96A441FA9AA50C259,
	EnemyMobile_OnDamaged_m018F6EE8A9B94FB5C31C797E2432DC89EE20EDB2,
	EnemyMobile__ctor_m14A3C627C27DEE38D29B4E2742DA2497634F5D29,
	EnemyTurret_get_aiState_m42C1E86DB3AFB8E1A45F9BD0144B9CCAE467ED6D,
	EnemyTurret_set_aiState_m4E356EEFD9B7367279638FA9231360BBA720C9E8,
	EnemyTurret_Start_m61B0CB62515DD8CED6811C31E7935FE763D3F317,
	EnemyTurret_Update_m08E030C70DA708876306FD37068D886A855F0E94,
	EnemyTurret_LateUpdate_mF36BB07AF83793703F43FDC74422E851E5FD8FCB,
	EnemyTurret_UpdateCurrentAIState_mAB6D5F0791200B0875B05965F415C0223E344E87,
	EnemyTurret_UpdateTurretAiming_mCFCE236E812C81900199C0305FC34D941C7AD95E,
	EnemyTurret_OnDamaged_mF0ADA8D3C35254788566CD109331E7807E1AC1E3,
	EnemyTurret_OnDetectedTarget_m74245D20A910E9BCBA93F9FD4E8502F7335FD1CC,
	EnemyTurret_OnLostTarget_mAF1742D8E0B3E7712E7E21965057C35DE745CED4,
	EnemyTurret__ctor_m2BF67D445F9E1DE50E241F49612E7A83A1C3086E,
	FollowPlayer_Start_m3D4944B71D74CF5E99F5032DC85E078F16A977C9,
	FollowPlayer_LateUpdate_m2EA9433E8DEDB1E26FDB5C57566B5B97CAAEB02D,
	FollowPlayer__ctor_m7A7391BDBACA4FB475491CC3B13D12271C5AE761,
	FramerateCounter_Update_m5D0E02ADFD513D4178242B0FF1606850D9CA23E8,
	FramerateCounter__ctor_m1F212E7C7383024342DF149A461021C0917ED1CC,
	GameConstants__ctor_m111888F48CBAD817DDB0297AE242A7CD73D10AE9,
	GameFlowManager_get_gameIsEnding_m5703547651BFFD4660C06237D549F930C28ABFB0,
	GameFlowManager_set_gameIsEnding_mD3583C1B9E56794FA279A2D66FFAB1729285A5A9,
	GameFlowManager_Start_mD63A93BF2772FEC818E391AFAE9FAFED6DEE2FB0,
	GameFlowManager_Update_mC637F36E7F04FA30095BAA2CAEFDBE4A986FB5D8,
	GameFlowManager_EndGame_mDC0A6BCBBB4072D37AF58488DB3BB5A18B8F9428,
	GameFlowManager__ctor_m71A7B0808984589B8930E24D5D469603EC7C6602,
	Health_get_currentHealth_m3702D4E7A03A5153B3383738CC91CECC14EA5D47,
	Health_set_currentHealth_m2456633D32E1137A8CB406E55EA4F2431EC8C1A6,
	Health_get_invincible_mC2C333A7ED3093772E8B33CACE19F794B247DC11,
	Health_set_invincible_mB2FD644FB1CAE9AFE2F39A43BEE11FDAEA7775D8,
	Health_canPickup_m256CDDD6ABD420D847AB444A83571FD02D98F27A,
	Health_getRatio_mBE1E48477D95E2783463176157E743D791228B24,
	Health_isCritical_m800B41C15A66973EFA2982E978BF8EE8214D51FB,
	Health_Start_mB4EBE77597318321F1F745E9A192DCF0CE601A98,
	Health_Heal_mCE7E1A95E560F7DC122AF651ECAAD10456E6C445,
	Health_TakeDamage_mC6EB544A26405EB731E7207623C34D909ADE217D,
	Health_Kill_m2CB79E658731C38B96347639D0C5B7302604F086,
	Health_HandleDeath_m1BB2061FBD1104DB8A858546C2D50265E7C59F47,
	Health__ctor_m5EEFBF1CB89A5255DD6DF527EF268C351F56FB16,
	HealthPickup_Start_m3F86BDD65577D5C98907AA8BA8A7F014F02A3DCE,
	HealthPickup_OnPicked_mBDDEB3BE08BAB6AB37A3620F587D48DE27E704A4,
	HealthPickup__ctor_m6AFD4A1A60F44EFA2FA80451FE6951C009B9C2A6,
	IgnoreHeatMap__ctor_m228187BA199FA9A02A8EF339339D3612F3728D80,
	IgnoreHitDetection__ctor_mE454D2F2FE4CFD0D272C47295A1861EF4AED8B65,
	InGameMenuManager_Start_mC56B90D83BA53A972D97708806E46A585C6A4FD8,
	InGameMenuManager_Update_m38CEC8995AF54965E40EE63815FBC85F39855067,
	InGameMenuManager_ClosePauseMenu_m0F793A3FEE4274D7EE6F232B731C0CFC7B2F1EBA,
	InGameMenuManager_SetPauseMenuActivation_mF96E328C9D70F2964A2BF76536BB3DEF3307ADA8,
	InGameMenuManager_OnMouseSensitivityChanged_mD3353F140F19E7E8EFD9CF917E88C4922406C2B1,
	InGameMenuManager_OnShadowsChanged_mC17AEF26AFB653EB8DC3076BFEEA0FBC468F4708,
	InGameMenuManager_OnInvincibilityChanged_m6C049E2A1CCDD89C8307FF5CEA6C5C39CC89E3EB,
	InGameMenuManager_OnFramerateCounterChanged_m1B6E22A542D5111975F24848AEFEDF91215EE2FB,
	InGameMenuManager_OnShowControlButtonClicked_m6F832C99C77C4B745B2693D1E4322E0B47E0B3A1,
	InGameMenuManager__ctor_m01917B747BF82159104B329F15E5F9AC77B37A49,
	Jetpack_get_currentFillRatio_m62AF7D1177839391A8EF63A97F9C2297FDF2B9BA,
	Jetpack_set_currentFillRatio_m5C250952557C4D6EC1EB75CA129371CC14AB9913,
	Jetpack_get_isJetpackUnlocked_mC67FFE3AE7221A4F00B30E5A976BEB927AAA7248,
	Jetpack_set_isJetpackUnlocked_mBE60918EF56E76640EE231A2BBBC1877FF509059,
	Jetpack_isPlayergrounded_m4887B7B653E4B0DD6B16C07DEC2D7B04240E48F9,
	Jetpack_Start_mAF11D818D0C79460574487D6E66652F8DE2436AE,
	Jetpack_Update_mF2CCB7F781FC00DF70C42571A059E9A7A43A0AAA,
	Jetpack_TryUnlock_m5D8F5AA374FDD0DC9A9597926316688ABF6DC81F,
	Jetpack__ctor_m91E09FFF1621AE20F6D935B75A22A31EEC92F9EE,
	JetpackPickup_Start_m083DF2E95F0E9CB6912B2FE9636FA9084ADF8C29,
	JetpackPickup_OnPicked_mD04E1001E8DD4C30BC530D87B2362A97AA459011,
	JetpackPickup__ctor_m5FC8702833D54EDECBD7116EA06D3453EC666616,
	MeshCombineUtility_Combine_m5B6AA5A11B6E0A664670C5B17F9BEA4C21A1E66A,
	MeshCombineUtility_GetExistingRenderBatch_m1614EEE1B467DE249A8605F3A58ECBEDA7251D02,
	MeshCombiner_Start_mEF6C88CF06CB60750580B21FA0AAE7DF3708CBB4,
	MeshCombiner_Combine_m2D8105FC0E01A0C3ACB59A88CB8383F690DEC980,
	MeshCombiner_CombineAllInBounds_m69A238068B90D55CE16FE2512C03535F00DE9083,
	MeshCombiner_GetGridCellCount_mC2C7424C04EFD97BBBAFC0BBFA189DCF3E8D538F,
	MeshCombiner_GetGridCellBounds_m62508E4B7CD21D7540EDD29550F3AE7B8D3BD829,
	MeshCombiner_OnDrawGizmosSelected_m3DDB558F637D0968F7FE9FEE948423C135B3D695,
	MeshCombiner__ctor_mC781E88512B38203ADF37968802F45875C259DC1,
	MinMaxFloat_GetValueFromRatio_mD97564073C90866765E36F13D6460E7A7D4033D5_AdjustorThunk,
	MinMaxColor_GetValueFromRatio_mF84EF5E0C91CB9DF8393B4F3B6DA980CF614B718_AdjustorThunk,
	MinMaxVector3_GetValueFromRatio_mE44BE0E31AE7500B08D7C486E9CA4B3858F37F87_AdjustorThunk,
	NavigationModule__ctor_m8E1F4150B0049E7B5B7F67C723B5EB62BF531EA1,
	Objective_get_isCompleted_m3EAA117894FE7828C6B1645C3BE836D20F2F7D3B,
	Objective_set_isCompleted_m7120EFA189D7A6DDE7AC8DAA7E57582226B0F7EF,
	Objective_isBlocking_m0F7B9F234C0FCDDB740A4723532899EFE7C547FB,
	Objective_Start_m7B028AD27102FDBD7B17E1FC8226861C762C9BD9,
	Objective_UpdateObjective_mD7DC674113C559AEFE74C1AC7BB6398372D89B94,
	Objective_CompleteObjective_mB0EAC46C472144FAC89C8B210358CD71FD3B1990,
	Objective__ctor_m9BE24CDE8FFB146C50CD147DCF92A5F99A40E0C8,
	UnityActionUpdateObjective__ctor_mBF121C52844E8BFA76A7AA31D79005E852B20162,
	ObjectiveKillEnemies_Start_mC415D80A91693A8CC45C54D510E7B54B866136B8,
	ObjectiveKillEnemies_OnKillEnemy_mF0BE43F964977823F1D4B27B507339A92E3C2137,
	ObjectiveKillEnemies_GetUpdatedCounterAmount_mA3D5DAF1A61235F436399B43359369CFCF286771,
	ObjectiveKillEnemies__ctor_mE920BE26CABFEA648857E1E08C7DB55DC0D3470D,
	ObjectiveManager_AreAllObjectivesCompleted_m12749CB1409F4608579E2CC3196B21ADC325D7A6,
	ObjectiveManager_RegisterObjective_m902D3BDB9EAF0D7A016C9E116B80BF788ED52A7E,
	ObjectiveManager__ctor_m09D7C5CFC9760D1BB93B3F01278F16C94D6BFB9F,
	ObjectivePickupItem_Awake_mCBAF859FDBDDF46A8EA1B20CAABCFB799018E408,
	ObjectivePickupItem_OnPickup_m5764D4C775E5C82A1926A4F5AEF318D11A0EB21A,
	ObjectivePickupItem__ctor_m5C725C3B268E18248727752CA17CDA901340F221,
	ObjectiveReachPoint_Awake_m6CEDBCD4390656772D4FB563074D32CC2C41FB7D,
	ObjectiveReachPoint_OnTriggerEnter_m89F95D81CAB2B50052C8508A0870B77E4BE1DE1D,
	ObjectiveReachPoint__ctor_m70C2818350FFCFF4F98FE3E2977B4A7B7EEEB03D,
	OverheatBehavior_Awake_m88CC1B12038FA1ECDD4374CE18F3ACDE70E4D31B,
	OverheatBehavior_Update_m02941BE81C1530D1849538F1FD0B7E7541BF1DCD,
	OverheatBehavior__ctor_m222606A153EC33AEE969E20E2610BC7C0954926F,
	PatrolPath_Start_m5C0E90ED1C3607FE3174D05BE84DB6F1C1FAD771,
	PatrolPath_GetDistanceToNode_m4D4D361354219A81A827B5E47BC3D4277F1A77B0,
	PatrolPath_GetPositionOfPathNode_mE20A3C9887678E96D314BBCB935138F7FFF55FB1,
	PatrolPath_OnDrawGizmosSelected_mE8EE2932EE3CB1FF9036E8BF4D496FD3FC14C523,
	PatrolPath__ctor_m4958B22E3F594B6698BDA3C302BAF84DE8AC720A,
	Pickup_get_pickupRigidbody_mF7725C48939A25BD25AA4809D55EE7B5EC880BB6,
	Pickup_set_pickupRigidbody_m82B725BE80A6565FE573D445CF899B73029F5276,
	Pickup_Start_m486A5E3031CC34D41A9A087669D367AE14B129E7,
	Pickup_Update_mAF9F8DE49E77332FB0F423A9F1554F2194FE2C17,
	Pickup_OnTriggerEnter_m65D9DAB9B1519B0EABA8DF5799B4B6932C39D111,
	Pickup_PlayPickupFeedback_mD48A3AE0650E0A46BE9490336496B59741B0F1FC,
	Pickup__ctor_mD95E5208BC0FF094E8BFA21084232C03DA22AEA8,
	PlayerCharacterController_get_characterVelocity_m8663C69E75E5D8FC4DF1DCEAA5989102FCC5FB14,
	PlayerCharacterController_set_characterVelocity_m72065DCE98A9E5C5566BCC9F81E1ECF03017451D,
	PlayerCharacterController_get_isGrounded_m9F0527F2983A16F97837EAE53359783DE1325E55,
	PlayerCharacterController_set_isGrounded_m86D0907A844B9131C0430C55CCFD906A6401FBA1,
	PlayerCharacterController_get_hasJumpedThisFrame_m61C38C4DC49640CE60C1E2B969691D715FC577B9,
	PlayerCharacterController_set_hasJumpedThisFrame_m70F6273684C020E6A3D2B980C526CEEA9349F14E,
	PlayerCharacterController_get_isDead_m4207FC2D3647D4322683B7C8534E8622AA6B409B,
	PlayerCharacterController_set_isDead_m3BFF6065B90479C6E15D45CEE5B3B2E1D2E99385,
	PlayerCharacterController_get_isCrouching_m644FA07DD2228B9A45DCBDA0404C99D220696C22,
	PlayerCharacterController_set_isCrouching_m82743293FAF8695BBFF43C5539C2D9BE84BD5B57,
	PlayerCharacterController_get_RotationMultiplier_m2AA73E497CD0D0A094A4188EB2AA56F18A8F6B0C,
	PlayerCharacterController_Start_m72E382538592CD782BF9F09FE29CD59B40403776,
	PlayerCharacterController_Update_mAB277B45C69CED773619CCF8FEF3C0D2FB6B0917,
	PlayerCharacterController_OnDie_m8171C9B0CB00F32491CB2852C0E88B6452A1B329,
	PlayerCharacterController_GroundCheck_m66295DEFEADA7AA10A74648C343411FD86CD3281,
	PlayerCharacterController_HandleCharacterMovement_m48CF8C50B0BD075BA617BB8DEAD258C6E58EE596,
	PlayerCharacterController_IsNormalUnderSlopeLimit_mA6F88524DAA67463EFB4E06DBFB64E4FEF5F5CF7,
	PlayerCharacterController_GetCapsuleBottomHemisphere_m33199FF9B6683A6C05FBE76C4012390342E2C5C3,
	PlayerCharacterController_GetCapsuleTopHemisphere_mEC47D28A96A7F28256777721596FE5F5AB4AED5C,
	PlayerCharacterController_GetDirectionReorientedOnSlope_m2BE0B880C7BBDE4D6D57B651B23C24C291F92FA5,
	PlayerCharacterController_UpdateCharacterHeight_m4169751150D07082AFB2B68D7EBEBB454559F589,
	PlayerCharacterController_SetCrouchingState_m17F7E0AC222AC65ADE978612F4DCB5776DF0EA40,
	PlayerCharacterController__ctor_m020FB80AEE5269F9B4DD50F14C1013FF5290F549,
	PlayerInputHandler_Start_mB37ADFEFE34779CE0DA3264EAE85AB6810C036EA,
	PlayerInputHandler_LateUpdate_mC262DB22852296FB69887E85DB088679C46B47E0,
	PlayerInputHandler_CanProcessInput_m71D2D4DF35B349B352E4883514C4AD3C3E5ECD06,
	PlayerInputHandler_GetMoveInput_m0874907ACDB98A02D6D5AD827D22507AD7A0A4AC,
	PlayerInputHandler_GetLookInputsHorizontal_mC286BDDC5BB63A7121162B81BAFAD2556F404EEF,
	PlayerInputHandler_GetLookInputsVertical_m8B79693D83813662238A44DA2F4A070B1C00EF6B,
	PlayerInputHandler_GetJumpInputDown_m1D3389B9ED931B44BA34C0604E1AB1CB3DC7B8CD,
	PlayerInputHandler_GetJumpInputHeld_m0822079C30EA025CCC1DA6E919589E6836FC9C65,
	PlayerInputHandler_GetFireInputDown_m3AA7DA6E4C4E33A2924EEABFD75E49327135BBEE,
	PlayerInputHandler_GetFireInputReleased_mDF51AD29D7341784CA0C7117215B8D7144504D1B,
	PlayerInputHandler_GetFireInputHeld_mE7D176D4FD0E011E69BC6FA83C0C5B144421B5CB,
	PlayerInputHandler_GetAimInputHeld_m67265974BFEA58BF3C4F10D108D3FEE0CBD78737,
	PlayerInputHandler_GetSprintInputHeld_mB06B18A97B10D9774E6DE58A819AD79DF8C3B4A7,
	PlayerInputHandler_GetCrouchInputDown_m581075CAF043220C6D9F08CEC3ECF1A885A92174,
	PlayerInputHandler_GetCrouchInputReleased_m5FAB8C928AACDC2755AD57DE723315194E02D1FD,
	PlayerInputHandler_GetSwitchWeaponInput_mB5F957A1844B7E2A4DD132024D11005FBB1574C6,
	PlayerInputHandler_GetSelectWeaponInput_mC9144A22F02E66E7789C1A76BED19D0C683FEB85,
	PlayerInputHandler_GetMouseOrStickLookAxis_mF5AB346B44FBBC610380D37B0D3AC0DD06415B62,
	PlayerInputHandler__ctor_m57C20066B300D46827395B6EF8C0BBF16963FFF9,
	PlayerWeaponsManager_get_isAiming_mFB413FD2D4DA949F99AA02B1CEDFBCD143285142,
	PlayerWeaponsManager_set_isAiming_m98566CFF8314A968317F8A8A5D5AB04264EF948A,
	PlayerWeaponsManager_get_isPointingAtEnemy_m3A1DA9E73D8926D01C60DF64C84D628D1219F057,
	PlayerWeaponsManager_set_isPointingAtEnemy_m65F60C3E6B8843368F52DA0230F892AE4B58E6F8,
	PlayerWeaponsManager_get_activeWeaponIndex_mC60445132555E8ABA60E7B70B18C05FC15426764,
	PlayerWeaponsManager_set_activeWeaponIndex_m26B271D865F2D37982BA1A39B159D248F08222A9,
	PlayerWeaponsManager_Start_mE7E930BF3D43B68CFD1D11FCB7149C14EE0D09F0,
	PlayerWeaponsManager_Update_m0591AC8C2E84B00421EEB35EF90698A244C9D79F,
	PlayerWeaponsManager_LateUpdate_m942B337CD3E02A3ACE5D029AAA7DB982AC669676,
	PlayerWeaponsManager_SetFOV_m59C9A88B42112BBCF600641E5F7E29EE75073979,
	PlayerWeaponsManager_SwitchWeapon_m08848BFC67A6310BEEC3D2717525745A25997DA2,
	PlayerWeaponsManager_SwitchToWeaponIndex_m90E8F7B77D6A5B4F22C30EE1B0A21EA70541218C,
	PlayerWeaponsManager_HasWeapon_mC35C97D29DDCFD27EEFE9E6694D407AD6BD2CE50,
	PlayerWeaponsManager_UpdateWeaponAiming_mD6D59E18110591B41DDB05D7D5B0E4ECFE66E2FD,
	PlayerWeaponsManager_UpdateWeaponBob_mA595B2F9A5A50ED62610D551A3DF9985D514A642,
	PlayerWeaponsManager_UpdateWeaponRecoil_m03D1298F23D64A1EAEF9A818410AB77B20B985E0,
	PlayerWeaponsManager_UpdateWeaponSwitching_mE3328068C922D4F6D4B357979C5DD3E74BA8B9A4,
	PlayerWeaponsManager_AddWeapon_m6687BD9B7E34BB41035B0CB80BD14EF2488B5761,
	PlayerWeaponsManager_RemoveWeapon_m21B2D70F94961010CBBF7D06EFAB733B43E3FCF3,
	PlayerWeaponsManager_GetActiveWeapon_m2E9EF7BAE1D06DB5CCC560272A3FC7754FBC3ABD,
	PlayerWeaponsManager_GetWeaponAtSlotIndex_mA77611532B28181F22CECA69C018070F3E01C43E,
	PlayerWeaponsManager_GetDistanceBetweenWeaponSlots_m21934D486B1D51E89120697165D8C2BF4D691E26,
	PlayerWeaponsManager_OnWeaponSwitched_m8A6C57AD2B79B9AF6292A6EA3FF7720109C0BE3F,
	PlayerWeaponsManager__ctor_m832E538F3F10E830DD25344E7C6FD7A0E02C0DCA,
	PositionBobbing_Start_mCA16C5241D5CB78AC48BF2A1C24C682F5C5BECFC,
	PositionBobbing_Update_m3E00106DD5B039EE133EFCF5D016E797866EC0B3,
	PositionBobbing__ctor_mD736F650C3500042A22B5C81594BABE6DCF5DD13,
	PrefabReplacer__ctor_mF7077444EBA5EA319691AEE04B91F5DB3346A4D6,
	PrefabReplacerOnInstance_Awake_mDEA1BDDE19B93C319A716055AE9F3D17AA3B43EE,
	PrefabReplacerOnInstance__ctor_mEBB4CF25658DA9D66512DC953BA3700AF11BF87F,
	ProjectileBase_get_owner_m0D8944A73FA74A300254F220E3DE3665808D939C,
	ProjectileBase_set_owner_m0E90ACDDB16FC31FF73AC5A1D5E0057B919F1F7E,
	ProjectileBase_get_initialPosition_m096583D273B9235CA596B4D39D3C15B566494DEE,
	ProjectileBase_set_initialPosition_m440F9E9B16B35DC1F45547C75F2AD6E91EDF2005,
	ProjectileBase_get_initialDirection_mC654160B1F74E30F794B4BBBAFB6AB5BC897D080,
	ProjectileBase_set_initialDirection_mEBE01C46D9C31C10DB2E7B1556092307ADA3CB56,
	ProjectileBase_get_inheritedMuzzleVelocity_m9D123BE639A3E43C3D9E36481BA7CDF50F2503FC,
	ProjectileBase_set_inheritedMuzzleVelocity_m1BD5922A4515C89AFCCB22EFB49B2D7EE0B6D140,
	ProjectileBase_get_initialCharge_m0744E2064FAD08E27215AB586DE16DE9FABB2280,
	ProjectileBase_set_initialCharge_m0933C15ED25C0A8D3C0C75D9E21C59F4AC9E829B,
	ProjectileBase_Shoot_m335907E7139D7DD9185EF5BCF24007F38BF7BF08,
	ProjectileBase__ctor_mA5383CE6E44783561078E5FCEFA95429F4582F58,
	ProjectileChargeParameters_OnEnable_m42640373A6D79BA7D5BD14F96497A8C13874DD15,
	ProjectileChargeParameters_OnShoot_m23F2BC2222323634AFC18FBDD6ECBFA6D1B78CB0,
	ProjectileChargeParameters__ctor_mFDB6FD8B7845D817AFD8927C4D7E57366C36B39E,
	ProjectileStandard_OnEnable_mF2AFA986D8F0315C686F74247E613FDEB6B8BE77,
	ProjectileStandard_OnShoot_mABB31453B22ABF9279BEE931C5F8E89CEFBF990B,
	ProjectileStandard_Update_m45EE6941B5A6805A3592393E0989CFF7C36EE2E1,
	ProjectileStandard_IsHitValid_m0B69A299205AA8A823E051766106B86A40BD5BC3,
	ProjectileStandard_OnHit_m2082385E2E0B3B99D108E331F4D05129762272E8,
	ProjectileStandard_OnDrawGizmosSelected_m7CEB7D2AC125357E8AA3DDEA61D62BC5876052AB,
	ProjectileStandard__ctor_m56478F25CDF49B7BCC1439C516CCE6980EAA9027,
	TakeScreenshot_getPath_m0ED442178774BC69B513CA8C7B0AC69E55164471,
	TakeScreenshot_Awake_m8568E10F72FA9C9C01822086C7A4DB3309236039,
	TakeScreenshot_Update_m3229FBCB16C43ACF9D2D8CCC5568FCBF66879601,
	TakeScreenshot_OnTakeScreenshotButtonPressed_m041F5F9B6135B9D9F69E471E6C444C8AFC94BCAD,
	TakeScreenshot_LoadScreenshot_mDF1BA948BCCA32102807D2FDD1567CE4E7C5BDA2,
	TakeScreenshot__ctor_m78BAD2A0CEC02FE3F379CF8D4E9AFB31EA8AA0A3,
	TeleportPlayer_Awake_m6669BD20DC7DA542120776A256F933618F1FC13F,
	TeleportPlayer_Update_mAA1EC30FC2C20D736FCF4BDF8BDB61ED7906A4DA,
	TeleportPlayer__ctor_mF8D41A4C761A5048189B87E45AF53ABB07287DCD,
	TemplateObject_Update_m6A4FC11E8BA9014440AEAD83A477727928A03006,
	TemplateObject__ctor_mD98217FCAFD6FE79075042484FA36699E93A25CF,
	TimedSelfDestruct_Awake_mDAE061EBCEF2F199837B76B3B76F68FBD35F10A3,
	TimedSelfDestruct_Update_mE6FDC9D11CA8FBF8641E33699524E11A9687C021,
	TimedSelfDestruct__ctor_mB65A7D642EFD0DA0580F8E94AE83F5F8BD42A6F9,
	AmmoCounter_get_weaponCounterIndex_mB3100D3B5713CA73962911C3D47D0E84BD4584B6,
	AmmoCounter_set_weaponCounterIndex_mA45DE6A1F4EF1860A94F65EDA63CD49E6E924758,
	AmmoCounter_Initialize_m8213538E0F5CCEF79F8FF69501026966062B9FAB,
	AmmoCounter_Update_m7900C8AE682D18C4BC181FB697F559027B2C8C7E,
	AmmoCounter__ctor_mC0ED0E5878DED33D8D8BAE4E2F6A1629FA6827C0,
	Compass_Awake_m305AC7675BE6B58252E3742C6FDDBE08009CCDF8,
	Compass_Update_m8E250BBFD26E03985A45673212E543971EA6E234,
	Compass_RegisterCompassElement_m745267F45655F5E8EBCCB026EE4ECA5382491BAA,
	Compass_UnregisterCompassElement_mDE678F73CAB089285882780485C784DB982896AC,
	Compass__ctor_mBAF5AD608A7C804C96BB5874F61606789B0C2B3F,
	CompassElement_Awake_m1E05A93653775FA76B561B82A934B573F0A19183,
	CompassElement_OnDestroy_mF8F5A47FC58A88888A82BC9658C5AB34EB1B9867,
	CompassElement__ctor_m460AF29D4D3B9ABC0921035C988E823E8B9473A6,
	CompassMarker_Initialize_m7105288E83CBC8975FD49217D771C8FDD23469E5,
	CompassMarker_DetectTarget_mB2F4469725C370B56C2A643C873150DE37BDF15D,
	CompassMarker_LostTarget_mBEAFF72693D333B1218030D0D82ABAF573DA7EDC,
	CompassMarker__ctor_m0E60B5B6AA329B60D6AAB01D68C426400C0F4774,
	CrosshairManager_Start_mDC5EAD0555029FC48547C70080088A8BA1879C81,
	CrosshairManager_Update_mDB80E5FB668CE7FAB8EC7D5451EDCB7DD8B55A3C,
	CrosshairManager_UpdateCrosshairPointingAtEnemy_m37048969418927A30A9A7D3F6E446FD0E0507E4F,
	CrosshairManager_OnWeaponChanged_m0BE29994ACF2A98ACBC4AB3F2B772D04F5A0E7E7,
	CrosshairManager__ctor_mBBDEF19E2F95C020D43B16DACEE2A84B21B91E17,
	DisplayMessage_Start_m5B3FC0C2BB5875C621F39BE5B70B0AF87C44AE41,
	DisplayMessage_Update_m151CB05BB8024D49F65B9ABA1F58E9F1C49DD836,
	DisplayMessage__ctor_m22ED3CA685FB2C57B21732FD411C01DCA759E64B,
	DisplayMessageManager__ctor_m690CC30984FCF0B2499C84D8015C3D61BBCEE5AA,
	EnemyCounter_Awake_mB5A95CF9ACD40A0B05F626268F9EFE194CD9B29A,
	EnemyCounter_Update_mB279D0E020CC58E826C37D02E97EE5C13D9B335A,
	EnemyCounter__ctor_m1079EEC6381D6C35CCFFB8718E9E31CD72D2E379,
	FeedbackFlashHUD_Start_m122C035796C6E5DFC466AE320822A2812330C2B7,
	FeedbackFlashHUD_Update_m977B90332CDC31531B6CE1C29F1919BE09610CE8,
	FeedbackFlashHUD_ResetFlash_m35DBBCA88523D88897646DA8E1A1F4D4D0887795,
	FeedbackFlashHUD_OnTakeDamage_mC6E5F4FC013BC131FD8E6E2AF1178B4AD4041D55,
	FeedbackFlashHUD_OnHealed_mABF624B767DB393577A63E55CB44AA00DB5DBA4E,
	FeedbackFlashHUD__ctor_mD3C0C0F5746097D1B362E83C01DC13C20B203481,
	FillBarColorChange_Initialize_m9B1B1C0CE415AA4862C7C5F19B66B76C32EDB1ED,
	FillBarColorChange_UpdateVisual_m1B35DF56805D89C6D0BF43E0D4346AA0BF7E8830,
	FillBarColorChange__ctor_mF5E65AC21CE36C375D52BD6004FE1BCCCE832BB7,
	JetpackCounter_Awake_mC5A4375272BBEBE819686708BC380A2F6B00E92B,
	JetpackCounter_Update_m8194C740C674F40A453FA7FF1301FCEF74ED6A97,
	JetpackCounter__ctor_mD751CE07F2BFA2389BDC1C0E12027F2CE4C47C4A,
	LoadSceneButton_Update_mC7A1DDD605F49C7394A7C06D4292DE00175D1289,
	LoadSceneButton_LoadTargetScene_m030672AC4E2107C510F44DA0F39049B6C24D2776,
	LoadSceneButton__ctor_m3E912B5D57F0C196F6A4AB7DB8F9A527BFFD61F5,
	MenuNavigation_Start_mF31F57B5C995FAB7D18CCDEE22C2333742BFC871,
	MenuNavigation_LateUpdate_m41F4C71AFF6114D8D938D6B4A15CFD7C19BA8B7E,
	MenuNavigation__ctor_mE1485B5092DF6AFEFE5D34BB328320633B988047,
	NotificationHUDManager_Awake_m1638F07707309F5B2B3C6EB638DEBCCD08F03A68,
	NotificationHUDManager_OnUpdateObjective_m4CAAC870601FF5062D8C3FE25A525077847CB1B9,
	NotificationHUDManager_OnPickupWeapon_m5B44AD36A993F6313A100AC5D232E41ED1C3A58E,
	NotificationHUDManager_OnUnlockJetpack_mC7AECB1A7E9AA0E6213CC2BF9BAE689C6E343B61,
	NotificationHUDManager_CreateNotification_mA49A3AB6182D005997318F39BCFF34A7EDA1A38E,
	NotificationHUDManager_RegisterObjective_mAED484DCF0731E6506EF9C3AC117A283CB2443B5,
	NotificationHUDManager_UnregisterObjective_mCE4E5799A4D970BD24C95FD93515EAE947093623,
	NotificationHUDManager__ctor_m7163EA3A42A92C5102DF649734C4449DF36F6502,
	NotificationToast_Initialize_m9E521B69E8806DC18EF0073CE9994E9C327CF912,
	NotificationToast_Update_m0260AFB121B72BCC83CFD149E28EE9B12AA23CCF,
	NotificationToast__ctor_m2D2B8F4FEFD22C3CEC3305C426952C9042463204,
	ObjectiveHUDManger_Awake_mF234DEB699B1548C5364FC49A73ABC905C72F4E7,
	ObjectiveHUDManger_RegisterObjective_m0C18B94122EBA9CCAFB3DC1C2194953CF932801B,
	ObjectiveHUDManger_UnregisterObjective_m241D7B3A88A8D2D3D542AB78479C700537C39656,
	ObjectiveHUDManger_OnUpdateObjective_mD32BD0EA9F81EA7A85AA0CF52F983B542FC6AD4E,
	ObjectiveHUDManger__ctor_m5179F38AF0E9997D8EF37891F4D6293AF7090608,
	ObjectiveToast_Initialize_mBA94C5A27D955591494ED878F5248BA02F950BBC,
	ObjectiveToast_Complete_m28F479DDCA0F4346B1638DBED4CADD12B00BA0F4,
	ObjectiveToast_Update_mF394491E029329DEBFA82E4C4FCC4CE9CD818026,
	ObjectiveToast_PlaySound_m2DC26ADEA26B22A58915050ECADB6717946763A3,
	ObjectiveToast__ctor_m28F49E032867B2084A5103184B1B308191B39106,
	PlayerHealthBar_Start_mCF5EACA3F6C7B2074B55001FF47CCC13EACE47E4,
	PlayerHealthBar_Update_mB81AC7EDB185CEBA07A859DF872DC67086AF24CD,
	PlayerHealthBar__ctor_m0F2FA58075B89822E044627CC28DC6B2B860D94C,
	StanceHUD_Start_m814DF36C577F00B1EE8287AFFFC7672E98C91360,
	StanceHUD_OnStanceChanged_m297398277ABA475A4A25B5B5676580F0E1EE58B0,
	StanceHUD__ctor_m3BEAB585F0B255C086E26580A5247D1671CE15CD,
	ToggleGameObjectButton_Update_m4B4A607C06471F694EDCB9CFDF43D2738A6AB8DE,
	ToggleGameObjectButton_SetGameObjectActive_m69EC0360AFD97A4CF1DADFE5A6B754A4189B6431,
	ToggleGameObjectButton__ctor_m5856E6CBB56BE7A64AE6EB112F25A316CEFBB74E,
	WeaponHUDManager_Start_mB3691A6FADA9D605A53BD1B56E24442CBF7B97CC,
	WeaponHUDManager_AddWeapon_m40F4058D81FFCA0640432F656F1506825190559D,
	WeaponHUDManager_RemoveWeapon_m01F57E6BDDB655C20A2308D7D15FD8201AE58F15,
	WeaponHUDManager_ChangeWeapon_mF6F576F5B15E561DF5D09CCE7417BA699E072683,
	WeaponHUDManager__ctor_m1B5D93240B1B0EDD834D7294AD637F936C66D180,
	WorldspaceHealthBar_Update_mE1B2CE097FF63BDA7D2EA4A5D538FBF65BF676B8,
	WorldspaceHealthBar__ctor_m9DD18744523EDD803B3A75898E4EDEF38DFC17FE,
	WeaponController_get_LastChargeTriggerTimestamp_mC77917468FADC1A568BD9E2A4D378028B1765DF3,
	WeaponController_set_LastChargeTriggerTimestamp_mB3ECE4004948C0D4C3E11CCA701E269F95A7AF06,
	WeaponController_get_owner_mB5CA66395D4AA5B45E4FFF61DA49BB70F487281D,
	WeaponController_set_owner_m5C399CEA370A39BFB2AA8564276F43BA92464BB2,
	WeaponController_get_sourcePrefab_m9B0A30B7ADE6CBA36E7CE2388380C32D91E56BA6,
	WeaponController_set_sourcePrefab_m7FA17C97D95A300D980081F26E3013BB4C8964CA,
	WeaponController_get_isCharging_m62B0A4EC535BDD25071F76C653DBE76ED73DA0D9,
	WeaponController_set_isCharging_m959B528A094C572AA6F8AC5852699F3A17795943,
	WeaponController_get_currentAmmoRatio_mF7180E6799FB3AD19BE14019CE98B6A12125C842,
	WeaponController_set_currentAmmoRatio_mAD3632A3E72BFCC9A2D3718C08FB485F325A28E3,
	WeaponController_get_isWeaponActive_mAA00F2995C9212A16626E57B1BCC0BD561E0214F,
	WeaponController_set_isWeaponActive_mCC1C68A19A675E4D9E2389F239F1EE22FDF04B6F,
	WeaponController_get_isCooling_m943D42CD96EC99A45FBD6756D523D06B4D3BA99F,
	WeaponController_set_isCooling_m2C9857D7145DBFA34B40B8214EBB0C83B8963AE5,
	WeaponController_get_currentCharge_m7EA8715BC01C38F1117870CE39BE125435D0A814,
	WeaponController_set_currentCharge_m93D527A49E4A95A9221B4638BEEFBDEC0830F188,
	WeaponController_get_muzzleWorldVelocity_mA018C743F8C5C5E327E7461F8E8B90FB33054875,
	WeaponController_set_muzzleWorldVelocity_m91E8B1C8BD47AAFE9C6DA5F6A3AEB16B86E6D929,
	WeaponController_GetAmmoNeededToShoot_mE7B0C102AB5B87D7A0300F26374905AB5A788947,
	WeaponController_Awake_m05B2535AD72C22C7AFCE1496517B79F7F588C570,
	WeaponController_Update_mDCF4F60C5E354DE8062B0FC860E0ED5AB36B1525,
	WeaponController_UpdateAmmo_mAC4A086C81686FB4C5B20412207B2DA2559C4905,
	WeaponController_UpdateCharge_mC93687464F57F5F5C1D025FCEE804F8D2DEDC164,
	WeaponController_UpdateContinuousShootSound_mCC5EDFC5C83B465E54851ADE78C282A4974F0B3B,
	WeaponController_ShowWeapon_m8E2AB0EC2D103F7C5500F646FC84BAFC4E99BDFC,
	WeaponController_UseAmmo_m869CCDD903D481785BCCD76FFF0E4347E3287D44,
	WeaponController_HandleShootInputs_m0F1DE552B0C1155C55B95A703017FF0465A7F01C,
	WeaponController_TryShoot_m5A9B3341B62A4E46E7B3CCF6C12FAD9D93DED291,
	WeaponController_TryBeginCharge_m5D39B03E72C21C4C741A5B125D8A486F5435D014,
	WeaponController_TryReleaseCharge_mDECC76CD65B14EAC78E937E40E78AF5DF1348654,
	WeaponController_HandleShoot_m0C5F723C040002F4CC6E6BDE3EF634313678B018,
	WeaponController_GetShotDirectionWithinSpread_mA1A165FB9F62FBDDB906CA9C907129EA9D7D54CA,
	WeaponController__ctor_mEF2EFCA70472BEE760C56586F4B3C7637BDB8512,
	WeaponFuelCellHandler_Start_mC5BDF5D098F7138A800B85BA4D934514C8DDD3FC,
	WeaponFuelCellHandler_Update_mDB4277536619C89299A9072410D1462BADEC4A47,
	WeaponFuelCellHandler__ctor_m5D0C4964CB7F4C149BFEE22852F4FB224F4BF4AA,
	WeaponPickup_Start_mDD98BD5363E0B4D89AA720E4BD6D942924A5A7DB,
	WeaponPickup_OnPicked_m19C08F01C850D8A2FA89CC1A961006A541A2AF6C,
	WeaponPickup__ctor_m1ADFB171B8333C4EA16C3FB0B09F861548EB206E,
	RendererIndexData__ctor_m74AF941246DC215455016DBAD4A6C1B235780F2F_AdjustorThunk,
	RenderBatchData__ctor_m1FE81D444573AFA9AE4D37FC12A38AFA31E4C2A3,
	RendererIndexData__ctor_m0E73078E1BC53CC52A683EC27A8D83CB0862AC3E_AdjustorThunk,
	MeshAndTRS__ctor_m3CD5F70F5E811604883E3A0A6B23C7C441DB83E6,
};
static const int32_t s_InvokerIndices[458] = 
{
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	28,
	828,
	565,
	23,
	1616,
	43,
	1145,
	1023,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1617,
	23,
	23,
	14,
	26,
	23,
	1618,
	23,
	-1,
	-1,
	-1,
	-1,
	23,
	1619,
	23,
	23,
	14,
	26,
	102,
	31,
	102,
	31,
	102,
	31,
	23,
	27,
	23,
	23,
	26,
	23,
	23,
	14,
	26,
	14,
	102,
	102,
	102,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	1001,
	102,
	23,
	23,
	1000,
	1001,
	31,
	1619,
	23,
	23,
	1001,
	1010,
	102,
	23,
	14,
	32,
	23,
	14,
	26,
	10,
	32,
	10,
	23,
	26,
	26,
	23,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	1619,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	102,
	31,
	23,
	23,
	31,
	23,
	654,
	275,
	102,
	31,
	102,
	654,
	102,
	23,
	275,
	1619,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	275,
	31,
	31,
	31,
	31,
	23,
	654,
	275,
	102,
	31,
	102,
	23,
	23,
	102,
	23,
	23,
	26,
	23,
	425,
	1620,
	23,
	23,
	1622,
	10,
	1478,
	23,
	23,
	985,
	1071,
	1005,
	23,
	102,
	31,
	102,
	23,
	156,
	156,
	23,
	1623,
	23,
	124,
	14,
	23,
	102,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	1624,
	1625,
	23,
	23,
	14,
	26,
	23,
	23,
	26,
	23,
	23,
	1000,
	1001,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	654,
	23,
	23,
	23,
	23,
	23,
	1010,
	1000,
	1005,
	1626,
	31,
	594,
	23,
	23,
	23,
	102,
	1000,
	654,
	654,
	102,
	102,
	102,
	102,
	102,
	102,
	102,
	102,
	102,
	10,
	10,
	1627,
	23,
	102,
	31,
	102,
	31,
	10,
	32,
	23,
	23,
	23,
	275,
	31,
	569,
	9,
	23,
	23,
	23,
	23,
	9,
	9,
	14,
	34,
	723,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	1000,
	1001,
	1000,
	1001,
	1000,
	1001,
	654,
	275,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1628,
	1629,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	32,
	124,
	23,
	23,
	23,
	23,
	27,
	26,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	31,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1619,
	275,
	23,
	1086,
	275,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	124,
	31,
	26,
	26,
	26,
	23,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	1630,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	31,
	23,
	23,
	124,
	124,
	26,
	23,
	23,
	23,
	654,
	275,
	14,
	26,
	14,
	26,
	102,
	31,
	654,
	275,
	102,
	31,
	102,
	31,
	654,
	275,
	1000,
	1001,
	654,
	23,
	23,
	23,
	23,
	23,
	31,
	275,
	1631,
	102,
	102,
	102,
	23,
	1632,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	124,
	23,
	124,
	1621,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	458,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
