﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AI.NavMeshBuilder::CollectSources(UnityEngine.Bounds,System.Int32,UnityEngine.AI.NavMeshCollectGeometry,System.Int32,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildMarkup>,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>)
extern void NavMeshBuilder_CollectSources_m11DFC2DE0CD5165927B83ADA915E026D2A2BADAE ();
// 0x00000002 System.Void UnityEngine.AI.NavMeshBuilder::CollectSources(UnityEngine.Transform,System.Int32,UnityEngine.AI.NavMeshCollectGeometry,System.Int32,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildMarkup>,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>)
extern void NavMeshBuilder_CollectSources_m73C0100A28595C935428590A4A469D80C44D4C67 ();
// 0x00000003 UnityEngine.AI.NavMeshBuildSource[] UnityEngine.AI.NavMeshBuilder::CollectSourcesInternal(System.Int32,UnityEngine.Bounds,UnityEngine.Transform,System.Boolean,UnityEngine.AI.NavMeshCollectGeometry,System.Int32,UnityEngine.AI.NavMeshBuildMarkup[])
extern void NavMeshBuilder_CollectSourcesInternal_m83D18AA37DB1651C5DACE705C60D2FCF79D52C43 ();
// 0x00000004 UnityEngine.AI.NavMeshData UnityEngine.AI.NavMeshBuilder::BuildNavMeshData(UnityEngine.AI.NavMeshBuildSettings,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>,UnityEngine.Bounds,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NavMeshBuilder_BuildNavMeshData_m346D7D0C459AB4859E03D4989FB0955151D19389 ();
// 0x00000005 System.Boolean UnityEngine.AI.NavMeshBuilder::UpdateNavMeshDataListInternal(UnityEngine.AI.NavMeshData,UnityEngine.AI.NavMeshBuildSettings,System.Object,UnityEngine.Bounds)
extern void NavMeshBuilder_UpdateNavMeshDataListInternal_mC137D6C654B5479D835C7B98611F4EA84E3A5B2C ();
// 0x00000006 UnityEngine.AsyncOperation UnityEngine.AI.NavMeshBuilder::UpdateNavMeshDataAsync(UnityEngine.AI.NavMeshData,UnityEngine.AI.NavMeshBuildSettings,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>,UnityEngine.Bounds)
extern void NavMeshBuilder_UpdateNavMeshDataAsync_m953992EDF8FE5C791858CD043F0250138351010B ();
// 0x00000007 UnityEngine.AsyncOperation UnityEngine.AI.NavMeshBuilder::UpdateNavMeshDataAsyncListInternal(UnityEngine.AI.NavMeshData,UnityEngine.AI.NavMeshBuildSettings,System.Object,UnityEngine.Bounds)
extern void NavMeshBuilder_UpdateNavMeshDataAsyncListInternal_m374D1C5CE452D36E47F006C2DDF8C5689D6CF3D0 ();
// 0x00000008 UnityEngine.AI.NavMeshBuildSource[] UnityEngine.AI.NavMeshBuilder::CollectSourcesInternal_Injected(System.Int32,UnityEngine.Bounds&,UnityEngine.Transform,System.Boolean,UnityEngine.AI.NavMeshCollectGeometry,System.Int32,UnityEngine.AI.NavMeshBuildMarkup[])
extern void NavMeshBuilder_CollectSourcesInternal_Injected_mA0A78D3BE843386FF640205FA025D974976848BC ();
// 0x00000009 System.Boolean UnityEngine.AI.NavMeshBuilder::UpdateNavMeshDataListInternal_Injected(UnityEngine.AI.NavMeshData,UnityEngine.AI.NavMeshBuildSettings&,System.Object,UnityEngine.Bounds&)
extern void NavMeshBuilder_UpdateNavMeshDataListInternal_Injected_m14208EC28E1F5BDD8A5CDB5CD1913BFA2C5FD4EC ();
// 0x0000000A UnityEngine.AsyncOperation UnityEngine.AI.NavMeshBuilder::UpdateNavMeshDataAsyncListInternal_Injected(UnityEngine.AI.NavMeshData,UnityEngine.AI.NavMeshBuildSettings&,System.Object,UnityEngine.Bounds&)
extern void NavMeshBuilder_UpdateNavMeshDataAsyncListInternal_Injected_mBC3CEBC66EF10C73A496CC21F12C23D112CA70D6 ();
// 0x0000000B System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern void NavMeshAgent_SetDestination_mDCFFAB501EFCC89E17EA13F49A917D340AEF0BC8 ();
// 0x0000000C UnityEngine.Vector3 UnityEngine.AI.NavMeshAgent::get_velocity()
extern void NavMeshAgent_get_velocity_m1CF6CC11732234E2E3EC3B778B79C5492A990186 ();
// 0x0000000D System.Single UnityEngine.AI.NavMeshAgent::get_speed()
extern void NavMeshAgent_get_speed_mED796DC28B1B5D848CA96E1831DA12696A595AC6 ();
// 0x0000000E System.Void UnityEngine.AI.NavMeshAgent::set_speed(System.Single)
extern void NavMeshAgent_set_speed_mE709E788BFB285190B9504F6DB3199B35B9B0D35 ();
// 0x0000000F System.Void UnityEngine.AI.NavMeshAgent::set_angularSpeed(System.Single)
extern void NavMeshAgent_set_angularSpeed_m90DB947AF61F1AEA798FF5194961B2DA63219FC0 ();
// 0x00000010 System.Void UnityEngine.AI.NavMeshAgent::set_acceleration(System.Single)
extern void NavMeshAgent_set_acceleration_mA37022F97D535F75B34C7D07FA50A8D89639F3F7 ();
// 0x00000011 System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_SetDestination_Injected_m03F00018E71D91B109510C2EF1F01A3ECD84FF89 ();
// 0x00000012 System.Void UnityEngine.AI.NavMeshAgent::get_velocity_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_get_velocity_Injected_mBBFC5D1D8F50C16F9ACD5FCC0A0E50A0FE525FB3 ();
// 0x00000013 System.Void UnityEngine.AI.NavMeshData::.ctor(System.Int32)
extern void NavMeshData__ctor_mFFCE3983B9C075BF2CFAFD20CCB1F1F9EE860010 ();
// 0x00000014 System.Void UnityEngine.AI.NavMeshData::Internal_Create(UnityEngine.AI.NavMeshData,System.Int32)
extern void NavMeshData_Internal_Create_m5EB9327216B5DAF47D57F469F798E4583F302CF3 ();
// 0x00000015 System.Void UnityEngine.AI.NavMeshData::set_position(UnityEngine.Vector3)
extern void NavMeshData_set_position_m70F95E20AE46E773E19D73A3FEFBFCABD1CF061A ();
// 0x00000016 System.Void UnityEngine.AI.NavMeshData::set_rotation(UnityEngine.Quaternion)
extern void NavMeshData_set_rotation_mF90B0D2ED475B0DD536BA43B4C00AACAEEA7449F ();
// 0x00000017 System.Void UnityEngine.AI.NavMeshData::set_position_Injected(UnityEngine.Vector3&)
extern void NavMeshData_set_position_Injected_m5B27DA09BC1D1AAB6C774BBFAA4EF1143BADB196 ();
// 0x00000018 System.Void UnityEngine.AI.NavMeshData::set_rotation_Injected(UnityEngine.Quaternion&)
extern void NavMeshData_set_rotation_Injected_m05B6C44C78BC6C4748086D4ED000594BF9093122 ();
// 0x00000019 System.Boolean UnityEngine.AI.NavMeshDataInstance::get_valid()
extern void NavMeshDataInstance_get_valid_m4334E80C0A66334F366EBF69DCB634E86DDFA05B_AdjustorThunk ();
// 0x0000001A System.Int32 UnityEngine.AI.NavMeshDataInstance::get_id()
extern void NavMeshDataInstance_get_id_mEA3B300C16CC2B995641461BE9E69BEEF60BC2EF_AdjustorThunk ();
// 0x0000001B System.Void UnityEngine.AI.NavMeshDataInstance::set_id(System.Int32)
extern void NavMeshDataInstance_set_id_mACF5FCAB5F05DAF0F2C64C3670141DB2598B7DE9_AdjustorThunk ();
// 0x0000001C System.Void UnityEngine.AI.NavMeshDataInstance::Remove()
extern void NavMeshDataInstance_Remove_m6B51624B186D5F570D115A6205BBF58CBA60A837_AdjustorThunk ();
// 0x0000001D System.Void UnityEngine.AI.NavMeshDataInstance::set_owner(UnityEngine.Object)
extern void NavMeshDataInstance_set_owner_m80A0BFBF4A375008A5F9CD126A520F7DF66333FD_AdjustorThunk ();
// 0x0000001E System.Void UnityEngine.AI.NavMeshLinkData::set_startPosition(UnityEngine.Vector3)
extern void NavMeshLinkData_set_startPosition_mB28631278DA2678D228FD7C917012068ADFF05F5_AdjustorThunk ();
// 0x0000001F System.Void UnityEngine.AI.NavMeshLinkData::set_endPosition(UnityEngine.Vector3)
extern void NavMeshLinkData_set_endPosition_m2D2051E1F6166CA3C803B5B93ADAE0DAE7F6C5F9_AdjustorThunk ();
// 0x00000020 System.Void UnityEngine.AI.NavMeshLinkData::set_costModifier(System.Single)
extern void NavMeshLinkData_set_costModifier_m1552A1199CE5EFBA60C60AE601A3E43E4361ED9B_AdjustorThunk ();
// 0x00000021 System.Void UnityEngine.AI.NavMeshLinkData::set_bidirectional(System.Boolean)
extern void NavMeshLinkData_set_bidirectional_m1AC3CAD1CEFE47FE7B36E4403F7036B819199289_AdjustorThunk ();
// 0x00000022 System.Void UnityEngine.AI.NavMeshLinkData::set_width(System.Single)
extern void NavMeshLinkData_set_width_m8473F9DE6D05FD86BA04DF84AA593788B645870D_AdjustorThunk ();
// 0x00000023 System.Void UnityEngine.AI.NavMeshLinkData::set_area(System.Int32)
extern void NavMeshLinkData_set_area_m9833F712659EA6D789487F1C78DE0B00599FCE67_AdjustorThunk ();
// 0x00000024 System.Void UnityEngine.AI.NavMeshLinkData::set_agentTypeID(System.Int32)
extern void NavMeshLinkData_set_agentTypeID_m9D7FC516E796D8F5B84D88CBFF2865D54123A9E7_AdjustorThunk ();
// 0x00000025 System.Boolean UnityEngine.AI.NavMeshLinkInstance::get_valid()
extern void NavMeshLinkInstance_get_valid_mE143382F0205F0BD104CAA0214666C7A307D2608_AdjustorThunk ();
// 0x00000026 System.Int32 UnityEngine.AI.NavMeshLinkInstance::get_id()
extern void NavMeshLinkInstance_get_id_m8B494FAE3B97FED8177D20816D541EB271BC9363_AdjustorThunk ();
// 0x00000027 System.Void UnityEngine.AI.NavMeshLinkInstance::set_id(System.Int32)
extern void NavMeshLinkInstance_set_id_m48D09FEDF41895E1EA92FE794811113565F4281C_AdjustorThunk ();
// 0x00000028 System.Void UnityEngine.AI.NavMeshLinkInstance::Remove()
extern void NavMeshLinkInstance_Remove_mF440F826BECB512A1BEBB8648E64ACFA7B3D0830_AdjustorThunk ();
// 0x00000029 System.Void UnityEngine.AI.NavMeshLinkInstance::set_owner(UnityEngine.Object)
extern void NavMeshLinkInstance_set_owner_m2DF837AD84594C78E22CDC6636CF51B599E722D6_AdjustorThunk ();
// 0x0000002A System.Void UnityEngine.AI.NavMesh::Internal_CallOnNavMeshPreUpdate()
extern void NavMesh_Internal_CallOnNavMeshPreUpdate_mED6CAB94A6CB61A5FD547B7026DB4C96F2AF5B60 ();
// 0x0000002B UnityEngine.AI.NavMeshDataInstance UnityEngine.AI.NavMesh::AddNavMeshData(UnityEngine.AI.NavMeshData,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NavMesh_AddNavMeshData_m8644DBCE7C1C3D201DF09DE38C2DAC45459F4380 ();
// 0x0000002C System.Boolean UnityEngine.AI.NavMesh::IsValidNavMeshDataHandle(System.Int32)
extern void NavMesh_IsValidNavMeshDataHandle_m84E79E847B847D87FF4D98212D9E932F1837E815 ();
// 0x0000002D System.Boolean UnityEngine.AI.NavMesh::IsValidLinkHandle(System.Int32)
extern void NavMesh_IsValidLinkHandle_m76E4AB70D416D76158489A407F032BF079A8153F ();
// 0x0000002E System.Boolean UnityEngine.AI.NavMesh::InternalSetOwner(System.Int32,System.Int32)
extern void NavMesh_InternalSetOwner_m9C57A11AE2C80BC6ADC251267F4065556D7BF5B8 ();
// 0x0000002F System.Boolean UnityEngine.AI.NavMesh::InternalSetLinkOwner(System.Int32,System.Int32)
extern void NavMesh_InternalSetLinkOwner_m9825A197AEA5EAFF4A196CBC561502DED835080D ();
// 0x00000030 System.Int32 UnityEngine.AI.NavMesh::AddNavMeshDataTransformedInternal(UnityEngine.AI.NavMeshData,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NavMesh_AddNavMeshDataTransformedInternal_m00CF5EA76AE64FE7D26B050D0B56F4D4D31167B6 ();
// 0x00000031 System.Void UnityEngine.AI.NavMesh::RemoveNavMeshDataInternal(System.Int32)
extern void NavMesh_RemoveNavMeshDataInternal_m653550983DDCEE84C44F0EF041D36F455E287C6B ();
// 0x00000032 UnityEngine.AI.NavMeshLinkInstance UnityEngine.AI.NavMesh::AddLink(UnityEngine.AI.NavMeshLinkData,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NavMesh_AddLink_mC8E70FAB247C2E33213BBE436E17D12F524114C3 ();
// 0x00000033 System.Int32 UnityEngine.AI.NavMesh::AddLinkInternal(UnityEngine.AI.NavMeshLinkData,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NavMesh_AddLinkInternal_m1E1038561F7A53755E589BDED731CE87DE18C776 ();
// 0x00000034 System.Void UnityEngine.AI.NavMesh::RemoveLinkInternal(System.Int32)
extern void NavMesh_RemoveLinkInternal_mAE7CD6A1EC37829AB36C9829B5E2848686A8686C ();
// 0x00000035 UnityEngine.AI.NavMeshBuildSettings UnityEngine.AI.NavMesh::GetSettingsByID(System.Int32)
extern void NavMesh_GetSettingsByID_m9B61F5191DBBE8D7D595D17FD10EBBCAD8881D05 ();
// 0x00000036 System.Int32 UnityEngine.AI.NavMesh::AddNavMeshDataTransformedInternal_Injected(UnityEngine.AI.NavMeshData,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void NavMesh_AddNavMeshDataTransformedInternal_Injected_m4ACC2049A75503A225529F3EAF1DCE61DDAB8786 ();
// 0x00000037 System.Int32 UnityEngine.AI.NavMesh::AddLinkInternal_Injected(UnityEngine.AI.NavMeshLinkData&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void NavMesh_AddLinkInternal_Injected_m4F49F4146BD7867BA3456B0FE3636063FC4A21E6 ();
// 0x00000038 System.Void UnityEngine.AI.NavMesh::GetSettingsByID_Injected(System.Int32,UnityEngine.AI.NavMeshBuildSettings&)
extern void NavMesh_GetSettingsByID_Injected_m3F525258D1AB71E7A5BAF7D354BD117C60AB81DB ();
// 0x00000039 System.Void UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::.ctor(System.Object,System.IntPtr)
extern void OnNavMeshPreUpdate__ctor_mD019C429BD8D299B85C320A6EFB2FFEDC3F85F42 ();
// 0x0000003A System.Void UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::Invoke()
extern void OnNavMeshPreUpdate_Invoke_mE56CD30B200FECFD94AD4B22923B32BD789D70F0 ();
// 0x0000003B System.IAsyncResult UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnNavMeshPreUpdate_BeginInvoke_m67FA7767274E77169A57ADFE041EA9B914E752C6 ();
// 0x0000003C System.Void UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::EndInvoke(System.IAsyncResult)
extern void OnNavMeshPreUpdate_EndInvoke_mB55765702AA123A6D7C3DF8DDC597E3DEBC79836 ();
// 0x0000003D UnityEngine.Matrix4x4 UnityEngine.AI.NavMeshBuildSource::get_transform()
extern void NavMeshBuildSource_get_transform_m50CACC31C5752FB986CCBB76ABBD3F4BC0EB6A0F_AdjustorThunk ();
// 0x0000003E System.Void UnityEngine.AI.NavMeshBuildSource::set_transform(UnityEngine.Matrix4x4)
extern void NavMeshBuildSource_set_transform_m0923D18B2589981624DA68AA230470639B495859_AdjustorThunk ();
// 0x0000003F UnityEngine.Vector3 UnityEngine.AI.NavMeshBuildSource::get_size()
extern void NavMeshBuildSource_get_size_m8B671FBB666166905779429AA7452D99A2A82B61_AdjustorThunk ();
// 0x00000040 System.Void UnityEngine.AI.NavMeshBuildSource::set_size(UnityEngine.Vector3)
extern void NavMeshBuildSource_set_size_m31522E0DD87A660F8DC023B42CFB8AB816488390_AdjustorThunk ();
// 0x00000041 UnityEngine.AI.NavMeshBuildSourceShape UnityEngine.AI.NavMeshBuildSource::get_shape()
extern void NavMeshBuildSource_get_shape_m56F9CA4F38B911918351887AEB9E135D576811CA_AdjustorThunk ();
// 0x00000042 System.Void UnityEngine.AI.NavMeshBuildSource::set_shape(UnityEngine.AI.NavMeshBuildSourceShape)
extern void NavMeshBuildSource_set_shape_m2D811D746EEB863DE892485C8E6564CDC5AAD51B_AdjustorThunk ();
// 0x00000043 System.Void UnityEngine.AI.NavMeshBuildSource::set_area(System.Int32)
extern void NavMeshBuildSource_set_area_mC5B4417CEDBFEA0F72DFC0CCB6D075BDA519144E_AdjustorThunk ();
// 0x00000044 UnityEngine.Object UnityEngine.AI.NavMeshBuildSource::get_sourceObject()
extern void NavMeshBuildSource_get_sourceObject_mCF31F93BEF12310070B5D9F6D6FD1FAC330486C5_AdjustorThunk ();
// 0x00000045 UnityEngine.Component UnityEngine.AI.NavMeshBuildSource::get_component()
extern void NavMeshBuildSource_get_component_m3041C9FEEC284237CEA8F92B94D1BBF215C846E7_AdjustorThunk ();
// 0x00000046 UnityEngine.Component UnityEngine.AI.NavMeshBuildSource::InternalGetComponent(System.Int32)
extern void NavMeshBuildSource_InternalGetComponent_m47A75357A0F2E9381BF18A79A6F7633E8423EA57 ();
// 0x00000047 UnityEngine.Object UnityEngine.AI.NavMeshBuildSource::InternalGetObject(System.Int32)
extern void NavMeshBuildSource_InternalGetObject_m620ED8ACC353D990F73CBC0E716FE25F99D135F2 ();
// 0x00000048 System.Void UnityEngine.AI.NavMeshBuildMarkup::set_overrideArea(System.Boolean)
extern void NavMeshBuildMarkup_set_overrideArea_mFA7BD1329F9BCBE74AF766F0618EEB3C2EF6E9C2_AdjustorThunk ();
// 0x00000049 System.Void UnityEngine.AI.NavMeshBuildMarkup::set_area(System.Int32)
extern void NavMeshBuildMarkup_set_area_m559665BABF14E217FF6ED552EA91F9A5469B6273_AdjustorThunk ();
// 0x0000004A System.Void UnityEngine.AI.NavMeshBuildMarkup::set_ignoreFromBuild(System.Boolean)
extern void NavMeshBuildMarkup_set_ignoreFromBuild_mFCDADEA5E3275A336A0A91CBBB19BE693FCCB8A1_AdjustorThunk ();
// 0x0000004B System.Void UnityEngine.AI.NavMeshBuildMarkup::set_root(UnityEngine.Transform)
extern void NavMeshBuildMarkup_set_root_mE71EEB509212A5A3A2D572EC6C0D49293980D386_AdjustorThunk ();
// 0x0000004C System.Int32 UnityEngine.AI.NavMeshBuildSettings::get_agentTypeID()
extern void NavMeshBuildSettings_get_agentTypeID_m92765F76C200F6174B61A1A9F5F6E9D60695D3BB_AdjustorThunk ();
// 0x0000004D System.Void UnityEngine.AI.NavMeshBuildSettings::set_agentTypeID(System.Int32)
extern void NavMeshBuildSettings_set_agentTypeID_m897F152E5560E14C7EB0571407AE93CAE4D05966_AdjustorThunk ();
// 0x0000004E System.Void UnityEngine.AI.NavMeshBuildSettings::set_overrideVoxelSize(System.Boolean)
extern void NavMeshBuildSettings_set_overrideVoxelSize_m5D3E3920C692754F915DFB088E31D362A57EC764_AdjustorThunk ();
// 0x0000004F System.Void UnityEngine.AI.NavMeshBuildSettings::set_voxelSize(System.Single)
extern void NavMeshBuildSettings_set_voxelSize_m069E108B58A2A0A4BEB7A0D946B0143B388EA9C3_AdjustorThunk ();
// 0x00000050 System.Void UnityEngine.AI.NavMeshBuildSettings::set_overrideTileSize(System.Boolean)
extern void NavMeshBuildSettings_set_overrideTileSize_mE9ADFED0E7D5EC2D18F6F4F6D2D5146B31C784C5_AdjustorThunk ();
// 0x00000051 System.Void UnityEngine.AI.NavMeshBuildSettings::set_tileSize(System.Int32)
extern void NavMeshBuildSettings_set_tileSize_m3C8F374924A2FC259065FC7362F3B9C376B795E6_AdjustorThunk ();
static Il2CppMethodPointer s_methodPointers[81] = 
{
	NavMeshBuilder_CollectSources_m11DFC2DE0CD5165927B83ADA915E026D2A2BADAE,
	NavMeshBuilder_CollectSources_m73C0100A28595C935428590A4A469D80C44D4C67,
	NavMeshBuilder_CollectSourcesInternal_m83D18AA37DB1651C5DACE705C60D2FCF79D52C43,
	NavMeshBuilder_BuildNavMeshData_m346D7D0C459AB4859E03D4989FB0955151D19389,
	NavMeshBuilder_UpdateNavMeshDataListInternal_mC137D6C654B5479D835C7B98611F4EA84E3A5B2C,
	NavMeshBuilder_UpdateNavMeshDataAsync_m953992EDF8FE5C791858CD043F0250138351010B,
	NavMeshBuilder_UpdateNavMeshDataAsyncListInternal_m374D1C5CE452D36E47F006C2DDF8C5689D6CF3D0,
	NavMeshBuilder_CollectSourcesInternal_Injected_mA0A78D3BE843386FF640205FA025D974976848BC,
	NavMeshBuilder_UpdateNavMeshDataListInternal_Injected_m14208EC28E1F5BDD8A5CDB5CD1913BFA2C5FD4EC,
	NavMeshBuilder_UpdateNavMeshDataAsyncListInternal_Injected_mBC3CEBC66EF10C73A496CC21F12C23D112CA70D6,
	NavMeshAgent_SetDestination_mDCFFAB501EFCC89E17EA13F49A917D340AEF0BC8,
	NavMeshAgent_get_velocity_m1CF6CC11732234E2E3EC3B778B79C5492A990186,
	NavMeshAgent_get_speed_mED796DC28B1B5D848CA96E1831DA12696A595AC6,
	NavMeshAgent_set_speed_mE709E788BFB285190B9504F6DB3199B35B9B0D35,
	NavMeshAgent_set_angularSpeed_m90DB947AF61F1AEA798FF5194961B2DA63219FC0,
	NavMeshAgent_set_acceleration_mA37022F97D535F75B34C7D07FA50A8D89639F3F7,
	NavMeshAgent_SetDestination_Injected_m03F00018E71D91B109510C2EF1F01A3ECD84FF89,
	NavMeshAgent_get_velocity_Injected_mBBFC5D1D8F50C16F9ACD5FCC0A0E50A0FE525FB3,
	NavMeshData__ctor_mFFCE3983B9C075BF2CFAFD20CCB1F1F9EE860010,
	NavMeshData_Internal_Create_m5EB9327216B5DAF47D57F469F798E4583F302CF3,
	NavMeshData_set_position_m70F95E20AE46E773E19D73A3FEFBFCABD1CF061A,
	NavMeshData_set_rotation_mF90B0D2ED475B0DD536BA43B4C00AACAEEA7449F,
	NavMeshData_set_position_Injected_m5B27DA09BC1D1AAB6C774BBFAA4EF1143BADB196,
	NavMeshData_set_rotation_Injected_m05B6C44C78BC6C4748086D4ED000594BF9093122,
	NavMeshDataInstance_get_valid_m4334E80C0A66334F366EBF69DCB634E86DDFA05B_AdjustorThunk,
	NavMeshDataInstance_get_id_mEA3B300C16CC2B995641461BE9E69BEEF60BC2EF_AdjustorThunk,
	NavMeshDataInstance_set_id_mACF5FCAB5F05DAF0F2C64C3670141DB2598B7DE9_AdjustorThunk,
	NavMeshDataInstance_Remove_m6B51624B186D5F570D115A6205BBF58CBA60A837_AdjustorThunk,
	NavMeshDataInstance_set_owner_m80A0BFBF4A375008A5F9CD126A520F7DF66333FD_AdjustorThunk,
	NavMeshLinkData_set_startPosition_mB28631278DA2678D228FD7C917012068ADFF05F5_AdjustorThunk,
	NavMeshLinkData_set_endPosition_m2D2051E1F6166CA3C803B5B93ADAE0DAE7F6C5F9_AdjustorThunk,
	NavMeshLinkData_set_costModifier_m1552A1199CE5EFBA60C60AE601A3E43E4361ED9B_AdjustorThunk,
	NavMeshLinkData_set_bidirectional_m1AC3CAD1CEFE47FE7B36E4403F7036B819199289_AdjustorThunk,
	NavMeshLinkData_set_width_m8473F9DE6D05FD86BA04DF84AA593788B645870D_AdjustorThunk,
	NavMeshLinkData_set_area_m9833F712659EA6D789487F1C78DE0B00599FCE67_AdjustorThunk,
	NavMeshLinkData_set_agentTypeID_m9D7FC516E796D8F5B84D88CBFF2865D54123A9E7_AdjustorThunk,
	NavMeshLinkInstance_get_valid_mE143382F0205F0BD104CAA0214666C7A307D2608_AdjustorThunk,
	NavMeshLinkInstance_get_id_m8B494FAE3B97FED8177D20816D541EB271BC9363_AdjustorThunk,
	NavMeshLinkInstance_set_id_m48D09FEDF41895E1EA92FE794811113565F4281C_AdjustorThunk,
	NavMeshLinkInstance_Remove_mF440F826BECB512A1BEBB8648E64ACFA7B3D0830_AdjustorThunk,
	NavMeshLinkInstance_set_owner_m2DF837AD84594C78E22CDC6636CF51B599E722D6_AdjustorThunk,
	NavMesh_Internal_CallOnNavMeshPreUpdate_mED6CAB94A6CB61A5FD547B7026DB4C96F2AF5B60,
	NavMesh_AddNavMeshData_m8644DBCE7C1C3D201DF09DE38C2DAC45459F4380,
	NavMesh_IsValidNavMeshDataHandle_m84E79E847B847D87FF4D98212D9E932F1837E815,
	NavMesh_IsValidLinkHandle_m76E4AB70D416D76158489A407F032BF079A8153F,
	NavMesh_InternalSetOwner_m9C57A11AE2C80BC6ADC251267F4065556D7BF5B8,
	NavMesh_InternalSetLinkOwner_m9825A197AEA5EAFF4A196CBC561502DED835080D,
	NavMesh_AddNavMeshDataTransformedInternal_m00CF5EA76AE64FE7D26B050D0B56F4D4D31167B6,
	NavMesh_RemoveNavMeshDataInternal_m653550983DDCEE84C44F0EF041D36F455E287C6B,
	NavMesh_AddLink_mC8E70FAB247C2E33213BBE436E17D12F524114C3,
	NavMesh_AddLinkInternal_m1E1038561F7A53755E589BDED731CE87DE18C776,
	NavMesh_RemoveLinkInternal_mAE7CD6A1EC37829AB36C9829B5E2848686A8686C,
	NavMesh_GetSettingsByID_m9B61F5191DBBE8D7D595D17FD10EBBCAD8881D05,
	NavMesh_AddNavMeshDataTransformedInternal_Injected_m4ACC2049A75503A225529F3EAF1DCE61DDAB8786,
	NavMesh_AddLinkInternal_Injected_m4F49F4146BD7867BA3456B0FE3636063FC4A21E6,
	NavMesh_GetSettingsByID_Injected_m3F525258D1AB71E7A5BAF7D354BD117C60AB81DB,
	OnNavMeshPreUpdate__ctor_mD019C429BD8D299B85C320A6EFB2FFEDC3F85F42,
	OnNavMeshPreUpdate_Invoke_mE56CD30B200FECFD94AD4B22923B32BD789D70F0,
	OnNavMeshPreUpdate_BeginInvoke_m67FA7767274E77169A57ADFE041EA9B914E752C6,
	OnNavMeshPreUpdate_EndInvoke_mB55765702AA123A6D7C3DF8DDC597E3DEBC79836,
	NavMeshBuildSource_get_transform_m50CACC31C5752FB986CCBB76ABBD3F4BC0EB6A0F_AdjustorThunk,
	NavMeshBuildSource_set_transform_m0923D18B2589981624DA68AA230470639B495859_AdjustorThunk,
	NavMeshBuildSource_get_size_m8B671FBB666166905779429AA7452D99A2A82B61_AdjustorThunk,
	NavMeshBuildSource_set_size_m31522E0DD87A660F8DC023B42CFB8AB816488390_AdjustorThunk,
	NavMeshBuildSource_get_shape_m56F9CA4F38B911918351887AEB9E135D576811CA_AdjustorThunk,
	NavMeshBuildSource_set_shape_m2D811D746EEB863DE892485C8E6564CDC5AAD51B_AdjustorThunk,
	NavMeshBuildSource_set_area_mC5B4417CEDBFEA0F72DFC0CCB6D075BDA519144E_AdjustorThunk,
	NavMeshBuildSource_get_sourceObject_mCF31F93BEF12310070B5D9F6D6FD1FAC330486C5_AdjustorThunk,
	NavMeshBuildSource_get_component_m3041C9FEEC284237CEA8F92B94D1BBF215C846E7_AdjustorThunk,
	NavMeshBuildSource_InternalGetComponent_m47A75357A0F2E9381BF18A79A6F7633E8423EA57,
	NavMeshBuildSource_InternalGetObject_m620ED8ACC353D990F73CBC0E716FE25F99D135F2,
	NavMeshBuildMarkup_set_overrideArea_mFA7BD1329F9BCBE74AF766F0618EEB3C2EF6E9C2_AdjustorThunk,
	NavMeshBuildMarkup_set_area_m559665BABF14E217FF6ED552EA91F9A5469B6273_AdjustorThunk,
	NavMeshBuildMarkup_set_ignoreFromBuild_mFCDADEA5E3275A336A0A91CBBB19BE693FCCB8A1_AdjustorThunk,
	NavMeshBuildMarkup_set_root_mE71EEB509212A5A3A2D572EC6C0D49293980D386_AdjustorThunk,
	NavMeshBuildSettings_get_agentTypeID_m92765F76C200F6174B61A1A9F5F6E9D60695D3BB_AdjustorThunk,
	NavMeshBuildSettings_set_agentTypeID_m897F152E5560E14C7EB0571407AE93CAE4D05966_AdjustorThunk,
	NavMeshBuildSettings_set_overrideVoxelSize_m5D3E3920C692754F915DFB088E31D362A57EC764_AdjustorThunk,
	NavMeshBuildSettings_set_voxelSize_m069E108B58A2A0A4BEB7A0D946B0143B388EA9C3_AdjustorThunk,
	NavMeshBuildSettings_set_overrideTileSize_mE9ADFED0E7D5EC2D18F6F4F6D2D5146B31C784C5_AdjustorThunk,
	NavMeshBuildSettings_set_tileSize_m3C8F374924A2FC259065FC7362F3B9C376B795E6_AdjustorThunk,
};
static const int32_t s_InvokerIndices[81] = 
{
	1561,
	1562,
	1563,
	1564,
	1565,
	1566,
	1566,
	1567,
	1568,
	1569,
	1010,
	1000,
	654,
	275,
	275,
	275,
	757,
	6,
	32,
	305,
	1001,
	1153,
	6,
	6,
	102,
	10,
	32,
	23,
	26,
	1001,
	1001,
	275,
	31,
	275,
	32,
	32,
	102,
	10,
	32,
	23,
	26,
	3,
	1570,
	46,
	46,
	53,
	53,
	1571,
	121,
	1572,
	1573,
	121,
	1574,
	914,
	461,
	1171,
	163,
	23,
	101,
	26,
	1080,
	1042,
	1000,
	1001,
	10,
	32,
	32,
	14,
	14,
	43,
	43,
	31,
	32,
	31,
	26,
	10,
	32,
	31,
	275,
	31,
	32,
};
extern const Il2CppCodeGenModule g_UnityEngine_AIModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AIModuleCodeGenModule = 
{
	"UnityEngine.AIModule.dll",
	81,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
