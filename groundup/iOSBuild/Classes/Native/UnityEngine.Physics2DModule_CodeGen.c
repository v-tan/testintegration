﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.Physics2D::.cctor()
extern void Physics2D__cctor_mC0D622F2EAF13BF0513DB2969E50EEC5631CDBFC ();
// 0x00000002 UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern void Collision2D_get_collider_m8767ED466970D214201582C37D90F2CD4BEFD6D2 ();
// 0x00000003 UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_rigidbody()
extern void Collision2D_get_rigidbody_m7344D69B114D326F6866C0F02E5152CBE5C8B5BB ();
// 0x00000004 UnityEngine.GameObject UnityEngine.Collision2D::get_gameObject()
extern void Collision2D_get_gameObject_m209F9F15585DE3F9270E0D9BFB050950AD301A5F ();
// 0x00000005 UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern void RaycastHit2D_get_point_mC567E234B1B673C3A9819023C3DC97C781443098_AdjustorThunk ();
// 0x00000006 UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern void RaycastHit2D_get_normal_m9F0974E4514AD56C00FCF6FF4CDF10AED62FE6E4_AdjustorThunk ();
// 0x00000007 System.Single UnityEngine.RaycastHit2D::get_distance()
extern void RaycastHit2D_get_distance_m2D9F391717ECACFDA8E01A4126E0F8F59F7E774F_AdjustorThunk ();
// 0x00000008 UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern void RaycastHit2D_get_collider_m6A7EC53B2E179C2EFF4F29018A132B2979CBE976_AdjustorThunk ();
static Il2CppMethodPointer s_methodPointers[8] = 
{
	Physics2D__cctor_mC0D622F2EAF13BF0513DB2969E50EEC5631CDBFC,
	Collision2D_get_collider_m8767ED466970D214201582C37D90F2CD4BEFD6D2,
	Collision2D_get_rigidbody_m7344D69B114D326F6866C0F02E5152CBE5C8B5BB,
	Collision2D_get_gameObject_m209F9F15585DE3F9270E0D9BFB050950AD301A5F,
	RaycastHit2D_get_point_mC567E234B1B673C3A9819023C3DC97C781443098_AdjustorThunk,
	RaycastHit2D_get_normal_m9F0974E4514AD56C00FCF6FF4CDF10AED62FE6E4_AdjustorThunk,
	RaycastHit2D_get_distance_m2D9F391717ECACFDA8E01A4126E0F8F59F7E774F_AdjustorThunk,
	RaycastHit2D_get_collider_m6A7EC53B2E179C2EFF4F29018A132B2979CBE976_AdjustorThunk,
};
static const int32_t s_InvokerIndices[8] = 
{
	3,
	14,
	14,
	14,
	1009,
	1009,
	654,
	14,
};
extern const Il2CppCodeGenModule g_UnityEngine_Physics2DModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Physics2DModuleCodeGenModule = 
{
	"UnityEngine.Physics2DModule.dll",
	8,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
