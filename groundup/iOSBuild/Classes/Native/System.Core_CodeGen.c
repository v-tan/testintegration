﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000004 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000008 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000009 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000000E TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000012 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000015 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000019 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000001A System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583 ();
// 0x0000001B System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Int32>)
// 0x0000001C System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000001D TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000001E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000001F System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000020 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000021 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000022 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000023 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000025 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000026 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000027 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000028 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000029 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000002A System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000002B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000002E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000002F System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000030 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000031 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000034 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000038 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000039 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000003A System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003D System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003E System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000003F System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000041 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000042 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000043 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000044 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000046 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000047 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000048 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000049 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000004A TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000004B System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000004C System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000004D System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x0000004E System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x0000004F System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000050 TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000051 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000052 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000053 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000054 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000055 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x00000056 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x00000057 System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x00000058 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x00000059 TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000005A System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x0000005B System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x0000005C System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000005D System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005E System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x0000005F System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000060 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x00000061 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000062 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000063 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000064 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000065 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000066 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000067 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000068 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x00000069 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x0000006A System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x0000006B System.Void System.Linq.Set`1::Resize()
// 0x0000006C System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x0000006D System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000006E System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000006F System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000070 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000071 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000072 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000073 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000074 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000075 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000076 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000077 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000078 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000079 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000007A System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000007B System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000007C System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000007D System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000007E System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x0000007F System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000080 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000081 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000082 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000083 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000084 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000085 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000086 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000087 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000088 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000089 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000008A System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000008B System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000008C System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000008D System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000008E System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000008F System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000090 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000091 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000092 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000093 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000094 System.Void System.Collections.Generic.HashSet`1::ExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000095 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000096 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000097 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000098 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000099 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000009A System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000009B System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000009C System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000009D System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000009E System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000009F T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000A0 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000A1 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[161] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[161] = 
{
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	186,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[51] = 
{
	{ 0x02000004, { 73, 4 } },
	{ 0x02000005, { 77, 9 } },
	{ 0x02000006, { 88, 7 } },
	{ 0x02000007, { 97, 10 } },
	{ 0x02000008, { 109, 11 } },
	{ 0x02000009, { 123, 9 } },
	{ 0x0200000A, { 135, 12 } },
	{ 0x0200000B, { 150, 1 } },
	{ 0x0200000C, { 151, 2 } },
	{ 0x0200000D, { 153, 12 } },
	{ 0x0200000E, { 165, 11 } },
	{ 0x0200000F, { 176, 6 } },
	{ 0x02000011, { 182, 8 } },
	{ 0x02000013, { 190, 3 } },
	{ 0x02000014, { 195, 5 } },
	{ 0x02000015, { 200, 7 } },
	{ 0x02000016, { 207, 3 } },
	{ 0x02000017, { 210, 7 } },
	{ 0x02000018, { 217, 4 } },
	{ 0x02000019, { 221, 25 } },
	{ 0x0200001B, { 246, 2 } },
	{ 0x06000004, { 0, 10 } },
	{ 0x06000005, { 10, 10 } },
	{ 0x06000006, { 20, 5 } },
	{ 0x06000007, { 25, 5 } },
	{ 0x06000008, { 30, 1 } },
	{ 0x06000009, { 31, 2 } },
	{ 0x0600000A, { 33, 2 } },
	{ 0x0600000B, { 35, 1 } },
	{ 0x0600000C, { 36, 1 } },
	{ 0x0600000D, { 37, 2 } },
	{ 0x0600000E, { 39, 3 } },
	{ 0x0600000F, { 42, 2 } },
	{ 0x06000010, { 44, 2 } },
	{ 0x06000011, { 46, 2 } },
	{ 0x06000012, { 48, 4 } },
	{ 0x06000013, { 52, 4 } },
	{ 0x06000014, { 56, 3 } },
	{ 0x06000015, { 59, 1 } },
	{ 0x06000016, { 60, 3 } },
	{ 0x06000017, { 63, 2 } },
	{ 0x06000018, { 65, 2 } },
	{ 0x06000019, { 67, 5 } },
	{ 0x0600001B, { 72, 1 } },
	{ 0x0600002B, { 86, 2 } },
	{ 0x06000030, { 95, 2 } },
	{ 0x06000035, { 107, 2 } },
	{ 0x0600003B, { 120, 3 } },
	{ 0x06000040, { 132, 3 } },
	{ 0x06000045, { 147, 3 } },
	{ 0x06000070, { 193, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[248] = 
{
	{ (Il2CppRGCTXDataType)2, 15652 },
	{ (Il2CppRGCTXDataType)3, 10291 },
	{ (Il2CppRGCTXDataType)2, 15653 },
	{ (Il2CppRGCTXDataType)2, 15654 },
	{ (Il2CppRGCTXDataType)3, 10292 },
	{ (Il2CppRGCTXDataType)2, 15655 },
	{ (Il2CppRGCTXDataType)2, 15656 },
	{ (Il2CppRGCTXDataType)3, 10293 },
	{ (Il2CppRGCTXDataType)2, 15657 },
	{ (Il2CppRGCTXDataType)3, 10294 },
	{ (Il2CppRGCTXDataType)2, 15658 },
	{ (Il2CppRGCTXDataType)3, 10295 },
	{ (Il2CppRGCTXDataType)2, 15659 },
	{ (Il2CppRGCTXDataType)2, 15660 },
	{ (Il2CppRGCTXDataType)3, 10296 },
	{ (Il2CppRGCTXDataType)2, 15661 },
	{ (Il2CppRGCTXDataType)2, 15662 },
	{ (Il2CppRGCTXDataType)3, 10297 },
	{ (Il2CppRGCTXDataType)2, 15663 },
	{ (Il2CppRGCTXDataType)3, 10298 },
	{ (Il2CppRGCTXDataType)2, 15664 },
	{ (Il2CppRGCTXDataType)3, 10299 },
	{ (Il2CppRGCTXDataType)3, 10300 },
	{ (Il2CppRGCTXDataType)2, 11723 },
	{ (Il2CppRGCTXDataType)3, 10301 },
	{ (Il2CppRGCTXDataType)2, 15665 },
	{ (Il2CppRGCTXDataType)3, 10302 },
	{ (Il2CppRGCTXDataType)3, 10303 },
	{ (Il2CppRGCTXDataType)2, 11730 },
	{ (Il2CppRGCTXDataType)3, 10304 },
	{ (Il2CppRGCTXDataType)3, 10305 },
	{ (Il2CppRGCTXDataType)2, 15666 },
	{ (Il2CppRGCTXDataType)3, 10306 },
	{ (Il2CppRGCTXDataType)2, 15667 },
	{ (Il2CppRGCTXDataType)3, 10307 },
	{ (Il2CppRGCTXDataType)3, 10308 },
	{ (Il2CppRGCTXDataType)3, 10309 },
	{ (Il2CppRGCTXDataType)2, 15668 },
	{ (Il2CppRGCTXDataType)3, 10310 },
	{ (Il2CppRGCTXDataType)2, 15669 },
	{ (Il2CppRGCTXDataType)3, 10311 },
	{ (Il2CppRGCTXDataType)3, 10312 },
	{ (Il2CppRGCTXDataType)2, 11760 },
	{ (Il2CppRGCTXDataType)3, 10313 },
	{ (Il2CppRGCTXDataType)2, 11761 },
	{ (Il2CppRGCTXDataType)3, 10314 },
	{ (Il2CppRGCTXDataType)2, 15670 },
	{ (Il2CppRGCTXDataType)3, 10315 },
	{ (Il2CppRGCTXDataType)2, 15671 },
	{ (Il2CppRGCTXDataType)2, 15672 },
	{ (Il2CppRGCTXDataType)2, 11765 },
	{ (Il2CppRGCTXDataType)2, 15673 },
	{ (Il2CppRGCTXDataType)2, 15674 },
	{ (Il2CppRGCTXDataType)2, 15675 },
	{ (Il2CppRGCTXDataType)2, 11767 },
	{ (Il2CppRGCTXDataType)2, 15676 },
	{ (Il2CppRGCTXDataType)2, 11769 },
	{ (Il2CppRGCTXDataType)2, 15677 },
	{ (Il2CppRGCTXDataType)3, 10316 },
	{ (Il2CppRGCTXDataType)2, 11772 },
	{ (Il2CppRGCTXDataType)2, 11774 },
	{ (Il2CppRGCTXDataType)2, 15678 },
	{ (Il2CppRGCTXDataType)3, 10317 },
	{ (Il2CppRGCTXDataType)2, 15679 },
	{ (Il2CppRGCTXDataType)2, 11777 },
	{ (Il2CppRGCTXDataType)2, 15680 },
	{ (Il2CppRGCTXDataType)3, 10318 },
	{ (Il2CppRGCTXDataType)3, 10319 },
	{ (Il2CppRGCTXDataType)2, 15681 },
	{ (Il2CppRGCTXDataType)2, 11781 },
	{ (Il2CppRGCTXDataType)2, 15682 },
	{ (Il2CppRGCTXDataType)2, 11783 },
	{ (Il2CppRGCTXDataType)3, 10320 },
	{ (Il2CppRGCTXDataType)3, 10321 },
	{ (Il2CppRGCTXDataType)3, 10322 },
	{ (Il2CppRGCTXDataType)2, 11789 },
	{ (Il2CppRGCTXDataType)3, 10323 },
	{ (Il2CppRGCTXDataType)3, 10324 },
	{ (Il2CppRGCTXDataType)2, 11801 },
	{ (Il2CppRGCTXDataType)2, 15683 },
	{ (Il2CppRGCTXDataType)3, 10325 },
	{ (Il2CppRGCTXDataType)3, 10326 },
	{ (Il2CppRGCTXDataType)2, 11803 },
	{ (Il2CppRGCTXDataType)2, 15574 },
	{ (Il2CppRGCTXDataType)3, 10327 },
	{ (Il2CppRGCTXDataType)3, 10328 },
	{ (Il2CppRGCTXDataType)2, 15684 },
	{ (Il2CppRGCTXDataType)3, 10329 },
	{ (Il2CppRGCTXDataType)3, 10330 },
	{ (Il2CppRGCTXDataType)2, 11813 },
	{ (Il2CppRGCTXDataType)2, 15685 },
	{ (Il2CppRGCTXDataType)3, 10331 },
	{ (Il2CppRGCTXDataType)3, 10332 },
	{ (Il2CppRGCTXDataType)3, 9927 },
	{ (Il2CppRGCTXDataType)3, 10333 },
	{ (Il2CppRGCTXDataType)2, 15686 },
	{ (Il2CppRGCTXDataType)3, 10334 },
	{ (Il2CppRGCTXDataType)3, 10335 },
	{ (Il2CppRGCTXDataType)2, 11825 },
	{ (Il2CppRGCTXDataType)2, 15687 },
	{ (Il2CppRGCTXDataType)3, 10336 },
	{ (Il2CppRGCTXDataType)3, 10337 },
	{ (Il2CppRGCTXDataType)3, 10338 },
	{ (Il2CppRGCTXDataType)3, 10339 },
	{ (Il2CppRGCTXDataType)3, 10340 },
	{ (Il2CppRGCTXDataType)3, 9933 },
	{ (Il2CppRGCTXDataType)3, 10341 },
	{ (Il2CppRGCTXDataType)2, 15688 },
	{ (Il2CppRGCTXDataType)3, 10342 },
	{ (Il2CppRGCTXDataType)3, 10343 },
	{ (Il2CppRGCTXDataType)2, 11838 },
	{ (Il2CppRGCTXDataType)2, 15689 },
	{ (Il2CppRGCTXDataType)3, 10344 },
	{ (Il2CppRGCTXDataType)3, 10345 },
	{ (Il2CppRGCTXDataType)2, 11840 },
	{ (Il2CppRGCTXDataType)2, 15690 },
	{ (Il2CppRGCTXDataType)3, 10346 },
	{ (Il2CppRGCTXDataType)3, 10347 },
	{ (Il2CppRGCTXDataType)2, 15691 },
	{ (Il2CppRGCTXDataType)3, 10348 },
	{ (Il2CppRGCTXDataType)3, 10349 },
	{ (Il2CppRGCTXDataType)2, 15692 },
	{ (Il2CppRGCTXDataType)3, 10350 },
	{ (Il2CppRGCTXDataType)3, 10351 },
	{ (Il2CppRGCTXDataType)2, 11855 },
	{ (Il2CppRGCTXDataType)2, 15693 },
	{ (Il2CppRGCTXDataType)3, 10352 },
	{ (Il2CppRGCTXDataType)3, 10353 },
	{ (Il2CppRGCTXDataType)3, 10354 },
	{ (Il2CppRGCTXDataType)3, 9944 },
	{ (Il2CppRGCTXDataType)2, 15694 },
	{ (Il2CppRGCTXDataType)3, 10355 },
	{ (Il2CppRGCTXDataType)3, 10356 },
	{ (Il2CppRGCTXDataType)2, 15695 },
	{ (Il2CppRGCTXDataType)3, 10357 },
	{ (Il2CppRGCTXDataType)3, 10358 },
	{ (Il2CppRGCTXDataType)2, 11871 },
	{ (Il2CppRGCTXDataType)2, 15696 },
	{ (Il2CppRGCTXDataType)3, 10359 },
	{ (Il2CppRGCTXDataType)3, 10360 },
	{ (Il2CppRGCTXDataType)3, 10361 },
	{ (Il2CppRGCTXDataType)3, 10362 },
	{ (Il2CppRGCTXDataType)3, 10363 },
	{ (Il2CppRGCTXDataType)3, 10364 },
	{ (Il2CppRGCTXDataType)3, 9950 },
	{ (Il2CppRGCTXDataType)2, 15697 },
	{ (Il2CppRGCTXDataType)3, 10365 },
	{ (Il2CppRGCTXDataType)3, 10366 },
	{ (Il2CppRGCTXDataType)2, 15698 },
	{ (Il2CppRGCTXDataType)3, 10367 },
	{ (Il2CppRGCTXDataType)3, 10368 },
	{ (Il2CppRGCTXDataType)3, 10369 },
	{ (Il2CppRGCTXDataType)3, 10370 },
	{ (Il2CppRGCTXDataType)3, 10371 },
	{ (Il2CppRGCTXDataType)3, 10372 },
	{ (Il2CppRGCTXDataType)2, 15699 },
	{ (Il2CppRGCTXDataType)2, 15700 },
	{ (Il2CppRGCTXDataType)3, 10373 },
	{ (Il2CppRGCTXDataType)2, 11906 },
	{ (Il2CppRGCTXDataType)2, 11900 },
	{ (Il2CppRGCTXDataType)3, 10374 },
	{ (Il2CppRGCTXDataType)2, 11899 },
	{ (Il2CppRGCTXDataType)2, 15701 },
	{ (Il2CppRGCTXDataType)3, 10375 },
	{ (Il2CppRGCTXDataType)3, 10376 },
	{ (Il2CppRGCTXDataType)3, 10377 },
	{ (Il2CppRGCTXDataType)2, 15702 },
	{ (Il2CppRGCTXDataType)3, 10378 },
	{ (Il2CppRGCTXDataType)2, 11922 },
	{ (Il2CppRGCTXDataType)2, 11914 },
	{ (Il2CppRGCTXDataType)3, 10379 },
	{ (Il2CppRGCTXDataType)3, 10380 },
	{ (Il2CppRGCTXDataType)2, 11913 },
	{ (Il2CppRGCTXDataType)2, 15703 },
	{ (Il2CppRGCTXDataType)3, 10381 },
	{ (Il2CppRGCTXDataType)3, 10382 },
	{ (Il2CppRGCTXDataType)3, 10383 },
	{ (Il2CppRGCTXDataType)2, 11926 },
	{ (Il2CppRGCTXDataType)3, 10384 },
	{ (Il2CppRGCTXDataType)2, 15704 },
	{ (Il2CppRGCTXDataType)3, 10385 },
	{ (Il2CppRGCTXDataType)3, 10386 },
	{ (Il2CppRGCTXDataType)3, 10387 },
	{ (Il2CppRGCTXDataType)2, 15705 },
	{ (Il2CppRGCTXDataType)2, 15706 },
	{ (Il2CppRGCTXDataType)3, 10388 },
	{ (Il2CppRGCTXDataType)3, 10389 },
	{ (Il2CppRGCTXDataType)2, 11942 },
	{ (Il2CppRGCTXDataType)3, 10390 },
	{ (Il2CppRGCTXDataType)2, 11943 },
	{ (Il2CppRGCTXDataType)2, 15707 },
	{ (Il2CppRGCTXDataType)3, 10391 },
	{ (Il2CppRGCTXDataType)3, 10392 },
	{ (Il2CppRGCTXDataType)2, 15708 },
	{ (Il2CppRGCTXDataType)3, 10393 },
	{ (Il2CppRGCTXDataType)2, 15709 },
	{ (Il2CppRGCTXDataType)3, 10394 },
	{ (Il2CppRGCTXDataType)3, 10395 },
	{ (Il2CppRGCTXDataType)3, 10396 },
	{ (Il2CppRGCTXDataType)2, 11962 },
	{ (Il2CppRGCTXDataType)3, 10397 },
	{ (Il2CppRGCTXDataType)2, 11970 },
	{ (Il2CppRGCTXDataType)3, 10398 },
	{ (Il2CppRGCTXDataType)2, 15710 },
	{ (Il2CppRGCTXDataType)2, 15711 },
	{ (Il2CppRGCTXDataType)3, 10399 },
	{ (Il2CppRGCTXDataType)3, 10400 },
	{ (Il2CppRGCTXDataType)3, 10401 },
	{ (Il2CppRGCTXDataType)3, 10402 },
	{ (Il2CppRGCTXDataType)3, 10403 },
	{ (Il2CppRGCTXDataType)3, 10404 },
	{ (Il2CppRGCTXDataType)2, 11986 },
	{ (Il2CppRGCTXDataType)2, 15712 },
	{ (Il2CppRGCTXDataType)3, 10405 },
	{ (Il2CppRGCTXDataType)3, 10406 },
	{ (Il2CppRGCTXDataType)2, 11990 },
	{ (Il2CppRGCTXDataType)3, 10407 },
	{ (Il2CppRGCTXDataType)2, 15713 },
	{ (Il2CppRGCTXDataType)2, 12000 },
	{ (Il2CppRGCTXDataType)2, 11998 },
	{ (Il2CppRGCTXDataType)2, 15714 },
	{ (Il2CppRGCTXDataType)3, 10408 },
	{ (Il2CppRGCTXDataType)2, 15715 },
	{ (Il2CppRGCTXDataType)3, 10409 },
	{ (Il2CppRGCTXDataType)3, 10410 },
	{ (Il2CppRGCTXDataType)3, 10411 },
	{ (Il2CppRGCTXDataType)2, 12004 },
	{ (Il2CppRGCTXDataType)3, 10412 },
	{ (Il2CppRGCTXDataType)3, 10413 },
	{ (Il2CppRGCTXDataType)2, 12007 },
	{ (Il2CppRGCTXDataType)3, 10414 },
	{ (Il2CppRGCTXDataType)1, 15716 },
	{ (Il2CppRGCTXDataType)2, 12006 },
	{ (Il2CppRGCTXDataType)3, 10415 },
	{ (Il2CppRGCTXDataType)1, 12006 },
	{ (Il2CppRGCTXDataType)1, 12004 },
	{ (Il2CppRGCTXDataType)2, 15717 },
	{ (Il2CppRGCTXDataType)2, 12006 },
	{ (Il2CppRGCTXDataType)3, 10416 },
	{ (Il2CppRGCTXDataType)2, 12009 },
	{ (Il2CppRGCTXDataType)2, 12008 },
	{ (Il2CppRGCTXDataType)3, 10417 },
	{ (Il2CppRGCTXDataType)3, 10418 },
	{ (Il2CppRGCTXDataType)3, 10419 },
	{ (Il2CppRGCTXDataType)3, 10420 },
	{ (Il2CppRGCTXDataType)2, 12005 },
	{ (Il2CppRGCTXDataType)3, 10421 },
	{ (Il2CppRGCTXDataType)2, 12019 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	161,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	51,
	s_rgctxIndices,
	248,
	s_rgctxValues,
	NULL,
};
