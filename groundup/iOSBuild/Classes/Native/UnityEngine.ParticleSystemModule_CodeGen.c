﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32)
extern void ParticleSystem_Emit_m8C3FCE4F94165CDF0B86326DDB5DB886C1D7B0CF ();
// 0x00000002 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem_Particle)
extern void ParticleSystem_Emit_m26C1CE51747F6F96A02AF1E56DDF3C3539FC926D ();
// 0x00000003 System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern void ParticleSystem_Play_mBB238026E6389F44C76498D31038FE7A8C47E3AA ();
// 0x00000004 System.Void UnityEngine.ParticleSystem::Play()
extern void ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F ();
// 0x00000005 System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern void ParticleSystem_Stop_m0EEE504756C56D38D3F8AB0FE73042A4612352F2 ();
// 0x00000006 System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
extern void ParticleSystem_Stop_m6A4F8F9868A2A39AE5C1A0A780716E0DCBFF6F92 ();
// 0x00000007 System.Void UnityEngine.ParticleSystem::Stop()
extern void ParticleSystem_Stop_m02FB082790DB4DEC7A8D0A56A1AC34D1E446099E ();
// 0x00000008 System.Void UnityEngine.ParticleSystem::Emit(System.Int32)
extern void ParticleSystem_Emit_m4C0873B2917D6C3E000609EA35B3C3F648B0BBC2 ();
// 0x00000009 System.Void UnityEngine.ParticleSystem::Emit_Internal(System.Int32)
extern void ParticleSystem_Emit_Internal_m1857956B7219B8232C1777E515706F8075C8B925 ();
// 0x0000000A System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem_EmitParams,System.Int32)
extern void ParticleSystem_Emit_mC0F1810F887D9EDE111F2307F2280CD0E4BA6AA2 ();
// 0x0000000B System.Void UnityEngine.ParticleSystem::EmitOld_Internal(UnityEngine.ParticleSystem_Particle&)
extern void ParticleSystem_EmitOld_Internal_m4313E5BD80E21011786EA12F2D2D9EFE9186320E ();
// 0x0000000C UnityEngine.ParticleSystem_EmissionModule UnityEngine.ParticleSystem::get_emission()
extern void ParticleSystem_get_emission_mA1204EAF07A6C6B3F65B45295797A1FFF64D343C ();
// 0x0000000D UnityEngine.ParticleSystem_VelocityOverLifetimeModule UnityEngine.ParticleSystem::get_velocityOverLifetime()
extern void ParticleSystem_get_velocityOverLifetime_mD620A6845B0D929F6D8F8A5F50037CE879E989D7 ();
// 0x0000000E System.Void UnityEngine.ParticleSystem::Emit_Injected(UnityEngine.ParticleSystem_EmitParams&,System.Int32)
extern void ParticleSystem_Emit_Injected_mB34A23399928EDC3111C060A2346A1EF63E1B9CC ();
// 0x0000000F System.Void UnityEngine.ParticleSystem_EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern void EmissionModule__ctor_mD6B4029B58ECFECE0567E7FD67962FEF52B15843_AdjustorThunk ();
// 0x00000010 System.Void UnityEngine.ParticleSystem_EmissionModule::set_enabled(System.Boolean)
extern void EmissionModule_set_enabled_m3896B441BDE0F0752A6D113012B20D5D31B16D36_AdjustorThunk ();
// 0x00000011 System.Void UnityEngine.ParticleSystem_EmissionModule::set_rateOverTimeMultiplier(System.Single)
extern void EmissionModule_set_rateOverTimeMultiplier_m3DE4B55DADA6962D5935E5B394182680AF4D8812_AdjustorThunk ();
// 0x00000012 System.Void UnityEngine.ParticleSystem_EmissionModule::set_enabled_Injected(UnityEngine.ParticleSystem_EmissionModule&,System.Boolean)
extern void EmissionModule_set_enabled_Injected_m80C70B60E49D3ADE643B12579CE8CD119BD7D5F4 ();
// 0x00000013 System.Void UnityEngine.ParticleSystem_EmissionModule::set_rateOverTimeMultiplier_Injected(UnityEngine.ParticleSystem_EmissionModule&,System.Single)
extern void EmissionModule_set_rateOverTimeMultiplier_Injected_m6127B571C988F227946A484A720B69FDEB648CC5 ();
// 0x00000014 System.Void UnityEngine.ParticleSystem_Particle::set_lifetime(System.Single)
extern void Particle_set_lifetime_m0DB60575386F2D365BCCCAB07538FC2BFF81EC17_AdjustorThunk ();
// 0x00000015 System.Void UnityEngine.ParticleSystem_Particle::set_position(UnityEngine.Vector3)
extern void Particle_set_position_m3E99F891841E8B03490433FAFF5B601A6D12BDEF_AdjustorThunk ();
// 0x00000016 System.Void UnityEngine.ParticleSystem_Particle::set_velocity(UnityEngine.Vector3)
extern void Particle_set_velocity_mD0476C793611AD570296960FB0CB8FECD387E99C_AdjustorThunk ();
// 0x00000017 System.Void UnityEngine.ParticleSystem_Particle::set_remainingLifetime(System.Single)
extern void Particle_set_remainingLifetime_mD6ABB0C19127BD86DE3723B443331E5968EE0E87_AdjustorThunk ();
// 0x00000018 System.Void UnityEngine.ParticleSystem_Particle::set_startLifetime(System.Single)
extern void Particle_set_startLifetime_mEEB2B63599B1E4D1B8B2CEE25F13A50F1BCE7BBE_AdjustorThunk ();
// 0x00000019 System.Void UnityEngine.ParticleSystem_Particle::set_startColor(UnityEngine.Color32)
extern void Particle_set_startColor_m67807C44D14862EBD8C030C1FE094E8438384AA6_AdjustorThunk ();
// 0x0000001A System.Void UnityEngine.ParticleSystem_Particle::set_randomSeed(System.UInt32)
extern void Particle_set_randomSeed_m1311237E65918DDD765FC4D6BAE85047D8B8CBCE_AdjustorThunk ();
// 0x0000001B System.Void UnityEngine.ParticleSystem_Particle::set_startSize(System.Single)
extern void Particle_set_startSize_m45B6CD1480219E30A96317D654B9439C8DB2DF87_AdjustorThunk ();
// 0x0000001C System.Void UnityEngine.ParticleSystem_Particle::set_rotation3D(UnityEngine.Vector3)
extern void Particle_set_rotation3D_m46DB39BFDEEF27C6119F5EEE2C0B1CA9093FC834_AdjustorThunk ();
// 0x0000001D System.Void UnityEngine.ParticleSystem_Particle::set_angularVelocity3D(UnityEngine.Vector3)
extern void Particle_set_angularVelocity3D_m0F282D7EE110DF290E04B2B99FEC697ED89BF4EF_AdjustorThunk ();
// 0x0000001E System.Void UnityEngine.ParticleSystem_MinMaxCurve::.ctor(System.Single)
extern void MinMaxCurve__ctor_mE5FDFD4ADB7EA897D19133CF82DC290577B4DD29_AdjustorThunk ();
// 0x0000001F UnityEngine.ParticleSystem_MinMaxCurve UnityEngine.ParticleSystem_MinMaxCurve::op_Implicit(System.Single)
extern void MinMaxCurve_op_Implicit_m998EE9F8D83B9545F63E2DFA304E99620F0F707F ();
// 0x00000020 System.Void UnityEngine.ParticleSystem_VelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern void VelocityOverLifetimeModule__ctor_m9A2FA3133A5C5E6930E83294EF9AC5EDB3E2C750_AdjustorThunk ();
// 0x00000021 System.Void UnityEngine.ParticleSystem_VelocityOverLifetimeModule::set_orbitalY(UnityEngine.ParticleSystem_MinMaxCurve)
extern void VelocityOverLifetimeModule_set_orbitalY_mBF7C7EB7DD994712DF8412168E3956FC424D2555_AdjustorThunk ();
// 0x00000022 System.Void UnityEngine.ParticleSystem_VelocityOverLifetimeModule::set_orbitalY_Injected(UnityEngine.ParticleSystem_VelocityOverLifetimeModule&,UnityEngine.ParticleSystem_MinMaxCurve&)
extern void VelocityOverLifetimeModule_set_orbitalY_Injected_mD5F9E8831271F242046E5A5BDBED8F8C989A6652 ();
// 0x00000023 System.Int32 UnityEngine.ParticleSystemRenderer::GetMeshes(UnityEngine.Mesh[])
extern void ParticleSystemRenderer_GetMeshes_m4DE519F198B6A36169F307F1FA5D76FA28316AD2 ();
static Il2CppMethodPointer s_methodPointers[35] = 
{
	ParticleSystem_Emit_m8C3FCE4F94165CDF0B86326DDB5DB886C1D7B0CF,
	ParticleSystem_Emit_m26C1CE51747F6F96A02AF1E56DDF3C3539FC926D,
	ParticleSystem_Play_mBB238026E6389F44C76498D31038FE7A8C47E3AA,
	ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F,
	ParticleSystem_Stop_m0EEE504756C56D38D3F8AB0FE73042A4612352F2,
	ParticleSystem_Stop_m6A4F8F9868A2A39AE5C1A0A780716E0DCBFF6F92,
	ParticleSystem_Stop_m02FB082790DB4DEC7A8D0A56A1AC34D1E446099E,
	ParticleSystem_Emit_m4C0873B2917D6C3E000609EA35B3C3F648B0BBC2,
	ParticleSystem_Emit_Internal_m1857956B7219B8232C1777E515706F8075C8B925,
	ParticleSystem_Emit_mC0F1810F887D9EDE111F2307F2280CD0E4BA6AA2,
	ParticleSystem_EmitOld_Internal_m4313E5BD80E21011786EA12F2D2D9EFE9186320E,
	ParticleSystem_get_emission_mA1204EAF07A6C6B3F65B45295797A1FFF64D343C,
	ParticleSystem_get_velocityOverLifetime_mD620A6845B0D929F6D8F8A5F50037CE879E989D7,
	ParticleSystem_Emit_Injected_mB34A23399928EDC3111C060A2346A1EF63E1B9CC,
	EmissionModule__ctor_mD6B4029B58ECFECE0567E7FD67962FEF52B15843_AdjustorThunk,
	EmissionModule_set_enabled_m3896B441BDE0F0752A6D113012B20D5D31B16D36_AdjustorThunk,
	EmissionModule_set_rateOverTimeMultiplier_m3DE4B55DADA6962D5935E5B394182680AF4D8812_AdjustorThunk,
	EmissionModule_set_enabled_Injected_m80C70B60E49D3ADE643B12579CE8CD119BD7D5F4,
	EmissionModule_set_rateOverTimeMultiplier_Injected_m6127B571C988F227946A484A720B69FDEB648CC5,
	Particle_set_lifetime_m0DB60575386F2D365BCCCAB07538FC2BFF81EC17_AdjustorThunk,
	Particle_set_position_m3E99F891841E8B03490433FAFF5B601A6D12BDEF_AdjustorThunk,
	Particle_set_velocity_mD0476C793611AD570296960FB0CB8FECD387E99C_AdjustorThunk,
	Particle_set_remainingLifetime_mD6ABB0C19127BD86DE3723B443331E5968EE0E87_AdjustorThunk,
	Particle_set_startLifetime_mEEB2B63599B1E4D1B8B2CEE25F13A50F1BCE7BBE_AdjustorThunk,
	Particle_set_startColor_m67807C44D14862EBD8C030C1FE094E8438384AA6_AdjustorThunk,
	Particle_set_randomSeed_m1311237E65918DDD765FC4D6BAE85047D8B8CBCE_AdjustorThunk,
	Particle_set_startSize_m45B6CD1480219E30A96317D654B9439C8DB2DF87_AdjustorThunk,
	Particle_set_rotation3D_m46DB39BFDEEF27C6119F5EEE2C0B1CA9093FC834_AdjustorThunk,
	Particle_set_angularVelocity3D_m0F282D7EE110DF290E04B2B99FEC697ED89BF4EF_AdjustorThunk,
	MinMaxCurve__ctor_mE5FDFD4ADB7EA897D19133CF82DC290577B4DD29_AdjustorThunk,
	MinMaxCurve_op_Implicit_m998EE9F8D83B9545F63E2DFA304E99620F0F707F,
	VelocityOverLifetimeModule__ctor_m9A2FA3133A5C5E6930E83294EF9AC5EDB3E2C750_AdjustorThunk,
	VelocityOverLifetimeModule_set_orbitalY_mBF7C7EB7DD994712DF8412168E3956FC424D2555_AdjustorThunk,
	VelocityOverLifetimeModule_set_orbitalY_Injected_mD5F9E8831271F242046E5A5BDBED8F8C989A6652,
	ParticleSystemRenderer_GetMeshes_m4DE519F198B6A36169F307F1FA5D76FA28316AD2,
};
static const int32_t s_InvokerIndices[35] = 
{
	1597,
	1598,
	31,
	23,
	736,
	31,
	23,
	32,
	32,
	1599,
	6,
	1600,
	1601,
	64,
	26,
	31,
	275,
	22,
	1019,
	275,
	1001,
	1001,
	275,
	275,
	1520,
	32,
	275,
	1001,
	1001,
	275,
	1602,
	26,
	1603,
	323,
	104,
};
extern const Il2CppCodeGenModule g_UnityEngine_ParticleSystemModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_ParticleSystemModuleCodeGenModule = 
{
	"UnityEngine.ParticleSystemModule.dll",
	35,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
