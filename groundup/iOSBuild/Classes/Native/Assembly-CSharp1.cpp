﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Actor
struct Actor_t7437E2FC69C71194ECD15A5DFE15FDA0FCDFF159;
// AmmoCounter
struct AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1;
// AmmoCounter[]
struct AmmoCounterU5BU5D_t0B6B561BE9AFA3B70A706D256998453B093A9D55;
// FillBarColorChange
struct FillBarColorChange_t810530569900F7083B6861A8D651A26DD2127C58;
// Health
struct Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D;
// Pickup
struct Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281;
// PlayerCharacterController
struct PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072;
// PlayerInputHandler
struct PlayerInputHandler_t2DF05A496167FB336DF5A1FD5D389972925D53C6;
// PlayerWeaponsManager
struct PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB;
// ProjectileBase
struct ProjectileBase_t0F3DE2AEFB78F25A28CCABF5901555BB8A4AAF6E;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<AmmoCounter>
struct List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA;
// System.Collections.Generic.List`1<WeaponController>
struct List_1_t1E51F67C63BDBE2C7EC22ED991734600EBB3FDA1;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Events.UnityAction`1<PlayerCharacterController>
struct UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_tB994D127B02789CE2010397AEF756615E5F84FDC;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t0064196FB7635B812E65BA9FD08D39F68C75DCD9;
// UnityEngine.Events.UnityAction`1<WeaponController>
struct UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F;
// UnityEngine.Events.UnityAction`2<System.Object,System.Int32>
struct UnityAction_2_t4BFEF410445D61DFA0D55D04E20ED20869EAD343;
// UnityEngine.Events.UnityAction`2<System.Single,UnityEngine.GameObject>
struct UnityAction_2_t9F6AFDD84F09137D22C285B2BC86A5EECF3C3D83;
// UnityEngine.Events.UnityAction`2<WeaponController,System.Int32>
struct UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// WeaponController
struct WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C;
// WeaponController[]
struct WeaponControllerU5BU5D_t829FA4C36C3C1FC8624A0CCFC79C0FB92EFA6B54;
// WeaponFuelCellHandler
struct WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A;
// WeaponHUDManager
struct WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773;
// WeaponPickup
struct WeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2;
// WorldspaceHealthBar
struct WorldspaceHealthBar_t74F5087839EF95CD78752D943A539D313AA3B9E3;

IL2CPP_EXTERN_C RuntimeClass* BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281_m7BE6031779B73EEFDE3F1F50ABEA23A1EF8DE198_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_m6268AA831D180B5189871925A88950D5E2D9F873_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisWeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C_m4A2B12FF0B97DC381C3C3D31D3090889AA93D808_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m335E18CE04D2AD7313D89DBEA3A88EDB76255113_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DebugUtility_HandleErrorIfNullFindObject_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_TisWeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773_m2AF55E66787FD7690127282340AB8101B5E1032D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DebugUtility_HandleErrorIfNullGetComponent_TisAmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1_TisWeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773_mD00757FACC332D1A15B551F8489368227494D2FE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DebugUtility_HandleErrorIfNullGetComponent_TisPickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281_TisWeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2_m4356919BC776A07298536A42B1D737045A9768B2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DebugUtility_HandleErrorIfNullGetComponent_TisWeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C_TisWeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A_mE36EED9FBC1F3EF970D00FBD4DFF4525C767674D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisAmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1_m39A810E8DC3B5DED898125C40BAFAB7B76097890_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mB69FED340E5222A49CA180DAD48E3BD3D31B05A7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_mF496CA03E4E0DC9E1F02770EE5B86E17A49212B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mEDA6C28C20A6EEA03532D2EF4E7196BCB85A6859_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m104F10F39220D74146D1DE9669E44D6AB9E09802_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m612C715B84047B764A1764A095EDD7E7CF2EA032_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_m59AFFA4CEAD4A223D456E870A02391948EB93FA6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m1CDF66D563B03D37B37264800222D4F3B307EDA0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_1__ctor_m4317C8EF454DAF2A7178F5A3854C7271732082FA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_1__ctor_m9362988F4BB4CD4FE51DB792660114863167415B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_2__ctor_m94489AC66327CA797AFC605B03BED338805074C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WeaponHUDManager_AddWeapon_m40F4058D81FFCA0640432F656F1506825190559D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WeaponHUDManager_ChangeWeapon_mF6F576F5B15E561DF5D09CCE7417BA699E072683_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WeaponHUDManager_RemoveWeapon_m01F57E6BDDB655C20A2308D7D15FD8201AE58F15_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WeaponPickup_OnPicked_m19C08F01C850D8A2FA89CC1A961006A541A2AF6C_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t WeaponFuelCellHandler_Start_mC5BDF5D098F7138A800B85BA4D934514C8DDD3FC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WeaponFuelCellHandler_Update_mDB4277536619C89299A9072410D1462BADEC4A47_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WeaponHUDManager_AddWeapon_m40F4058D81FFCA0640432F656F1506825190559D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WeaponHUDManager_ChangeWeapon_mF6F576F5B15E561DF5D09CCE7417BA699E072683_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WeaponHUDManager_RemoveWeapon_m01F57E6BDDB655C20A2308D7D15FD8201AE58F15_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WeaponHUDManager_Start_mB3691A6FADA9D605A53BD1B56E24442CBF7B97CC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WeaponHUDManager__ctor_m1B5D93240B1B0EDD834D7294AD637F936C66D180_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WeaponPickup_OnPicked_m19C08F01C850D8A2FA89CC1A961006A541A2AF6C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WeaponPickup_Start_mDD98BD5363E0B4D89AA720E4BD6D942924A5A7DB_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<AmmoCounter>
struct  List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AmmoCounterU5BU5D_t0B6B561BE9AFA3B70A706D256998453B093A9D55* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0, ____items_1)); }
	inline AmmoCounterU5BU5D_t0B6B561BE9AFA3B70A706D256998453B093A9D55* get__items_1() const { return ____items_1; }
	inline AmmoCounterU5BU5D_t0B6B561BE9AFA3B70A706D256998453B093A9D55** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AmmoCounterU5BU5D_t0B6B561BE9AFA3B70A706D256998453B093A9D55* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	AmmoCounterU5BU5D_t0B6B561BE9AFA3B70A706D256998453B093A9D55* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0_StaticFields, ____emptyArray_5)); }
	inline AmmoCounterU5BU5D_t0B6B561BE9AFA3B70A706D256998453B093A9D55* get__emptyArray_5() const { return ____emptyArray_5; }
	inline AmmoCounterU5BU5D_t0B6B561BE9AFA3B70A706D256998453B093A9D55** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(AmmoCounterU5BU5D_t0B6B561BE9AFA3B70A706D256998453B093A9D55* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// CrosshairData
struct  CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC 
{
public:
	// UnityEngine.Sprite CrosshairData::crosshairSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___crosshairSprite_0;
	// System.Int32 CrosshairData::crosshairSize
	int32_t ___crosshairSize_1;
	// UnityEngine.Color CrosshairData::crosshairColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___crosshairColor_2;

public:
	inline static int32_t get_offset_of_crosshairSprite_0() { return static_cast<int32_t>(offsetof(CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC, ___crosshairSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_crosshairSprite_0() const { return ___crosshairSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_crosshairSprite_0() { return &___crosshairSprite_0; }
	inline void set_crosshairSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___crosshairSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___crosshairSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_crosshairSize_1() { return static_cast<int32_t>(offsetof(CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC, ___crosshairSize_1)); }
	inline int32_t get_crosshairSize_1() const { return ___crosshairSize_1; }
	inline int32_t* get_address_of_crosshairSize_1() { return &___crosshairSize_1; }
	inline void set_crosshairSize_1(int32_t value)
	{
		___crosshairSize_1 = value;
	}

	inline static int32_t get_offset_of_crosshairColor_2() { return static_cast<int32_t>(offsetof(CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC, ___crosshairColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_crosshairColor_2() const { return ___crosshairColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_crosshairColor_2() { return &___crosshairColor_2; }
	inline void set_crosshairColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___crosshairColor_2 = value;
	}
};

// Native definition for P/Invoke marshalling of CrosshairData
struct CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___crosshairSprite_0;
	int32_t ___crosshairSize_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___crosshairColor_2;
};
// Native definition for COM marshalling of CrosshairData
struct CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___crosshairSprite_0;
	int32_t ___crosshairSize_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___crosshairColor_2;
};

// PlayerWeaponsManager_WeaponSwitchState
struct  WeaponSwitchState_t84D3B94309AFE56D47B176D8307E5046392D3390 
{
public:
	// System.Int32 PlayerWeaponsManager_WeaponSwitchState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WeaponSwitchState_t84D3B94309AFE56D47B176D8307E5046392D3390, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UI.Image_FillMethod
struct  FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5 
{
public:
	// System.Int32 UnityEngine.UI.Image_FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image_Type
struct  Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A 
{
public:
	// System.Int32 UnityEngine.UI.Image_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// WeaponShootType
struct  WeaponShootType_tB6F9D349B980CDF605897083591BB25137432A11 
{
public:
	// System.Int32 WeaponShootType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WeaponShootType_tB6F9D349B980CDF605897083591BB25137432A11, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<PlayerCharacterController>
struct  UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<WeaponController>
struct  UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`2<WeaponController,System.Int32>
struct  UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.RectTransform
struct  RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20  : public Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA
{
public:

public:
};

struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_StaticFields
{
public:
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// AmmoCounter
struct  AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.CanvasGroup AmmoCounter::canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___canvasGroup_4;
	// UnityEngine.UI.Image AmmoCounter::weaponImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___weaponImage_5;
	// UnityEngine.UI.Image AmmoCounter::ammoBackgroundImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___ammoBackgroundImage_6;
	// UnityEngine.UI.Image AmmoCounter::ammoFillImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___ammoFillImage_7;
	// TMPro.TextMeshProUGUI AmmoCounter::weaponIndexText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___weaponIndexText_8;
	// System.Single AmmoCounter::unselectedOpacity
	float ___unselectedOpacity_9;
	// UnityEngine.Vector3 AmmoCounter::unselectedScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___unselectedScale_10;
	// UnityEngine.GameObject AmmoCounter::controlKeysRoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___controlKeysRoot_11;
	// FillBarColorChange AmmoCounter::FillBarColorChange
	FillBarColorChange_t810530569900F7083B6861A8D651A26DD2127C58 * ___FillBarColorChange_12;
	// System.Single AmmoCounter::ammoFillMovementSharpness
	float ___ammoFillMovementSharpness_13;
	// System.Int32 AmmoCounter::<weaponCounterIndex>k__BackingField
	int32_t ___U3CweaponCounterIndexU3Ek__BackingField_14;
	// PlayerWeaponsManager AmmoCounter::m_PlayerWeaponsManager
	PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * ___m_PlayerWeaponsManager_15;
	// WeaponController AmmoCounter::m_Weapon
	WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * ___m_Weapon_16;

public:
	inline static int32_t get_offset_of_canvasGroup_4() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___canvasGroup_4)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_canvasGroup_4() const { return ___canvasGroup_4; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_canvasGroup_4() { return &___canvasGroup_4; }
	inline void set_canvasGroup_4(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___canvasGroup_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasGroup_4), (void*)value);
	}

	inline static int32_t get_offset_of_weaponImage_5() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___weaponImage_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_weaponImage_5() const { return ___weaponImage_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_weaponImage_5() { return &___weaponImage_5; }
	inline void set_weaponImage_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___weaponImage_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weaponImage_5), (void*)value);
	}

	inline static int32_t get_offset_of_ammoBackgroundImage_6() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___ammoBackgroundImage_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_ammoBackgroundImage_6() const { return ___ammoBackgroundImage_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_ammoBackgroundImage_6() { return &___ammoBackgroundImage_6; }
	inline void set_ammoBackgroundImage_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___ammoBackgroundImage_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ammoBackgroundImage_6), (void*)value);
	}

	inline static int32_t get_offset_of_ammoFillImage_7() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___ammoFillImage_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_ammoFillImage_7() const { return ___ammoFillImage_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_ammoFillImage_7() { return &___ammoFillImage_7; }
	inline void set_ammoFillImage_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___ammoFillImage_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ammoFillImage_7), (void*)value);
	}

	inline static int32_t get_offset_of_weaponIndexText_8() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___weaponIndexText_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_weaponIndexText_8() const { return ___weaponIndexText_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_weaponIndexText_8() { return &___weaponIndexText_8; }
	inline void set_weaponIndexText_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___weaponIndexText_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weaponIndexText_8), (void*)value);
	}

	inline static int32_t get_offset_of_unselectedOpacity_9() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___unselectedOpacity_9)); }
	inline float get_unselectedOpacity_9() const { return ___unselectedOpacity_9; }
	inline float* get_address_of_unselectedOpacity_9() { return &___unselectedOpacity_9; }
	inline void set_unselectedOpacity_9(float value)
	{
		___unselectedOpacity_9 = value;
	}

	inline static int32_t get_offset_of_unselectedScale_10() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___unselectedScale_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_unselectedScale_10() const { return ___unselectedScale_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_unselectedScale_10() { return &___unselectedScale_10; }
	inline void set_unselectedScale_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___unselectedScale_10 = value;
	}

	inline static int32_t get_offset_of_controlKeysRoot_11() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___controlKeysRoot_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_controlKeysRoot_11() const { return ___controlKeysRoot_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_controlKeysRoot_11() { return &___controlKeysRoot_11; }
	inline void set_controlKeysRoot_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___controlKeysRoot_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controlKeysRoot_11), (void*)value);
	}

	inline static int32_t get_offset_of_FillBarColorChange_12() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___FillBarColorChange_12)); }
	inline FillBarColorChange_t810530569900F7083B6861A8D651A26DD2127C58 * get_FillBarColorChange_12() const { return ___FillBarColorChange_12; }
	inline FillBarColorChange_t810530569900F7083B6861A8D651A26DD2127C58 ** get_address_of_FillBarColorChange_12() { return &___FillBarColorChange_12; }
	inline void set_FillBarColorChange_12(FillBarColorChange_t810530569900F7083B6861A8D651A26DD2127C58 * value)
	{
		___FillBarColorChange_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FillBarColorChange_12), (void*)value);
	}

	inline static int32_t get_offset_of_ammoFillMovementSharpness_13() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___ammoFillMovementSharpness_13)); }
	inline float get_ammoFillMovementSharpness_13() const { return ___ammoFillMovementSharpness_13; }
	inline float* get_address_of_ammoFillMovementSharpness_13() { return &___ammoFillMovementSharpness_13; }
	inline void set_ammoFillMovementSharpness_13(float value)
	{
		___ammoFillMovementSharpness_13 = value;
	}

	inline static int32_t get_offset_of_U3CweaponCounterIndexU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___U3CweaponCounterIndexU3Ek__BackingField_14)); }
	inline int32_t get_U3CweaponCounterIndexU3Ek__BackingField_14() const { return ___U3CweaponCounterIndexU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CweaponCounterIndexU3Ek__BackingField_14() { return &___U3CweaponCounterIndexU3Ek__BackingField_14; }
	inline void set_U3CweaponCounterIndexU3Ek__BackingField_14(int32_t value)
	{
		___U3CweaponCounterIndexU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_PlayerWeaponsManager_15() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___m_PlayerWeaponsManager_15)); }
	inline PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * get_m_PlayerWeaponsManager_15() const { return ___m_PlayerWeaponsManager_15; }
	inline PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB ** get_address_of_m_PlayerWeaponsManager_15() { return &___m_PlayerWeaponsManager_15; }
	inline void set_m_PlayerWeaponsManager_15(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * value)
	{
		___m_PlayerWeaponsManager_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerWeaponsManager_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_Weapon_16() { return static_cast<int32_t>(offsetof(AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1, ___m_Weapon_16)); }
	inline WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * get_m_Weapon_16() const { return ___m_Weapon_16; }
	inline WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C ** get_address_of_m_Weapon_16() { return &___m_Weapon_16; }
	inline void set_m_Weapon_16(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * value)
	{
		___m_Weapon_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Weapon_16), (void*)value);
	}
};


// Health
struct  Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Health::maxHealth
	float ___maxHealth_4;
	// System.Single Health::criticalHealthRatio
	float ___criticalHealthRatio_5;
	// UnityEngine.Events.UnityAction`2<System.Single,UnityEngine.GameObject> Health::onDamaged
	UnityAction_2_t9F6AFDD84F09137D22C285B2BC86A5EECF3C3D83 * ___onDamaged_6;
	// UnityEngine.Events.UnityAction`1<System.Single> Health::onHealed
	UnityAction_1_t0064196FB7635B812E65BA9FD08D39F68C75DCD9 * ___onHealed_7;
	// UnityEngine.Events.UnityAction Health::onDie
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onDie_8;
	// System.Single Health::<currentHealth>k__BackingField
	float ___U3CcurrentHealthU3Ek__BackingField_9;
	// System.Boolean Health::<invincible>k__BackingField
	bool ___U3CinvincibleU3Ek__BackingField_10;
	// System.Boolean Health::m_IsDead
	bool ___m_IsDead_11;

public:
	inline static int32_t get_offset_of_maxHealth_4() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___maxHealth_4)); }
	inline float get_maxHealth_4() const { return ___maxHealth_4; }
	inline float* get_address_of_maxHealth_4() { return &___maxHealth_4; }
	inline void set_maxHealth_4(float value)
	{
		___maxHealth_4 = value;
	}

	inline static int32_t get_offset_of_criticalHealthRatio_5() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___criticalHealthRatio_5)); }
	inline float get_criticalHealthRatio_5() const { return ___criticalHealthRatio_5; }
	inline float* get_address_of_criticalHealthRatio_5() { return &___criticalHealthRatio_5; }
	inline void set_criticalHealthRatio_5(float value)
	{
		___criticalHealthRatio_5 = value;
	}

	inline static int32_t get_offset_of_onDamaged_6() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___onDamaged_6)); }
	inline UnityAction_2_t9F6AFDD84F09137D22C285B2BC86A5EECF3C3D83 * get_onDamaged_6() const { return ___onDamaged_6; }
	inline UnityAction_2_t9F6AFDD84F09137D22C285B2BC86A5EECF3C3D83 ** get_address_of_onDamaged_6() { return &___onDamaged_6; }
	inline void set_onDamaged_6(UnityAction_2_t9F6AFDD84F09137D22C285B2BC86A5EECF3C3D83 * value)
	{
		___onDamaged_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onDamaged_6), (void*)value);
	}

	inline static int32_t get_offset_of_onHealed_7() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___onHealed_7)); }
	inline UnityAction_1_t0064196FB7635B812E65BA9FD08D39F68C75DCD9 * get_onHealed_7() const { return ___onHealed_7; }
	inline UnityAction_1_t0064196FB7635B812E65BA9FD08D39F68C75DCD9 ** get_address_of_onHealed_7() { return &___onHealed_7; }
	inline void set_onHealed_7(UnityAction_1_t0064196FB7635B812E65BA9FD08D39F68C75DCD9 * value)
	{
		___onHealed_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onHealed_7), (void*)value);
	}

	inline static int32_t get_offset_of_onDie_8() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___onDie_8)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onDie_8() const { return ___onDie_8; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onDie_8() { return &___onDie_8; }
	inline void set_onDie_8(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onDie_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onDie_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcurrentHealthU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___U3CcurrentHealthU3Ek__BackingField_9)); }
	inline float get_U3CcurrentHealthU3Ek__BackingField_9() const { return ___U3CcurrentHealthU3Ek__BackingField_9; }
	inline float* get_address_of_U3CcurrentHealthU3Ek__BackingField_9() { return &___U3CcurrentHealthU3Ek__BackingField_9; }
	inline void set_U3CcurrentHealthU3Ek__BackingField_9(float value)
	{
		___U3CcurrentHealthU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CinvincibleU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___U3CinvincibleU3Ek__BackingField_10)); }
	inline bool get_U3CinvincibleU3Ek__BackingField_10() const { return ___U3CinvincibleU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CinvincibleU3Ek__BackingField_10() { return &___U3CinvincibleU3Ek__BackingField_10; }
	inline void set_U3CinvincibleU3Ek__BackingField_10(bool value)
	{
		___U3CinvincibleU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_m_IsDead_11() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___m_IsDead_11)); }
	inline bool get_m_IsDead_11() const { return ___m_IsDead_11; }
	inline bool* get_address_of_m_IsDead_11() { return &___m_IsDead_11; }
	inline void set_m_IsDead_11(bool value)
	{
		___m_IsDead_11 = value;
	}
};


// Pickup
struct  Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Pickup::verticalBobFrequency
	float ___verticalBobFrequency_4;
	// System.Single Pickup::bobbingAmount
	float ___bobbingAmount_5;
	// System.Single Pickup::rotatingSpeed
	float ___rotatingSpeed_6;
	// UnityEngine.AudioClip Pickup::pickupSFX
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___pickupSFX_7;
	// UnityEngine.GameObject Pickup::pickupVFXPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickupVFXPrefab_8;
	// UnityEngine.Events.UnityAction`1<PlayerCharacterController> Pickup::onPick
	UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D * ___onPick_9;
	// UnityEngine.Rigidbody Pickup::<pickupRigidbody>k__BackingField
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___U3CpickupRigidbodyU3Ek__BackingField_10;
	// UnityEngine.Collider Pickup::m_Collider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___m_Collider_11;
	// UnityEngine.Vector3 Pickup::m_StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_StartPosition_12;
	// System.Boolean Pickup::m_HasPlayedFeedback
	bool ___m_HasPlayedFeedback_13;

public:
	inline static int32_t get_offset_of_verticalBobFrequency_4() { return static_cast<int32_t>(offsetof(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281, ___verticalBobFrequency_4)); }
	inline float get_verticalBobFrequency_4() const { return ___verticalBobFrequency_4; }
	inline float* get_address_of_verticalBobFrequency_4() { return &___verticalBobFrequency_4; }
	inline void set_verticalBobFrequency_4(float value)
	{
		___verticalBobFrequency_4 = value;
	}

	inline static int32_t get_offset_of_bobbingAmount_5() { return static_cast<int32_t>(offsetof(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281, ___bobbingAmount_5)); }
	inline float get_bobbingAmount_5() const { return ___bobbingAmount_5; }
	inline float* get_address_of_bobbingAmount_5() { return &___bobbingAmount_5; }
	inline void set_bobbingAmount_5(float value)
	{
		___bobbingAmount_5 = value;
	}

	inline static int32_t get_offset_of_rotatingSpeed_6() { return static_cast<int32_t>(offsetof(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281, ___rotatingSpeed_6)); }
	inline float get_rotatingSpeed_6() const { return ___rotatingSpeed_6; }
	inline float* get_address_of_rotatingSpeed_6() { return &___rotatingSpeed_6; }
	inline void set_rotatingSpeed_6(float value)
	{
		___rotatingSpeed_6 = value;
	}

	inline static int32_t get_offset_of_pickupSFX_7() { return static_cast<int32_t>(offsetof(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281, ___pickupSFX_7)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_pickupSFX_7() const { return ___pickupSFX_7; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_pickupSFX_7() { return &___pickupSFX_7; }
	inline void set_pickupSFX_7(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___pickupSFX_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pickupSFX_7), (void*)value);
	}

	inline static int32_t get_offset_of_pickupVFXPrefab_8() { return static_cast<int32_t>(offsetof(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281, ___pickupVFXPrefab_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickupVFXPrefab_8() const { return ___pickupVFXPrefab_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickupVFXPrefab_8() { return &___pickupVFXPrefab_8; }
	inline void set_pickupVFXPrefab_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickupVFXPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pickupVFXPrefab_8), (void*)value);
	}

	inline static int32_t get_offset_of_onPick_9() { return static_cast<int32_t>(offsetof(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281, ___onPick_9)); }
	inline UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D * get_onPick_9() const { return ___onPick_9; }
	inline UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D ** get_address_of_onPick_9() { return &___onPick_9; }
	inline void set_onPick_9(UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D * value)
	{
		___onPick_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPick_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpickupRigidbodyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281, ___U3CpickupRigidbodyU3Ek__BackingField_10)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_U3CpickupRigidbodyU3Ek__BackingField_10() const { return ___U3CpickupRigidbodyU3Ek__BackingField_10; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_U3CpickupRigidbodyU3Ek__BackingField_10() { return &___U3CpickupRigidbodyU3Ek__BackingField_10; }
	inline void set_U3CpickupRigidbodyU3Ek__BackingField_10(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___U3CpickupRigidbodyU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpickupRigidbodyU3Ek__BackingField_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Collider_11() { return static_cast<int32_t>(offsetof(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281, ___m_Collider_11)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_m_Collider_11() const { return ___m_Collider_11; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_m_Collider_11() { return &___m_Collider_11; }
	inline void set_m_Collider_11(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___m_Collider_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Collider_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartPosition_12() { return static_cast<int32_t>(offsetof(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281, ___m_StartPosition_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_StartPosition_12() const { return ___m_StartPosition_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_StartPosition_12() { return &___m_StartPosition_12; }
	inline void set_m_StartPosition_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_StartPosition_12 = value;
	}

	inline static int32_t get_offset_of_m_HasPlayedFeedback_13() { return static_cast<int32_t>(offsetof(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281, ___m_HasPlayedFeedback_13)); }
	inline bool get_m_HasPlayedFeedback_13() const { return ___m_HasPlayedFeedback_13; }
	inline bool* get_address_of_m_HasPlayedFeedback_13() { return &___m_HasPlayedFeedback_13; }
	inline void set_m_HasPlayedFeedback_13(bool value)
	{
		___m_HasPlayedFeedback_13 = value;
	}
};


// PlayerCharacterController
struct  PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera PlayerCharacterController::playerCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___playerCamera_4;
	// UnityEngine.AudioSource PlayerCharacterController::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_5;
	// System.Single PlayerCharacterController::gravityDownForce
	float ___gravityDownForce_6;
	// UnityEngine.LayerMask PlayerCharacterController::groundCheckLayers
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___groundCheckLayers_7;
	// System.Single PlayerCharacterController::groundCheckDistance
	float ___groundCheckDistance_8;
	// System.Single PlayerCharacterController::maxSpeedOnGround
	float ___maxSpeedOnGround_9;
	// System.Single PlayerCharacterController::movementSharpnessOnGround
	float ___movementSharpnessOnGround_10;
	// System.Single PlayerCharacterController::maxSpeedCrouchedRatio
	float ___maxSpeedCrouchedRatio_11;
	// System.Single PlayerCharacterController::maxSpeedInAir
	float ___maxSpeedInAir_12;
	// System.Single PlayerCharacterController::accelerationSpeedInAir
	float ___accelerationSpeedInAir_13;
	// System.Single PlayerCharacterController::sprintSpeedModifier
	float ___sprintSpeedModifier_14;
	// System.Single PlayerCharacterController::killHeight
	float ___killHeight_15;
	// System.Single PlayerCharacterController::rotationSpeed
	float ___rotationSpeed_16;
	// System.Single PlayerCharacterController::aimingRotationMultiplier
	float ___aimingRotationMultiplier_17;
	// System.Single PlayerCharacterController::jumpForce
	float ___jumpForce_18;
	// System.Single PlayerCharacterController::cameraHeightRatio
	float ___cameraHeightRatio_19;
	// System.Single PlayerCharacterController::capsuleHeightStanding
	float ___capsuleHeightStanding_20;
	// System.Single PlayerCharacterController::capsuleHeightCrouching
	float ___capsuleHeightCrouching_21;
	// System.Single PlayerCharacterController::crouchingSharpness
	float ___crouchingSharpness_22;
	// System.Single PlayerCharacterController::footstepSFXFrequency
	float ___footstepSFXFrequency_23;
	// System.Single PlayerCharacterController::footstepSFXFrequencyWhileSprinting
	float ___footstepSFXFrequencyWhileSprinting_24;
	// UnityEngine.AudioClip PlayerCharacterController::footstepSFX
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___footstepSFX_25;
	// UnityEngine.AudioClip PlayerCharacterController::jumpSFX
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___jumpSFX_26;
	// UnityEngine.AudioClip PlayerCharacterController::landSFX
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___landSFX_27;
	// UnityEngine.AudioClip PlayerCharacterController::fallDamageSFX
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___fallDamageSFX_28;
	// System.Boolean PlayerCharacterController::recievesFallDamage
	bool ___recievesFallDamage_29;
	// System.Single PlayerCharacterController::minSpeedForFallDamage
	float ___minSpeedForFallDamage_30;
	// System.Single PlayerCharacterController::maxSpeedForFallDamage
	float ___maxSpeedForFallDamage_31;
	// System.Single PlayerCharacterController::fallDamageAtMinSpeed
	float ___fallDamageAtMinSpeed_32;
	// System.Single PlayerCharacterController::fallDamageAtMaxSpeed
	float ___fallDamageAtMaxSpeed_33;
	// UnityEngine.Events.UnityAction`1<System.Boolean> PlayerCharacterController::onStanceChanged
	UnityAction_1_tB994D127B02789CE2010397AEF756615E5F84FDC * ___onStanceChanged_34;
	// UnityEngine.Vector3 PlayerCharacterController::<characterVelocity>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CcharacterVelocityU3Ek__BackingField_35;
	// System.Boolean PlayerCharacterController::<isGrounded>k__BackingField
	bool ___U3CisGroundedU3Ek__BackingField_36;
	// System.Boolean PlayerCharacterController::<hasJumpedThisFrame>k__BackingField
	bool ___U3ChasJumpedThisFrameU3Ek__BackingField_37;
	// System.Boolean PlayerCharacterController::<isDead>k__BackingField
	bool ___U3CisDeadU3Ek__BackingField_38;
	// System.Boolean PlayerCharacterController::<isCrouching>k__BackingField
	bool ___U3CisCrouchingU3Ek__BackingField_39;
	// Health PlayerCharacterController::m_Health
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * ___m_Health_40;
	// PlayerInputHandler PlayerCharacterController::m_InputHandler
	PlayerInputHandler_t2DF05A496167FB336DF5A1FD5D389972925D53C6 * ___m_InputHandler_41;
	// UnityEngine.CharacterController PlayerCharacterController::m_Controller
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___m_Controller_42;
	// PlayerWeaponsManager PlayerCharacterController::m_WeaponsManager
	PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * ___m_WeaponsManager_43;
	// Actor PlayerCharacterController::m_Actor
	Actor_t7437E2FC69C71194ECD15A5DFE15FDA0FCDFF159 * ___m_Actor_44;
	// UnityEngine.Vector3 PlayerCharacterController::m_GroundNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_GroundNormal_45;
	// UnityEngine.Vector3 PlayerCharacterController::m_CharacterVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_CharacterVelocity_46;
	// UnityEngine.Vector3 PlayerCharacterController::m_LatestImpactSpeed
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_LatestImpactSpeed_47;
	// System.Single PlayerCharacterController::m_LastTimeJumped
	float ___m_LastTimeJumped_48;
	// System.Single PlayerCharacterController::m_CameraVerticalAngle
	float ___m_CameraVerticalAngle_49;
	// System.Single PlayerCharacterController::m_footstepDistanceCounter
	float ___m_footstepDistanceCounter_50;
	// System.Single PlayerCharacterController::m_TargetCharacterHeight
	float ___m_TargetCharacterHeight_51;

public:
	inline static int32_t get_offset_of_playerCamera_4() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___playerCamera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_playerCamera_4() const { return ___playerCamera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_playerCamera_4() { return &___playerCamera_4; }
	inline void set_playerCamera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___playerCamera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerCamera_4), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_5() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___audioSource_5)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_5() const { return ___audioSource_5; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_5() { return &___audioSource_5; }
	inline void set_audioSource_5(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_5), (void*)value);
	}

	inline static int32_t get_offset_of_gravityDownForce_6() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___gravityDownForce_6)); }
	inline float get_gravityDownForce_6() const { return ___gravityDownForce_6; }
	inline float* get_address_of_gravityDownForce_6() { return &___gravityDownForce_6; }
	inline void set_gravityDownForce_6(float value)
	{
		___gravityDownForce_6 = value;
	}

	inline static int32_t get_offset_of_groundCheckLayers_7() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___groundCheckLayers_7)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_groundCheckLayers_7() const { return ___groundCheckLayers_7; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_groundCheckLayers_7() { return &___groundCheckLayers_7; }
	inline void set_groundCheckLayers_7(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___groundCheckLayers_7 = value;
	}

	inline static int32_t get_offset_of_groundCheckDistance_8() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___groundCheckDistance_8)); }
	inline float get_groundCheckDistance_8() const { return ___groundCheckDistance_8; }
	inline float* get_address_of_groundCheckDistance_8() { return &___groundCheckDistance_8; }
	inline void set_groundCheckDistance_8(float value)
	{
		___groundCheckDistance_8 = value;
	}

	inline static int32_t get_offset_of_maxSpeedOnGround_9() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___maxSpeedOnGround_9)); }
	inline float get_maxSpeedOnGround_9() const { return ___maxSpeedOnGround_9; }
	inline float* get_address_of_maxSpeedOnGround_9() { return &___maxSpeedOnGround_9; }
	inline void set_maxSpeedOnGround_9(float value)
	{
		___maxSpeedOnGround_9 = value;
	}

	inline static int32_t get_offset_of_movementSharpnessOnGround_10() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___movementSharpnessOnGround_10)); }
	inline float get_movementSharpnessOnGround_10() const { return ___movementSharpnessOnGround_10; }
	inline float* get_address_of_movementSharpnessOnGround_10() { return &___movementSharpnessOnGround_10; }
	inline void set_movementSharpnessOnGround_10(float value)
	{
		___movementSharpnessOnGround_10 = value;
	}

	inline static int32_t get_offset_of_maxSpeedCrouchedRatio_11() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___maxSpeedCrouchedRatio_11)); }
	inline float get_maxSpeedCrouchedRatio_11() const { return ___maxSpeedCrouchedRatio_11; }
	inline float* get_address_of_maxSpeedCrouchedRatio_11() { return &___maxSpeedCrouchedRatio_11; }
	inline void set_maxSpeedCrouchedRatio_11(float value)
	{
		___maxSpeedCrouchedRatio_11 = value;
	}

	inline static int32_t get_offset_of_maxSpeedInAir_12() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___maxSpeedInAir_12)); }
	inline float get_maxSpeedInAir_12() const { return ___maxSpeedInAir_12; }
	inline float* get_address_of_maxSpeedInAir_12() { return &___maxSpeedInAir_12; }
	inline void set_maxSpeedInAir_12(float value)
	{
		___maxSpeedInAir_12 = value;
	}

	inline static int32_t get_offset_of_accelerationSpeedInAir_13() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___accelerationSpeedInAir_13)); }
	inline float get_accelerationSpeedInAir_13() const { return ___accelerationSpeedInAir_13; }
	inline float* get_address_of_accelerationSpeedInAir_13() { return &___accelerationSpeedInAir_13; }
	inline void set_accelerationSpeedInAir_13(float value)
	{
		___accelerationSpeedInAir_13 = value;
	}

	inline static int32_t get_offset_of_sprintSpeedModifier_14() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___sprintSpeedModifier_14)); }
	inline float get_sprintSpeedModifier_14() const { return ___sprintSpeedModifier_14; }
	inline float* get_address_of_sprintSpeedModifier_14() { return &___sprintSpeedModifier_14; }
	inline void set_sprintSpeedModifier_14(float value)
	{
		___sprintSpeedModifier_14 = value;
	}

	inline static int32_t get_offset_of_killHeight_15() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___killHeight_15)); }
	inline float get_killHeight_15() const { return ___killHeight_15; }
	inline float* get_address_of_killHeight_15() { return &___killHeight_15; }
	inline void set_killHeight_15(float value)
	{
		___killHeight_15 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_16() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___rotationSpeed_16)); }
	inline float get_rotationSpeed_16() const { return ___rotationSpeed_16; }
	inline float* get_address_of_rotationSpeed_16() { return &___rotationSpeed_16; }
	inline void set_rotationSpeed_16(float value)
	{
		___rotationSpeed_16 = value;
	}

	inline static int32_t get_offset_of_aimingRotationMultiplier_17() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___aimingRotationMultiplier_17)); }
	inline float get_aimingRotationMultiplier_17() const { return ___aimingRotationMultiplier_17; }
	inline float* get_address_of_aimingRotationMultiplier_17() { return &___aimingRotationMultiplier_17; }
	inline void set_aimingRotationMultiplier_17(float value)
	{
		___aimingRotationMultiplier_17 = value;
	}

	inline static int32_t get_offset_of_jumpForce_18() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___jumpForce_18)); }
	inline float get_jumpForce_18() const { return ___jumpForce_18; }
	inline float* get_address_of_jumpForce_18() { return &___jumpForce_18; }
	inline void set_jumpForce_18(float value)
	{
		___jumpForce_18 = value;
	}

	inline static int32_t get_offset_of_cameraHeightRatio_19() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___cameraHeightRatio_19)); }
	inline float get_cameraHeightRatio_19() const { return ___cameraHeightRatio_19; }
	inline float* get_address_of_cameraHeightRatio_19() { return &___cameraHeightRatio_19; }
	inline void set_cameraHeightRatio_19(float value)
	{
		___cameraHeightRatio_19 = value;
	}

	inline static int32_t get_offset_of_capsuleHeightStanding_20() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___capsuleHeightStanding_20)); }
	inline float get_capsuleHeightStanding_20() const { return ___capsuleHeightStanding_20; }
	inline float* get_address_of_capsuleHeightStanding_20() { return &___capsuleHeightStanding_20; }
	inline void set_capsuleHeightStanding_20(float value)
	{
		___capsuleHeightStanding_20 = value;
	}

	inline static int32_t get_offset_of_capsuleHeightCrouching_21() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___capsuleHeightCrouching_21)); }
	inline float get_capsuleHeightCrouching_21() const { return ___capsuleHeightCrouching_21; }
	inline float* get_address_of_capsuleHeightCrouching_21() { return &___capsuleHeightCrouching_21; }
	inline void set_capsuleHeightCrouching_21(float value)
	{
		___capsuleHeightCrouching_21 = value;
	}

	inline static int32_t get_offset_of_crouchingSharpness_22() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___crouchingSharpness_22)); }
	inline float get_crouchingSharpness_22() const { return ___crouchingSharpness_22; }
	inline float* get_address_of_crouchingSharpness_22() { return &___crouchingSharpness_22; }
	inline void set_crouchingSharpness_22(float value)
	{
		___crouchingSharpness_22 = value;
	}

	inline static int32_t get_offset_of_footstepSFXFrequency_23() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___footstepSFXFrequency_23)); }
	inline float get_footstepSFXFrequency_23() const { return ___footstepSFXFrequency_23; }
	inline float* get_address_of_footstepSFXFrequency_23() { return &___footstepSFXFrequency_23; }
	inline void set_footstepSFXFrequency_23(float value)
	{
		___footstepSFXFrequency_23 = value;
	}

	inline static int32_t get_offset_of_footstepSFXFrequencyWhileSprinting_24() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___footstepSFXFrequencyWhileSprinting_24)); }
	inline float get_footstepSFXFrequencyWhileSprinting_24() const { return ___footstepSFXFrequencyWhileSprinting_24; }
	inline float* get_address_of_footstepSFXFrequencyWhileSprinting_24() { return &___footstepSFXFrequencyWhileSprinting_24; }
	inline void set_footstepSFXFrequencyWhileSprinting_24(float value)
	{
		___footstepSFXFrequencyWhileSprinting_24 = value;
	}

	inline static int32_t get_offset_of_footstepSFX_25() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___footstepSFX_25)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_footstepSFX_25() const { return ___footstepSFX_25; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_footstepSFX_25() { return &___footstepSFX_25; }
	inline void set_footstepSFX_25(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___footstepSFX_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___footstepSFX_25), (void*)value);
	}

	inline static int32_t get_offset_of_jumpSFX_26() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___jumpSFX_26)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_jumpSFX_26() const { return ___jumpSFX_26; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_jumpSFX_26() { return &___jumpSFX_26; }
	inline void set_jumpSFX_26(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___jumpSFX_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jumpSFX_26), (void*)value);
	}

	inline static int32_t get_offset_of_landSFX_27() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___landSFX_27)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_landSFX_27() const { return ___landSFX_27; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_landSFX_27() { return &___landSFX_27; }
	inline void set_landSFX_27(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___landSFX_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___landSFX_27), (void*)value);
	}

	inline static int32_t get_offset_of_fallDamageSFX_28() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___fallDamageSFX_28)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_fallDamageSFX_28() const { return ___fallDamageSFX_28; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_fallDamageSFX_28() { return &___fallDamageSFX_28; }
	inline void set_fallDamageSFX_28(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___fallDamageSFX_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fallDamageSFX_28), (void*)value);
	}

	inline static int32_t get_offset_of_recievesFallDamage_29() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___recievesFallDamage_29)); }
	inline bool get_recievesFallDamage_29() const { return ___recievesFallDamage_29; }
	inline bool* get_address_of_recievesFallDamage_29() { return &___recievesFallDamage_29; }
	inline void set_recievesFallDamage_29(bool value)
	{
		___recievesFallDamage_29 = value;
	}

	inline static int32_t get_offset_of_minSpeedForFallDamage_30() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___minSpeedForFallDamage_30)); }
	inline float get_minSpeedForFallDamage_30() const { return ___minSpeedForFallDamage_30; }
	inline float* get_address_of_minSpeedForFallDamage_30() { return &___minSpeedForFallDamage_30; }
	inline void set_minSpeedForFallDamage_30(float value)
	{
		___minSpeedForFallDamage_30 = value;
	}

	inline static int32_t get_offset_of_maxSpeedForFallDamage_31() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___maxSpeedForFallDamage_31)); }
	inline float get_maxSpeedForFallDamage_31() const { return ___maxSpeedForFallDamage_31; }
	inline float* get_address_of_maxSpeedForFallDamage_31() { return &___maxSpeedForFallDamage_31; }
	inline void set_maxSpeedForFallDamage_31(float value)
	{
		___maxSpeedForFallDamage_31 = value;
	}

	inline static int32_t get_offset_of_fallDamageAtMinSpeed_32() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___fallDamageAtMinSpeed_32)); }
	inline float get_fallDamageAtMinSpeed_32() const { return ___fallDamageAtMinSpeed_32; }
	inline float* get_address_of_fallDamageAtMinSpeed_32() { return &___fallDamageAtMinSpeed_32; }
	inline void set_fallDamageAtMinSpeed_32(float value)
	{
		___fallDamageAtMinSpeed_32 = value;
	}

	inline static int32_t get_offset_of_fallDamageAtMaxSpeed_33() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___fallDamageAtMaxSpeed_33)); }
	inline float get_fallDamageAtMaxSpeed_33() const { return ___fallDamageAtMaxSpeed_33; }
	inline float* get_address_of_fallDamageAtMaxSpeed_33() { return &___fallDamageAtMaxSpeed_33; }
	inline void set_fallDamageAtMaxSpeed_33(float value)
	{
		___fallDamageAtMaxSpeed_33 = value;
	}

	inline static int32_t get_offset_of_onStanceChanged_34() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___onStanceChanged_34)); }
	inline UnityAction_1_tB994D127B02789CE2010397AEF756615E5F84FDC * get_onStanceChanged_34() const { return ___onStanceChanged_34; }
	inline UnityAction_1_tB994D127B02789CE2010397AEF756615E5F84FDC ** get_address_of_onStanceChanged_34() { return &___onStanceChanged_34; }
	inline void set_onStanceChanged_34(UnityAction_1_tB994D127B02789CE2010397AEF756615E5F84FDC * value)
	{
		___onStanceChanged_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStanceChanged_34), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcharacterVelocityU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___U3CcharacterVelocityU3Ek__BackingField_35)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CcharacterVelocityU3Ek__BackingField_35() const { return ___U3CcharacterVelocityU3Ek__BackingField_35; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CcharacterVelocityU3Ek__BackingField_35() { return &___U3CcharacterVelocityU3Ek__BackingField_35; }
	inline void set_U3CcharacterVelocityU3Ek__BackingField_35(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CcharacterVelocityU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CisGroundedU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___U3CisGroundedU3Ek__BackingField_36)); }
	inline bool get_U3CisGroundedU3Ek__BackingField_36() const { return ___U3CisGroundedU3Ek__BackingField_36; }
	inline bool* get_address_of_U3CisGroundedU3Ek__BackingField_36() { return &___U3CisGroundedU3Ek__BackingField_36; }
	inline void set_U3CisGroundedU3Ek__BackingField_36(bool value)
	{
		___U3CisGroundedU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of_U3ChasJumpedThisFrameU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___U3ChasJumpedThisFrameU3Ek__BackingField_37)); }
	inline bool get_U3ChasJumpedThisFrameU3Ek__BackingField_37() const { return ___U3ChasJumpedThisFrameU3Ek__BackingField_37; }
	inline bool* get_address_of_U3ChasJumpedThisFrameU3Ek__BackingField_37() { return &___U3ChasJumpedThisFrameU3Ek__BackingField_37; }
	inline void set_U3ChasJumpedThisFrameU3Ek__BackingField_37(bool value)
	{
		___U3ChasJumpedThisFrameU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CisDeadU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___U3CisDeadU3Ek__BackingField_38)); }
	inline bool get_U3CisDeadU3Ek__BackingField_38() const { return ___U3CisDeadU3Ek__BackingField_38; }
	inline bool* get_address_of_U3CisDeadU3Ek__BackingField_38() { return &___U3CisDeadU3Ek__BackingField_38; }
	inline void set_U3CisDeadU3Ek__BackingField_38(bool value)
	{
		___U3CisDeadU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CisCrouchingU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___U3CisCrouchingU3Ek__BackingField_39)); }
	inline bool get_U3CisCrouchingU3Ek__BackingField_39() const { return ___U3CisCrouchingU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CisCrouchingU3Ek__BackingField_39() { return &___U3CisCrouchingU3Ek__BackingField_39; }
	inline void set_U3CisCrouchingU3Ek__BackingField_39(bool value)
	{
		___U3CisCrouchingU3Ek__BackingField_39 = value;
	}

	inline static int32_t get_offset_of_m_Health_40() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_Health_40)); }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * get_m_Health_40() const { return ___m_Health_40; }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D ** get_address_of_m_Health_40() { return &___m_Health_40; }
	inline void set_m_Health_40(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * value)
	{
		___m_Health_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Health_40), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputHandler_41() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_InputHandler_41)); }
	inline PlayerInputHandler_t2DF05A496167FB336DF5A1FD5D389972925D53C6 * get_m_InputHandler_41() const { return ___m_InputHandler_41; }
	inline PlayerInputHandler_t2DF05A496167FB336DF5A1FD5D389972925D53C6 ** get_address_of_m_InputHandler_41() { return &___m_InputHandler_41; }
	inline void set_m_InputHandler_41(PlayerInputHandler_t2DF05A496167FB336DF5A1FD5D389972925D53C6 * value)
	{
		___m_InputHandler_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputHandler_41), (void*)value);
	}

	inline static int32_t get_offset_of_m_Controller_42() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_Controller_42)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_m_Controller_42() const { return ___m_Controller_42; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_m_Controller_42() { return &___m_Controller_42; }
	inline void set_m_Controller_42(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___m_Controller_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Controller_42), (void*)value);
	}

	inline static int32_t get_offset_of_m_WeaponsManager_43() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_WeaponsManager_43)); }
	inline PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * get_m_WeaponsManager_43() const { return ___m_WeaponsManager_43; }
	inline PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB ** get_address_of_m_WeaponsManager_43() { return &___m_WeaponsManager_43; }
	inline void set_m_WeaponsManager_43(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * value)
	{
		___m_WeaponsManager_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WeaponsManager_43), (void*)value);
	}

	inline static int32_t get_offset_of_m_Actor_44() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_Actor_44)); }
	inline Actor_t7437E2FC69C71194ECD15A5DFE15FDA0FCDFF159 * get_m_Actor_44() const { return ___m_Actor_44; }
	inline Actor_t7437E2FC69C71194ECD15A5DFE15FDA0FCDFF159 ** get_address_of_m_Actor_44() { return &___m_Actor_44; }
	inline void set_m_Actor_44(Actor_t7437E2FC69C71194ECD15A5DFE15FDA0FCDFF159 * value)
	{
		___m_Actor_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Actor_44), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroundNormal_45() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_GroundNormal_45)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_GroundNormal_45() const { return ___m_GroundNormal_45; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_GroundNormal_45() { return &___m_GroundNormal_45; }
	inline void set_m_GroundNormal_45(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_GroundNormal_45 = value;
	}

	inline static int32_t get_offset_of_m_CharacterVelocity_46() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_CharacterVelocity_46)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_CharacterVelocity_46() const { return ___m_CharacterVelocity_46; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_CharacterVelocity_46() { return &___m_CharacterVelocity_46; }
	inline void set_m_CharacterVelocity_46(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_CharacterVelocity_46 = value;
	}

	inline static int32_t get_offset_of_m_LatestImpactSpeed_47() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_LatestImpactSpeed_47)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_LatestImpactSpeed_47() const { return ___m_LatestImpactSpeed_47; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_LatestImpactSpeed_47() { return &___m_LatestImpactSpeed_47; }
	inline void set_m_LatestImpactSpeed_47(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_LatestImpactSpeed_47 = value;
	}

	inline static int32_t get_offset_of_m_LastTimeJumped_48() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_LastTimeJumped_48)); }
	inline float get_m_LastTimeJumped_48() const { return ___m_LastTimeJumped_48; }
	inline float* get_address_of_m_LastTimeJumped_48() { return &___m_LastTimeJumped_48; }
	inline void set_m_LastTimeJumped_48(float value)
	{
		___m_LastTimeJumped_48 = value;
	}

	inline static int32_t get_offset_of_m_CameraVerticalAngle_49() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_CameraVerticalAngle_49)); }
	inline float get_m_CameraVerticalAngle_49() const { return ___m_CameraVerticalAngle_49; }
	inline float* get_address_of_m_CameraVerticalAngle_49() { return &___m_CameraVerticalAngle_49; }
	inline void set_m_CameraVerticalAngle_49(float value)
	{
		___m_CameraVerticalAngle_49 = value;
	}

	inline static int32_t get_offset_of_m_footstepDistanceCounter_50() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_footstepDistanceCounter_50)); }
	inline float get_m_footstepDistanceCounter_50() const { return ___m_footstepDistanceCounter_50; }
	inline float* get_address_of_m_footstepDistanceCounter_50() { return &___m_footstepDistanceCounter_50; }
	inline void set_m_footstepDistanceCounter_50(float value)
	{
		___m_footstepDistanceCounter_50 = value;
	}

	inline static int32_t get_offset_of_m_TargetCharacterHeight_51() { return static_cast<int32_t>(offsetof(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072, ___m_TargetCharacterHeight_51)); }
	inline float get_m_TargetCharacterHeight_51() const { return ___m_TargetCharacterHeight_51; }
	inline float* get_address_of_m_TargetCharacterHeight_51() { return &___m_TargetCharacterHeight_51; }
	inline void set_m_TargetCharacterHeight_51(float value)
	{
		___m_TargetCharacterHeight_51 = value;
	}
};


// PlayerWeaponsManager
struct  PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<WeaponController> PlayerWeaponsManager::startingWeapons
	List_1_t1E51F67C63BDBE2C7EC22ED991734600EBB3FDA1 * ___startingWeapons_4;
	// UnityEngine.Camera PlayerWeaponsManager::weaponCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___weaponCamera_5;
	// UnityEngine.Transform PlayerWeaponsManager::weaponParentSocket
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___weaponParentSocket_6;
	// UnityEngine.Transform PlayerWeaponsManager::defaultWeaponPosition
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___defaultWeaponPosition_7;
	// UnityEngine.Transform PlayerWeaponsManager::aimingWeaponPosition
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___aimingWeaponPosition_8;
	// UnityEngine.Transform PlayerWeaponsManager::downWeaponPosition
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___downWeaponPosition_9;
	// System.Single PlayerWeaponsManager::bobFrequency
	float ___bobFrequency_10;
	// System.Single PlayerWeaponsManager::bobSharpness
	float ___bobSharpness_11;
	// System.Single PlayerWeaponsManager::defaultBobAmount
	float ___defaultBobAmount_12;
	// System.Single PlayerWeaponsManager::aimingBobAmount
	float ___aimingBobAmount_13;
	// System.Single PlayerWeaponsManager::recoilSharpness
	float ___recoilSharpness_14;
	// System.Single PlayerWeaponsManager::maxRecoilDistance
	float ___maxRecoilDistance_15;
	// System.Single PlayerWeaponsManager::recoilRestitutionSharpness
	float ___recoilRestitutionSharpness_16;
	// System.Single PlayerWeaponsManager::aimingAnimationSpeed
	float ___aimingAnimationSpeed_17;
	// System.Single PlayerWeaponsManager::defaultFOV
	float ___defaultFOV_18;
	// System.Single PlayerWeaponsManager::weaponFOVMultiplier
	float ___weaponFOVMultiplier_19;
	// System.Single PlayerWeaponsManager::weaponSwitchDelay
	float ___weaponSwitchDelay_20;
	// UnityEngine.LayerMask PlayerWeaponsManager::FPSWeaponLayer
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___FPSWeaponLayer_21;
	// System.Boolean PlayerWeaponsManager::<isAiming>k__BackingField
	bool ___U3CisAimingU3Ek__BackingField_22;
	// System.Boolean PlayerWeaponsManager::<isPointingAtEnemy>k__BackingField
	bool ___U3CisPointingAtEnemyU3Ek__BackingField_23;
	// System.Int32 PlayerWeaponsManager::<activeWeaponIndex>k__BackingField
	int32_t ___U3CactiveWeaponIndexU3Ek__BackingField_24;
	// UnityEngine.Events.UnityAction`1<WeaponController> PlayerWeaponsManager::onSwitchedToWeapon
	UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F * ___onSwitchedToWeapon_25;
	// UnityEngine.Events.UnityAction`2<WeaponController,System.Int32> PlayerWeaponsManager::onAddedWeapon
	UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * ___onAddedWeapon_26;
	// UnityEngine.Events.UnityAction`2<WeaponController,System.Int32> PlayerWeaponsManager::onRemovedWeapon
	UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * ___onRemovedWeapon_27;
	// WeaponController[] PlayerWeaponsManager::m_WeaponSlots
	WeaponControllerU5BU5D_t829FA4C36C3C1FC8624A0CCFC79C0FB92EFA6B54* ___m_WeaponSlots_28;
	// PlayerInputHandler PlayerWeaponsManager::m_InputHandler
	PlayerInputHandler_t2DF05A496167FB336DF5A1FD5D389972925D53C6 * ___m_InputHandler_29;
	// PlayerCharacterController PlayerWeaponsManager::m_PlayerCharacterController
	PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072 * ___m_PlayerCharacterController_30;
	// System.Single PlayerWeaponsManager::m_WeaponBobFactor
	float ___m_WeaponBobFactor_31;
	// UnityEngine.Vector3 PlayerWeaponsManager::m_LastCharacterPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_LastCharacterPosition_32;
	// UnityEngine.Vector3 PlayerWeaponsManager::m_WeaponMainLocalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_WeaponMainLocalPosition_33;
	// UnityEngine.Vector3 PlayerWeaponsManager::m_WeaponBobLocalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_WeaponBobLocalPosition_34;
	// UnityEngine.Vector3 PlayerWeaponsManager::m_WeaponRecoilLocalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_WeaponRecoilLocalPosition_35;
	// UnityEngine.Vector3 PlayerWeaponsManager::m_AccumulatedRecoil
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_AccumulatedRecoil_36;
	// System.Single PlayerWeaponsManager::m_TimeStartedWeaponSwitch
	float ___m_TimeStartedWeaponSwitch_37;
	// PlayerWeaponsManager_WeaponSwitchState PlayerWeaponsManager::m_WeaponSwitchState
	int32_t ___m_WeaponSwitchState_38;
	// System.Int32 PlayerWeaponsManager::m_WeaponSwitchNewWeaponIndex
	int32_t ___m_WeaponSwitchNewWeaponIndex_39;

public:
	inline static int32_t get_offset_of_startingWeapons_4() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___startingWeapons_4)); }
	inline List_1_t1E51F67C63BDBE2C7EC22ED991734600EBB3FDA1 * get_startingWeapons_4() const { return ___startingWeapons_4; }
	inline List_1_t1E51F67C63BDBE2C7EC22ED991734600EBB3FDA1 ** get_address_of_startingWeapons_4() { return &___startingWeapons_4; }
	inline void set_startingWeapons_4(List_1_t1E51F67C63BDBE2C7EC22ED991734600EBB3FDA1 * value)
	{
		___startingWeapons_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startingWeapons_4), (void*)value);
	}

	inline static int32_t get_offset_of_weaponCamera_5() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___weaponCamera_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_weaponCamera_5() const { return ___weaponCamera_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_weaponCamera_5() { return &___weaponCamera_5; }
	inline void set_weaponCamera_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___weaponCamera_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weaponCamera_5), (void*)value);
	}

	inline static int32_t get_offset_of_weaponParentSocket_6() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___weaponParentSocket_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_weaponParentSocket_6() const { return ___weaponParentSocket_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_weaponParentSocket_6() { return &___weaponParentSocket_6; }
	inline void set_weaponParentSocket_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___weaponParentSocket_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weaponParentSocket_6), (void*)value);
	}

	inline static int32_t get_offset_of_defaultWeaponPosition_7() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___defaultWeaponPosition_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_defaultWeaponPosition_7() const { return ___defaultWeaponPosition_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_defaultWeaponPosition_7() { return &___defaultWeaponPosition_7; }
	inline void set_defaultWeaponPosition_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___defaultWeaponPosition_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultWeaponPosition_7), (void*)value);
	}

	inline static int32_t get_offset_of_aimingWeaponPosition_8() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___aimingWeaponPosition_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_aimingWeaponPosition_8() const { return ___aimingWeaponPosition_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_aimingWeaponPosition_8() { return &___aimingWeaponPosition_8; }
	inline void set_aimingWeaponPosition_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___aimingWeaponPosition_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aimingWeaponPosition_8), (void*)value);
	}

	inline static int32_t get_offset_of_downWeaponPosition_9() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___downWeaponPosition_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_downWeaponPosition_9() const { return ___downWeaponPosition_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_downWeaponPosition_9() { return &___downWeaponPosition_9; }
	inline void set_downWeaponPosition_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___downWeaponPosition_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___downWeaponPosition_9), (void*)value);
	}

	inline static int32_t get_offset_of_bobFrequency_10() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___bobFrequency_10)); }
	inline float get_bobFrequency_10() const { return ___bobFrequency_10; }
	inline float* get_address_of_bobFrequency_10() { return &___bobFrequency_10; }
	inline void set_bobFrequency_10(float value)
	{
		___bobFrequency_10 = value;
	}

	inline static int32_t get_offset_of_bobSharpness_11() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___bobSharpness_11)); }
	inline float get_bobSharpness_11() const { return ___bobSharpness_11; }
	inline float* get_address_of_bobSharpness_11() { return &___bobSharpness_11; }
	inline void set_bobSharpness_11(float value)
	{
		___bobSharpness_11 = value;
	}

	inline static int32_t get_offset_of_defaultBobAmount_12() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___defaultBobAmount_12)); }
	inline float get_defaultBobAmount_12() const { return ___defaultBobAmount_12; }
	inline float* get_address_of_defaultBobAmount_12() { return &___defaultBobAmount_12; }
	inline void set_defaultBobAmount_12(float value)
	{
		___defaultBobAmount_12 = value;
	}

	inline static int32_t get_offset_of_aimingBobAmount_13() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___aimingBobAmount_13)); }
	inline float get_aimingBobAmount_13() const { return ___aimingBobAmount_13; }
	inline float* get_address_of_aimingBobAmount_13() { return &___aimingBobAmount_13; }
	inline void set_aimingBobAmount_13(float value)
	{
		___aimingBobAmount_13 = value;
	}

	inline static int32_t get_offset_of_recoilSharpness_14() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___recoilSharpness_14)); }
	inline float get_recoilSharpness_14() const { return ___recoilSharpness_14; }
	inline float* get_address_of_recoilSharpness_14() { return &___recoilSharpness_14; }
	inline void set_recoilSharpness_14(float value)
	{
		___recoilSharpness_14 = value;
	}

	inline static int32_t get_offset_of_maxRecoilDistance_15() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___maxRecoilDistance_15)); }
	inline float get_maxRecoilDistance_15() const { return ___maxRecoilDistance_15; }
	inline float* get_address_of_maxRecoilDistance_15() { return &___maxRecoilDistance_15; }
	inline void set_maxRecoilDistance_15(float value)
	{
		___maxRecoilDistance_15 = value;
	}

	inline static int32_t get_offset_of_recoilRestitutionSharpness_16() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___recoilRestitutionSharpness_16)); }
	inline float get_recoilRestitutionSharpness_16() const { return ___recoilRestitutionSharpness_16; }
	inline float* get_address_of_recoilRestitutionSharpness_16() { return &___recoilRestitutionSharpness_16; }
	inline void set_recoilRestitutionSharpness_16(float value)
	{
		___recoilRestitutionSharpness_16 = value;
	}

	inline static int32_t get_offset_of_aimingAnimationSpeed_17() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___aimingAnimationSpeed_17)); }
	inline float get_aimingAnimationSpeed_17() const { return ___aimingAnimationSpeed_17; }
	inline float* get_address_of_aimingAnimationSpeed_17() { return &___aimingAnimationSpeed_17; }
	inline void set_aimingAnimationSpeed_17(float value)
	{
		___aimingAnimationSpeed_17 = value;
	}

	inline static int32_t get_offset_of_defaultFOV_18() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___defaultFOV_18)); }
	inline float get_defaultFOV_18() const { return ___defaultFOV_18; }
	inline float* get_address_of_defaultFOV_18() { return &___defaultFOV_18; }
	inline void set_defaultFOV_18(float value)
	{
		___defaultFOV_18 = value;
	}

	inline static int32_t get_offset_of_weaponFOVMultiplier_19() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___weaponFOVMultiplier_19)); }
	inline float get_weaponFOVMultiplier_19() const { return ___weaponFOVMultiplier_19; }
	inline float* get_address_of_weaponFOVMultiplier_19() { return &___weaponFOVMultiplier_19; }
	inline void set_weaponFOVMultiplier_19(float value)
	{
		___weaponFOVMultiplier_19 = value;
	}

	inline static int32_t get_offset_of_weaponSwitchDelay_20() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___weaponSwitchDelay_20)); }
	inline float get_weaponSwitchDelay_20() const { return ___weaponSwitchDelay_20; }
	inline float* get_address_of_weaponSwitchDelay_20() { return &___weaponSwitchDelay_20; }
	inline void set_weaponSwitchDelay_20(float value)
	{
		___weaponSwitchDelay_20 = value;
	}

	inline static int32_t get_offset_of_FPSWeaponLayer_21() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___FPSWeaponLayer_21)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_FPSWeaponLayer_21() const { return ___FPSWeaponLayer_21; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_FPSWeaponLayer_21() { return &___FPSWeaponLayer_21; }
	inline void set_FPSWeaponLayer_21(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___FPSWeaponLayer_21 = value;
	}

	inline static int32_t get_offset_of_U3CisAimingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___U3CisAimingU3Ek__BackingField_22)); }
	inline bool get_U3CisAimingU3Ek__BackingField_22() const { return ___U3CisAimingU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CisAimingU3Ek__BackingField_22() { return &___U3CisAimingU3Ek__BackingField_22; }
	inline void set_U3CisAimingU3Ek__BackingField_22(bool value)
	{
		___U3CisAimingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CisPointingAtEnemyU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___U3CisPointingAtEnemyU3Ek__BackingField_23)); }
	inline bool get_U3CisPointingAtEnemyU3Ek__BackingField_23() const { return ___U3CisPointingAtEnemyU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CisPointingAtEnemyU3Ek__BackingField_23() { return &___U3CisPointingAtEnemyU3Ek__BackingField_23; }
	inline void set_U3CisPointingAtEnemyU3Ek__BackingField_23(bool value)
	{
		___U3CisPointingAtEnemyU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CactiveWeaponIndexU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___U3CactiveWeaponIndexU3Ek__BackingField_24)); }
	inline int32_t get_U3CactiveWeaponIndexU3Ek__BackingField_24() const { return ___U3CactiveWeaponIndexU3Ek__BackingField_24; }
	inline int32_t* get_address_of_U3CactiveWeaponIndexU3Ek__BackingField_24() { return &___U3CactiveWeaponIndexU3Ek__BackingField_24; }
	inline void set_U3CactiveWeaponIndexU3Ek__BackingField_24(int32_t value)
	{
		___U3CactiveWeaponIndexU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_onSwitchedToWeapon_25() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___onSwitchedToWeapon_25)); }
	inline UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F * get_onSwitchedToWeapon_25() const { return ___onSwitchedToWeapon_25; }
	inline UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F ** get_address_of_onSwitchedToWeapon_25() { return &___onSwitchedToWeapon_25; }
	inline void set_onSwitchedToWeapon_25(UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F * value)
	{
		___onSwitchedToWeapon_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onSwitchedToWeapon_25), (void*)value);
	}

	inline static int32_t get_offset_of_onAddedWeapon_26() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___onAddedWeapon_26)); }
	inline UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * get_onAddedWeapon_26() const { return ___onAddedWeapon_26; }
	inline UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C ** get_address_of_onAddedWeapon_26() { return &___onAddedWeapon_26; }
	inline void set_onAddedWeapon_26(UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * value)
	{
		___onAddedWeapon_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onAddedWeapon_26), (void*)value);
	}

	inline static int32_t get_offset_of_onRemovedWeapon_27() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___onRemovedWeapon_27)); }
	inline UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * get_onRemovedWeapon_27() const { return ___onRemovedWeapon_27; }
	inline UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C ** get_address_of_onRemovedWeapon_27() { return &___onRemovedWeapon_27; }
	inline void set_onRemovedWeapon_27(UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * value)
	{
		___onRemovedWeapon_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRemovedWeapon_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_WeaponSlots_28() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_WeaponSlots_28)); }
	inline WeaponControllerU5BU5D_t829FA4C36C3C1FC8624A0CCFC79C0FB92EFA6B54* get_m_WeaponSlots_28() const { return ___m_WeaponSlots_28; }
	inline WeaponControllerU5BU5D_t829FA4C36C3C1FC8624A0CCFC79C0FB92EFA6B54** get_address_of_m_WeaponSlots_28() { return &___m_WeaponSlots_28; }
	inline void set_m_WeaponSlots_28(WeaponControllerU5BU5D_t829FA4C36C3C1FC8624A0CCFC79C0FB92EFA6B54* value)
	{
		___m_WeaponSlots_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WeaponSlots_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputHandler_29() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_InputHandler_29)); }
	inline PlayerInputHandler_t2DF05A496167FB336DF5A1FD5D389972925D53C6 * get_m_InputHandler_29() const { return ___m_InputHandler_29; }
	inline PlayerInputHandler_t2DF05A496167FB336DF5A1FD5D389972925D53C6 ** get_address_of_m_InputHandler_29() { return &___m_InputHandler_29; }
	inline void set_m_InputHandler_29(PlayerInputHandler_t2DF05A496167FB336DF5A1FD5D389972925D53C6 * value)
	{
		___m_InputHandler_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputHandler_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerCharacterController_30() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_PlayerCharacterController_30)); }
	inline PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072 * get_m_PlayerCharacterController_30() const { return ___m_PlayerCharacterController_30; }
	inline PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072 ** get_address_of_m_PlayerCharacterController_30() { return &___m_PlayerCharacterController_30; }
	inline void set_m_PlayerCharacterController_30(PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072 * value)
	{
		___m_PlayerCharacterController_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerCharacterController_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_WeaponBobFactor_31() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_WeaponBobFactor_31)); }
	inline float get_m_WeaponBobFactor_31() const { return ___m_WeaponBobFactor_31; }
	inline float* get_address_of_m_WeaponBobFactor_31() { return &___m_WeaponBobFactor_31; }
	inline void set_m_WeaponBobFactor_31(float value)
	{
		___m_WeaponBobFactor_31 = value;
	}

	inline static int32_t get_offset_of_m_LastCharacterPosition_32() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_LastCharacterPosition_32)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_LastCharacterPosition_32() const { return ___m_LastCharacterPosition_32; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_LastCharacterPosition_32() { return &___m_LastCharacterPosition_32; }
	inline void set_m_LastCharacterPosition_32(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_LastCharacterPosition_32 = value;
	}

	inline static int32_t get_offset_of_m_WeaponMainLocalPosition_33() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_WeaponMainLocalPosition_33)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_WeaponMainLocalPosition_33() const { return ___m_WeaponMainLocalPosition_33; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_WeaponMainLocalPosition_33() { return &___m_WeaponMainLocalPosition_33; }
	inline void set_m_WeaponMainLocalPosition_33(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_WeaponMainLocalPosition_33 = value;
	}

	inline static int32_t get_offset_of_m_WeaponBobLocalPosition_34() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_WeaponBobLocalPosition_34)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_WeaponBobLocalPosition_34() const { return ___m_WeaponBobLocalPosition_34; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_WeaponBobLocalPosition_34() { return &___m_WeaponBobLocalPosition_34; }
	inline void set_m_WeaponBobLocalPosition_34(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_WeaponBobLocalPosition_34 = value;
	}

	inline static int32_t get_offset_of_m_WeaponRecoilLocalPosition_35() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_WeaponRecoilLocalPosition_35)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_WeaponRecoilLocalPosition_35() const { return ___m_WeaponRecoilLocalPosition_35; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_WeaponRecoilLocalPosition_35() { return &___m_WeaponRecoilLocalPosition_35; }
	inline void set_m_WeaponRecoilLocalPosition_35(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_WeaponRecoilLocalPosition_35 = value;
	}

	inline static int32_t get_offset_of_m_AccumulatedRecoil_36() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_AccumulatedRecoil_36)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_AccumulatedRecoil_36() const { return ___m_AccumulatedRecoil_36; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_AccumulatedRecoil_36() { return &___m_AccumulatedRecoil_36; }
	inline void set_m_AccumulatedRecoil_36(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_AccumulatedRecoil_36 = value;
	}

	inline static int32_t get_offset_of_m_TimeStartedWeaponSwitch_37() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_TimeStartedWeaponSwitch_37)); }
	inline float get_m_TimeStartedWeaponSwitch_37() const { return ___m_TimeStartedWeaponSwitch_37; }
	inline float* get_address_of_m_TimeStartedWeaponSwitch_37() { return &___m_TimeStartedWeaponSwitch_37; }
	inline void set_m_TimeStartedWeaponSwitch_37(float value)
	{
		___m_TimeStartedWeaponSwitch_37 = value;
	}

	inline static int32_t get_offset_of_m_WeaponSwitchState_38() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_WeaponSwitchState_38)); }
	inline int32_t get_m_WeaponSwitchState_38() const { return ___m_WeaponSwitchState_38; }
	inline int32_t* get_address_of_m_WeaponSwitchState_38() { return &___m_WeaponSwitchState_38; }
	inline void set_m_WeaponSwitchState_38(int32_t value)
	{
		___m_WeaponSwitchState_38 = value;
	}

	inline static int32_t get_offset_of_m_WeaponSwitchNewWeaponIndex_39() { return static_cast<int32_t>(offsetof(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB, ___m_WeaponSwitchNewWeaponIndex_39)); }
	inline int32_t get_m_WeaponSwitchNewWeaponIndex_39() const { return ___m_WeaponSwitchNewWeaponIndex_39; }
	inline int32_t* get_address_of_m_WeaponSwitchNewWeaponIndex_39() { return &___m_WeaponSwitchNewWeaponIndex_39; }
	inline void set_m_WeaponSwitchNewWeaponIndex_39(int32_t value)
	{
		___m_WeaponSwitchNewWeaponIndex_39 = value;
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// WeaponController
struct  WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String WeaponController::weaponName
	String_t* ___weaponName_4;
	// UnityEngine.Sprite WeaponController::weaponIcon
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___weaponIcon_5;
	// CrosshairData WeaponController::crosshairDataDefault
	CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC  ___crosshairDataDefault_6;
	// CrosshairData WeaponController::crosshairDataTargetInSight
	CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC  ___crosshairDataTargetInSight_7;
	// UnityEngine.GameObject WeaponController::weaponRoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___weaponRoot_8;
	// UnityEngine.Transform WeaponController::weaponMuzzle
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___weaponMuzzle_9;
	// WeaponShootType WeaponController::shootType
	int32_t ___shootType_10;
	// ProjectileBase WeaponController::projectilePrefab
	ProjectileBase_t0F3DE2AEFB78F25A28CCABF5901555BB8A4AAF6E * ___projectilePrefab_11;
	// System.Single WeaponController::delayBetweenShots
	float ___delayBetweenShots_12;
	// System.Single WeaponController::bulletSpreadAngle
	float ___bulletSpreadAngle_13;
	// System.Int32 WeaponController::bulletsPerShot
	int32_t ___bulletsPerShot_14;
	// System.Single WeaponController::recoilForce
	float ___recoilForce_15;
	// System.Single WeaponController::aimZoomRatio
	float ___aimZoomRatio_16;
	// UnityEngine.Vector3 WeaponController::aimOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___aimOffset_17;
	// System.Single WeaponController::ammoReloadRate
	float ___ammoReloadRate_18;
	// System.Single WeaponController::ammoReloadDelay
	float ___ammoReloadDelay_19;
	// System.Single WeaponController::maxAmmo
	float ___maxAmmo_20;
	// System.Boolean WeaponController::automaticReleaseOnCharged
	bool ___automaticReleaseOnCharged_21;
	// System.Single WeaponController::maxChargeDuration
	float ___maxChargeDuration_22;
	// System.Single WeaponController::ammoUsedOnStartCharge
	float ___ammoUsedOnStartCharge_23;
	// System.Single WeaponController::ammoUsageRateWhileCharging
	float ___ammoUsageRateWhileCharging_24;
	// UnityEngine.Animator WeaponController::weaponAnimator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___weaponAnimator_25;
	// UnityEngine.GameObject WeaponController::muzzleFlashPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___muzzleFlashPrefab_26;
	// System.Boolean WeaponController::unparentMuzzleFlash
	bool ___unparentMuzzleFlash_27;
	// UnityEngine.AudioClip WeaponController::shootSFX
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___shootSFX_28;
	// UnityEngine.AudioClip WeaponController::changeWeaponSFX
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___changeWeaponSFX_29;
	// System.Boolean WeaponController::useContinuousShootSound
	bool ___useContinuousShootSound_30;
	// UnityEngine.AudioClip WeaponController::continuousShootStartSFX
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___continuousShootStartSFX_31;
	// UnityEngine.AudioClip WeaponController::continuousShootLoopSFX
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___continuousShootLoopSFX_32;
	// UnityEngine.AudioClip WeaponController::continuousShootEndSFX
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___continuousShootEndSFX_33;
	// UnityEngine.AudioSource WeaponController::m_continuousShootAudioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___m_continuousShootAudioSource_34;
	// System.Boolean WeaponController::m_wantsToShoot
	bool ___m_wantsToShoot_35;
	// UnityEngine.Events.UnityAction WeaponController::onShoot
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onShoot_36;
	// System.Single WeaponController::m_CurrentAmmo
	float ___m_CurrentAmmo_37;
	// System.Single WeaponController::m_LastTimeShot
	float ___m_LastTimeShot_38;
	// System.Single WeaponController::<LastChargeTriggerTimestamp>k__BackingField
	float ___U3CLastChargeTriggerTimestampU3Ek__BackingField_39;
	// UnityEngine.Vector3 WeaponController::m_LastMuzzlePosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_LastMuzzlePosition_40;
	// UnityEngine.GameObject WeaponController::<owner>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CownerU3Ek__BackingField_41;
	// UnityEngine.GameObject WeaponController::<sourcePrefab>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CsourcePrefabU3Ek__BackingField_42;
	// System.Boolean WeaponController::<isCharging>k__BackingField
	bool ___U3CisChargingU3Ek__BackingField_43;
	// System.Single WeaponController::<currentAmmoRatio>k__BackingField
	float ___U3CcurrentAmmoRatioU3Ek__BackingField_44;
	// System.Boolean WeaponController::<isWeaponActive>k__BackingField
	bool ___U3CisWeaponActiveU3Ek__BackingField_45;
	// System.Boolean WeaponController::<isCooling>k__BackingField
	bool ___U3CisCoolingU3Ek__BackingField_46;
	// System.Single WeaponController::<currentCharge>k__BackingField
	float ___U3CcurrentChargeU3Ek__BackingField_47;
	// UnityEngine.Vector3 WeaponController::<muzzleWorldVelocity>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CmuzzleWorldVelocityU3Ek__BackingField_48;
	// UnityEngine.AudioSource WeaponController::m_ShootAudioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___m_ShootAudioSource_49;

public:
	inline static int32_t get_offset_of_weaponName_4() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___weaponName_4)); }
	inline String_t* get_weaponName_4() const { return ___weaponName_4; }
	inline String_t** get_address_of_weaponName_4() { return &___weaponName_4; }
	inline void set_weaponName_4(String_t* value)
	{
		___weaponName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weaponName_4), (void*)value);
	}

	inline static int32_t get_offset_of_weaponIcon_5() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___weaponIcon_5)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_weaponIcon_5() const { return ___weaponIcon_5; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_weaponIcon_5() { return &___weaponIcon_5; }
	inline void set_weaponIcon_5(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___weaponIcon_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weaponIcon_5), (void*)value);
	}

	inline static int32_t get_offset_of_crosshairDataDefault_6() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___crosshairDataDefault_6)); }
	inline CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC  get_crosshairDataDefault_6() const { return ___crosshairDataDefault_6; }
	inline CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC * get_address_of_crosshairDataDefault_6() { return &___crosshairDataDefault_6; }
	inline void set_crosshairDataDefault_6(CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC  value)
	{
		___crosshairDataDefault_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___crosshairDataDefault_6))->___crosshairSprite_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_crosshairDataTargetInSight_7() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___crosshairDataTargetInSight_7)); }
	inline CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC  get_crosshairDataTargetInSight_7() const { return ___crosshairDataTargetInSight_7; }
	inline CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC * get_address_of_crosshairDataTargetInSight_7() { return &___crosshairDataTargetInSight_7; }
	inline void set_crosshairDataTargetInSight_7(CrosshairData_t463E84D364DB61733168AD4096B007DDA2C73AFC  value)
	{
		___crosshairDataTargetInSight_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___crosshairDataTargetInSight_7))->___crosshairSprite_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_weaponRoot_8() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___weaponRoot_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_weaponRoot_8() const { return ___weaponRoot_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_weaponRoot_8() { return &___weaponRoot_8; }
	inline void set_weaponRoot_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___weaponRoot_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weaponRoot_8), (void*)value);
	}

	inline static int32_t get_offset_of_weaponMuzzle_9() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___weaponMuzzle_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_weaponMuzzle_9() const { return ___weaponMuzzle_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_weaponMuzzle_9() { return &___weaponMuzzle_9; }
	inline void set_weaponMuzzle_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___weaponMuzzle_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weaponMuzzle_9), (void*)value);
	}

	inline static int32_t get_offset_of_shootType_10() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___shootType_10)); }
	inline int32_t get_shootType_10() const { return ___shootType_10; }
	inline int32_t* get_address_of_shootType_10() { return &___shootType_10; }
	inline void set_shootType_10(int32_t value)
	{
		___shootType_10 = value;
	}

	inline static int32_t get_offset_of_projectilePrefab_11() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___projectilePrefab_11)); }
	inline ProjectileBase_t0F3DE2AEFB78F25A28CCABF5901555BB8A4AAF6E * get_projectilePrefab_11() const { return ___projectilePrefab_11; }
	inline ProjectileBase_t0F3DE2AEFB78F25A28CCABF5901555BB8A4AAF6E ** get_address_of_projectilePrefab_11() { return &___projectilePrefab_11; }
	inline void set_projectilePrefab_11(ProjectileBase_t0F3DE2AEFB78F25A28CCABF5901555BB8A4AAF6E * value)
	{
		___projectilePrefab_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectilePrefab_11), (void*)value);
	}

	inline static int32_t get_offset_of_delayBetweenShots_12() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___delayBetweenShots_12)); }
	inline float get_delayBetweenShots_12() const { return ___delayBetweenShots_12; }
	inline float* get_address_of_delayBetweenShots_12() { return &___delayBetweenShots_12; }
	inline void set_delayBetweenShots_12(float value)
	{
		___delayBetweenShots_12 = value;
	}

	inline static int32_t get_offset_of_bulletSpreadAngle_13() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___bulletSpreadAngle_13)); }
	inline float get_bulletSpreadAngle_13() const { return ___bulletSpreadAngle_13; }
	inline float* get_address_of_bulletSpreadAngle_13() { return &___bulletSpreadAngle_13; }
	inline void set_bulletSpreadAngle_13(float value)
	{
		___bulletSpreadAngle_13 = value;
	}

	inline static int32_t get_offset_of_bulletsPerShot_14() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___bulletsPerShot_14)); }
	inline int32_t get_bulletsPerShot_14() const { return ___bulletsPerShot_14; }
	inline int32_t* get_address_of_bulletsPerShot_14() { return &___bulletsPerShot_14; }
	inline void set_bulletsPerShot_14(int32_t value)
	{
		___bulletsPerShot_14 = value;
	}

	inline static int32_t get_offset_of_recoilForce_15() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___recoilForce_15)); }
	inline float get_recoilForce_15() const { return ___recoilForce_15; }
	inline float* get_address_of_recoilForce_15() { return &___recoilForce_15; }
	inline void set_recoilForce_15(float value)
	{
		___recoilForce_15 = value;
	}

	inline static int32_t get_offset_of_aimZoomRatio_16() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___aimZoomRatio_16)); }
	inline float get_aimZoomRatio_16() const { return ___aimZoomRatio_16; }
	inline float* get_address_of_aimZoomRatio_16() { return &___aimZoomRatio_16; }
	inline void set_aimZoomRatio_16(float value)
	{
		___aimZoomRatio_16 = value;
	}

	inline static int32_t get_offset_of_aimOffset_17() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___aimOffset_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_aimOffset_17() const { return ___aimOffset_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_aimOffset_17() { return &___aimOffset_17; }
	inline void set_aimOffset_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___aimOffset_17 = value;
	}

	inline static int32_t get_offset_of_ammoReloadRate_18() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___ammoReloadRate_18)); }
	inline float get_ammoReloadRate_18() const { return ___ammoReloadRate_18; }
	inline float* get_address_of_ammoReloadRate_18() { return &___ammoReloadRate_18; }
	inline void set_ammoReloadRate_18(float value)
	{
		___ammoReloadRate_18 = value;
	}

	inline static int32_t get_offset_of_ammoReloadDelay_19() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___ammoReloadDelay_19)); }
	inline float get_ammoReloadDelay_19() const { return ___ammoReloadDelay_19; }
	inline float* get_address_of_ammoReloadDelay_19() { return &___ammoReloadDelay_19; }
	inline void set_ammoReloadDelay_19(float value)
	{
		___ammoReloadDelay_19 = value;
	}

	inline static int32_t get_offset_of_maxAmmo_20() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___maxAmmo_20)); }
	inline float get_maxAmmo_20() const { return ___maxAmmo_20; }
	inline float* get_address_of_maxAmmo_20() { return &___maxAmmo_20; }
	inline void set_maxAmmo_20(float value)
	{
		___maxAmmo_20 = value;
	}

	inline static int32_t get_offset_of_automaticReleaseOnCharged_21() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___automaticReleaseOnCharged_21)); }
	inline bool get_automaticReleaseOnCharged_21() const { return ___automaticReleaseOnCharged_21; }
	inline bool* get_address_of_automaticReleaseOnCharged_21() { return &___automaticReleaseOnCharged_21; }
	inline void set_automaticReleaseOnCharged_21(bool value)
	{
		___automaticReleaseOnCharged_21 = value;
	}

	inline static int32_t get_offset_of_maxChargeDuration_22() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___maxChargeDuration_22)); }
	inline float get_maxChargeDuration_22() const { return ___maxChargeDuration_22; }
	inline float* get_address_of_maxChargeDuration_22() { return &___maxChargeDuration_22; }
	inline void set_maxChargeDuration_22(float value)
	{
		___maxChargeDuration_22 = value;
	}

	inline static int32_t get_offset_of_ammoUsedOnStartCharge_23() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___ammoUsedOnStartCharge_23)); }
	inline float get_ammoUsedOnStartCharge_23() const { return ___ammoUsedOnStartCharge_23; }
	inline float* get_address_of_ammoUsedOnStartCharge_23() { return &___ammoUsedOnStartCharge_23; }
	inline void set_ammoUsedOnStartCharge_23(float value)
	{
		___ammoUsedOnStartCharge_23 = value;
	}

	inline static int32_t get_offset_of_ammoUsageRateWhileCharging_24() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___ammoUsageRateWhileCharging_24)); }
	inline float get_ammoUsageRateWhileCharging_24() const { return ___ammoUsageRateWhileCharging_24; }
	inline float* get_address_of_ammoUsageRateWhileCharging_24() { return &___ammoUsageRateWhileCharging_24; }
	inline void set_ammoUsageRateWhileCharging_24(float value)
	{
		___ammoUsageRateWhileCharging_24 = value;
	}

	inline static int32_t get_offset_of_weaponAnimator_25() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___weaponAnimator_25)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_weaponAnimator_25() const { return ___weaponAnimator_25; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_weaponAnimator_25() { return &___weaponAnimator_25; }
	inline void set_weaponAnimator_25(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___weaponAnimator_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weaponAnimator_25), (void*)value);
	}

	inline static int32_t get_offset_of_muzzleFlashPrefab_26() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___muzzleFlashPrefab_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_muzzleFlashPrefab_26() const { return ___muzzleFlashPrefab_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_muzzleFlashPrefab_26() { return &___muzzleFlashPrefab_26; }
	inline void set_muzzleFlashPrefab_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___muzzleFlashPrefab_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___muzzleFlashPrefab_26), (void*)value);
	}

	inline static int32_t get_offset_of_unparentMuzzleFlash_27() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___unparentMuzzleFlash_27)); }
	inline bool get_unparentMuzzleFlash_27() const { return ___unparentMuzzleFlash_27; }
	inline bool* get_address_of_unparentMuzzleFlash_27() { return &___unparentMuzzleFlash_27; }
	inline void set_unparentMuzzleFlash_27(bool value)
	{
		___unparentMuzzleFlash_27 = value;
	}

	inline static int32_t get_offset_of_shootSFX_28() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___shootSFX_28)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_shootSFX_28() const { return ___shootSFX_28; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_shootSFX_28() { return &___shootSFX_28; }
	inline void set_shootSFX_28(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___shootSFX_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shootSFX_28), (void*)value);
	}

	inline static int32_t get_offset_of_changeWeaponSFX_29() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___changeWeaponSFX_29)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_changeWeaponSFX_29() const { return ___changeWeaponSFX_29; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_changeWeaponSFX_29() { return &___changeWeaponSFX_29; }
	inline void set_changeWeaponSFX_29(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___changeWeaponSFX_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___changeWeaponSFX_29), (void*)value);
	}

	inline static int32_t get_offset_of_useContinuousShootSound_30() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___useContinuousShootSound_30)); }
	inline bool get_useContinuousShootSound_30() const { return ___useContinuousShootSound_30; }
	inline bool* get_address_of_useContinuousShootSound_30() { return &___useContinuousShootSound_30; }
	inline void set_useContinuousShootSound_30(bool value)
	{
		___useContinuousShootSound_30 = value;
	}

	inline static int32_t get_offset_of_continuousShootStartSFX_31() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___continuousShootStartSFX_31)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_continuousShootStartSFX_31() const { return ___continuousShootStartSFX_31; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_continuousShootStartSFX_31() { return &___continuousShootStartSFX_31; }
	inline void set_continuousShootStartSFX_31(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___continuousShootStartSFX_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___continuousShootStartSFX_31), (void*)value);
	}

	inline static int32_t get_offset_of_continuousShootLoopSFX_32() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___continuousShootLoopSFX_32)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_continuousShootLoopSFX_32() const { return ___continuousShootLoopSFX_32; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_continuousShootLoopSFX_32() { return &___continuousShootLoopSFX_32; }
	inline void set_continuousShootLoopSFX_32(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___continuousShootLoopSFX_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___continuousShootLoopSFX_32), (void*)value);
	}

	inline static int32_t get_offset_of_continuousShootEndSFX_33() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___continuousShootEndSFX_33)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_continuousShootEndSFX_33() const { return ___continuousShootEndSFX_33; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_continuousShootEndSFX_33() { return &___continuousShootEndSFX_33; }
	inline void set_continuousShootEndSFX_33(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___continuousShootEndSFX_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___continuousShootEndSFX_33), (void*)value);
	}

	inline static int32_t get_offset_of_m_continuousShootAudioSource_34() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___m_continuousShootAudioSource_34)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_m_continuousShootAudioSource_34() const { return ___m_continuousShootAudioSource_34; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_m_continuousShootAudioSource_34() { return &___m_continuousShootAudioSource_34; }
	inline void set_m_continuousShootAudioSource_34(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___m_continuousShootAudioSource_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_continuousShootAudioSource_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_wantsToShoot_35() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___m_wantsToShoot_35)); }
	inline bool get_m_wantsToShoot_35() const { return ___m_wantsToShoot_35; }
	inline bool* get_address_of_m_wantsToShoot_35() { return &___m_wantsToShoot_35; }
	inline void set_m_wantsToShoot_35(bool value)
	{
		___m_wantsToShoot_35 = value;
	}

	inline static int32_t get_offset_of_onShoot_36() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___onShoot_36)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onShoot_36() const { return ___onShoot_36; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onShoot_36() { return &___onShoot_36; }
	inline void set_onShoot_36(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onShoot_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onShoot_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentAmmo_37() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___m_CurrentAmmo_37)); }
	inline float get_m_CurrentAmmo_37() const { return ___m_CurrentAmmo_37; }
	inline float* get_address_of_m_CurrentAmmo_37() { return &___m_CurrentAmmo_37; }
	inline void set_m_CurrentAmmo_37(float value)
	{
		___m_CurrentAmmo_37 = value;
	}

	inline static int32_t get_offset_of_m_LastTimeShot_38() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___m_LastTimeShot_38)); }
	inline float get_m_LastTimeShot_38() const { return ___m_LastTimeShot_38; }
	inline float* get_address_of_m_LastTimeShot_38() { return &___m_LastTimeShot_38; }
	inline void set_m_LastTimeShot_38(float value)
	{
		___m_LastTimeShot_38 = value;
	}

	inline static int32_t get_offset_of_U3CLastChargeTriggerTimestampU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___U3CLastChargeTriggerTimestampU3Ek__BackingField_39)); }
	inline float get_U3CLastChargeTriggerTimestampU3Ek__BackingField_39() const { return ___U3CLastChargeTriggerTimestampU3Ek__BackingField_39; }
	inline float* get_address_of_U3CLastChargeTriggerTimestampU3Ek__BackingField_39() { return &___U3CLastChargeTriggerTimestampU3Ek__BackingField_39; }
	inline void set_U3CLastChargeTriggerTimestampU3Ek__BackingField_39(float value)
	{
		___U3CLastChargeTriggerTimestampU3Ek__BackingField_39 = value;
	}

	inline static int32_t get_offset_of_m_LastMuzzlePosition_40() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___m_LastMuzzlePosition_40)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_LastMuzzlePosition_40() const { return ___m_LastMuzzlePosition_40; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_LastMuzzlePosition_40() { return &___m_LastMuzzlePosition_40; }
	inline void set_m_LastMuzzlePosition_40(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_LastMuzzlePosition_40 = value;
	}

	inline static int32_t get_offset_of_U3CownerU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___U3CownerU3Ek__BackingField_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CownerU3Ek__BackingField_41() const { return ___U3CownerU3Ek__BackingField_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CownerU3Ek__BackingField_41() { return &___U3CownerU3Ek__BackingField_41; }
	inline void set_U3CownerU3Ek__BackingField_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CownerU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CownerU3Ek__BackingField_41), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsourcePrefabU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___U3CsourcePrefabU3Ek__BackingField_42)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CsourcePrefabU3Ek__BackingField_42() const { return ___U3CsourcePrefabU3Ek__BackingField_42; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CsourcePrefabU3Ek__BackingField_42() { return &___U3CsourcePrefabU3Ek__BackingField_42; }
	inline void set_U3CsourcePrefabU3Ek__BackingField_42(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CsourcePrefabU3Ek__BackingField_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsourcePrefabU3Ek__BackingField_42), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisChargingU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___U3CisChargingU3Ek__BackingField_43)); }
	inline bool get_U3CisChargingU3Ek__BackingField_43() const { return ___U3CisChargingU3Ek__BackingField_43; }
	inline bool* get_address_of_U3CisChargingU3Ek__BackingField_43() { return &___U3CisChargingU3Ek__BackingField_43; }
	inline void set_U3CisChargingU3Ek__BackingField_43(bool value)
	{
		___U3CisChargingU3Ek__BackingField_43 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentAmmoRatioU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___U3CcurrentAmmoRatioU3Ek__BackingField_44)); }
	inline float get_U3CcurrentAmmoRatioU3Ek__BackingField_44() const { return ___U3CcurrentAmmoRatioU3Ek__BackingField_44; }
	inline float* get_address_of_U3CcurrentAmmoRatioU3Ek__BackingField_44() { return &___U3CcurrentAmmoRatioU3Ek__BackingField_44; }
	inline void set_U3CcurrentAmmoRatioU3Ek__BackingField_44(float value)
	{
		___U3CcurrentAmmoRatioU3Ek__BackingField_44 = value;
	}

	inline static int32_t get_offset_of_U3CisWeaponActiveU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___U3CisWeaponActiveU3Ek__BackingField_45)); }
	inline bool get_U3CisWeaponActiveU3Ek__BackingField_45() const { return ___U3CisWeaponActiveU3Ek__BackingField_45; }
	inline bool* get_address_of_U3CisWeaponActiveU3Ek__BackingField_45() { return &___U3CisWeaponActiveU3Ek__BackingField_45; }
	inline void set_U3CisWeaponActiveU3Ek__BackingField_45(bool value)
	{
		___U3CisWeaponActiveU3Ek__BackingField_45 = value;
	}

	inline static int32_t get_offset_of_U3CisCoolingU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___U3CisCoolingU3Ek__BackingField_46)); }
	inline bool get_U3CisCoolingU3Ek__BackingField_46() const { return ___U3CisCoolingU3Ek__BackingField_46; }
	inline bool* get_address_of_U3CisCoolingU3Ek__BackingField_46() { return &___U3CisCoolingU3Ek__BackingField_46; }
	inline void set_U3CisCoolingU3Ek__BackingField_46(bool value)
	{
		___U3CisCoolingU3Ek__BackingField_46 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentChargeU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___U3CcurrentChargeU3Ek__BackingField_47)); }
	inline float get_U3CcurrentChargeU3Ek__BackingField_47() const { return ___U3CcurrentChargeU3Ek__BackingField_47; }
	inline float* get_address_of_U3CcurrentChargeU3Ek__BackingField_47() { return &___U3CcurrentChargeU3Ek__BackingField_47; }
	inline void set_U3CcurrentChargeU3Ek__BackingField_47(float value)
	{
		___U3CcurrentChargeU3Ek__BackingField_47 = value;
	}

	inline static int32_t get_offset_of_U3CmuzzleWorldVelocityU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___U3CmuzzleWorldVelocityU3Ek__BackingField_48)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CmuzzleWorldVelocityU3Ek__BackingField_48() const { return ___U3CmuzzleWorldVelocityU3Ek__BackingField_48; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CmuzzleWorldVelocityU3Ek__BackingField_48() { return &___U3CmuzzleWorldVelocityU3Ek__BackingField_48; }
	inline void set_U3CmuzzleWorldVelocityU3Ek__BackingField_48(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CmuzzleWorldVelocityU3Ek__BackingField_48 = value;
	}

	inline static int32_t get_offset_of_m_ShootAudioSource_49() { return static_cast<int32_t>(offsetof(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C, ___m_ShootAudioSource_49)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_m_ShootAudioSource_49() const { return ___m_ShootAudioSource_49; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_m_ShootAudioSource_49() { return &___m_ShootAudioSource_49; }
	inline void set_m_ShootAudioSource_49(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___m_ShootAudioSource_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ShootAudioSource_49), (void*)value);
	}
};


// WeaponFuelCellHandler
struct  WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean WeaponFuelCellHandler::SimultaneousFuelCellsUsage
	bool ___SimultaneousFuelCellsUsage_4;
	// UnityEngine.GameObject[] WeaponFuelCellHandler::fuelCells
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___fuelCells_5;
	// UnityEngine.Vector3 WeaponFuelCellHandler::fuelCellUsedPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___fuelCellUsedPosition_6;
	// UnityEngine.Vector3 WeaponFuelCellHandler::fuelCellUnusedPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___fuelCellUnusedPosition_7;
	// WeaponController WeaponFuelCellHandler::m_Weapon
	WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * ___m_Weapon_8;
	// System.Boolean[] WeaponFuelCellHandler::m_FuelCellsCooled
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___m_FuelCellsCooled_9;

public:
	inline static int32_t get_offset_of_SimultaneousFuelCellsUsage_4() { return static_cast<int32_t>(offsetof(WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A, ___SimultaneousFuelCellsUsage_4)); }
	inline bool get_SimultaneousFuelCellsUsage_4() const { return ___SimultaneousFuelCellsUsage_4; }
	inline bool* get_address_of_SimultaneousFuelCellsUsage_4() { return &___SimultaneousFuelCellsUsage_4; }
	inline void set_SimultaneousFuelCellsUsage_4(bool value)
	{
		___SimultaneousFuelCellsUsage_4 = value;
	}

	inline static int32_t get_offset_of_fuelCells_5() { return static_cast<int32_t>(offsetof(WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A, ___fuelCells_5)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_fuelCells_5() const { return ___fuelCells_5; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_fuelCells_5() { return &___fuelCells_5; }
	inline void set_fuelCells_5(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___fuelCells_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fuelCells_5), (void*)value);
	}

	inline static int32_t get_offset_of_fuelCellUsedPosition_6() { return static_cast<int32_t>(offsetof(WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A, ___fuelCellUsedPosition_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_fuelCellUsedPosition_6() const { return ___fuelCellUsedPosition_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_fuelCellUsedPosition_6() { return &___fuelCellUsedPosition_6; }
	inline void set_fuelCellUsedPosition_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___fuelCellUsedPosition_6 = value;
	}

	inline static int32_t get_offset_of_fuelCellUnusedPosition_7() { return static_cast<int32_t>(offsetof(WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A, ___fuelCellUnusedPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_fuelCellUnusedPosition_7() const { return ___fuelCellUnusedPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_fuelCellUnusedPosition_7() { return &___fuelCellUnusedPosition_7; }
	inline void set_fuelCellUnusedPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___fuelCellUnusedPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_Weapon_8() { return static_cast<int32_t>(offsetof(WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A, ___m_Weapon_8)); }
	inline WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * get_m_Weapon_8() const { return ___m_Weapon_8; }
	inline WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C ** get_address_of_m_Weapon_8() { return &___m_Weapon_8; }
	inline void set_m_Weapon_8(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * value)
	{
		___m_Weapon_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Weapon_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_FuelCellsCooled_9() { return static_cast<int32_t>(offsetof(WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A, ___m_FuelCellsCooled_9)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_m_FuelCellsCooled_9() const { return ___m_FuelCellsCooled_9; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_m_FuelCellsCooled_9() { return &___m_FuelCellsCooled_9; }
	inline void set_m_FuelCellsCooled_9(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___m_FuelCellsCooled_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FuelCellsCooled_9), (void*)value);
	}
};


// WeaponHUDManager
struct  WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform WeaponHUDManager::ammosPanel
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___ammosPanel_4;
	// UnityEngine.GameObject WeaponHUDManager::ammoCounterPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ammoCounterPrefab_5;
	// PlayerWeaponsManager WeaponHUDManager::m_PlayerWeaponsManager
	PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * ___m_PlayerWeaponsManager_6;
	// System.Collections.Generic.List`1<AmmoCounter> WeaponHUDManager::m_AmmoCounters
	List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * ___m_AmmoCounters_7;

public:
	inline static int32_t get_offset_of_ammosPanel_4() { return static_cast<int32_t>(offsetof(WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773, ___ammosPanel_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_ammosPanel_4() const { return ___ammosPanel_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_ammosPanel_4() { return &___ammosPanel_4; }
	inline void set_ammosPanel_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___ammosPanel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ammosPanel_4), (void*)value);
	}

	inline static int32_t get_offset_of_ammoCounterPrefab_5() { return static_cast<int32_t>(offsetof(WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773, ___ammoCounterPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ammoCounterPrefab_5() const { return ___ammoCounterPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ammoCounterPrefab_5() { return &___ammoCounterPrefab_5; }
	inline void set_ammoCounterPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ammoCounterPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ammoCounterPrefab_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerWeaponsManager_6() { return static_cast<int32_t>(offsetof(WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773, ___m_PlayerWeaponsManager_6)); }
	inline PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * get_m_PlayerWeaponsManager_6() const { return ___m_PlayerWeaponsManager_6; }
	inline PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB ** get_address_of_m_PlayerWeaponsManager_6() { return &___m_PlayerWeaponsManager_6; }
	inline void set_m_PlayerWeaponsManager_6(PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * value)
	{
		___m_PlayerWeaponsManager_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerWeaponsManager_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_AmmoCounters_7() { return static_cast<int32_t>(offsetof(WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773, ___m_AmmoCounters_7)); }
	inline List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * get_m_AmmoCounters_7() const { return ___m_AmmoCounters_7; }
	inline List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 ** get_address_of_m_AmmoCounters_7() { return &___m_AmmoCounters_7; }
	inline void set_m_AmmoCounters_7(List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * value)
	{
		___m_AmmoCounters_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AmmoCounters_7), (void*)value);
	}
};


// WeaponPickup
struct  WeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// WeaponController WeaponPickup::weaponPrefab
	WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * ___weaponPrefab_4;
	// Pickup WeaponPickup::m_Pickup
	Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * ___m_Pickup_5;

public:
	inline static int32_t get_offset_of_weaponPrefab_4() { return static_cast<int32_t>(offsetof(WeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2, ___weaponPrefab_4)); }
	inline WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * get_weaponPrefab_4() const { return ___weaponPrefab_4; }
	inline WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C ** get_address_of_weaponPrefab_4() { return &___weaponPrefab_4; }
	inline void set_weaponPrefab_4(WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * value)
	{
		___weaponPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___weaponPrefab_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Pickup_5() { return static_cast<int32_t>(offsetof(WeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2, ___m_Pickup_5)); }
	inline Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * get_m_Pickup_5() const { return ___m_Pickup_5; }
	inline Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 ** get_address_of_m_Pickup_5() { return &___m_Pickup_5; }
	inline void set_m_Pickup_5(Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * value)
	{
		___m_Pickup_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Pickup_5), (void*)value);
	}
};


// WorldspaceHealthBar
struct  WorldspaceHealthBar_t74F5087839EF95CD78752D943A539D313AA3B9E3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Health WorldspaceHealthBar::health
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * ___health_4;
	// UnityEngine.UI.Image WorldspaceHealthBar::healthBarImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___healthBarImage_5;
	// UnityEngine.Transform WorldspaceHealthBar::healthBarPivot
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___healthBarPivot_6;
	// System.Boolean WorldspaceHealthBar::hideFullHealthBar
	bool ___hideFullHealthBar_7;

public:
	inline static int32_t get_offset_of_health_4() { return static_cast<int32_t>(offsetof(WorldspaceHealthBar_t74F5087839EF95CD78752D943A539D313AA3B9E3, ___health_4)); }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * get_health_4() const { return ___health_4; }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D ** get_address_of_health_4() { return &___health_4; }
	inline void set_health_4(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * value)
	{
		___health_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___health_4), (void*)value);
	}

	inline static int32_t get_offset_of_healthBarImage_5() { return static_cast<int32_t>(offsetof(WorldspaceHealthBar_t74F5087839EF95CD78752D943A539D313AA3B9E3, ___healthBarImage_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_healthBarImage_5() const { return ___healthBarImage_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_healthBarImage_5() { return &___healthBarImage_5; }
	inline void set_healthBarImage_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___healthBarImage_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthBarImage_5), (void*)value);
	}

	inline static int32_t get_offset_of_healthBarPivot_6() { return static_cast<int32_t>(offsetof(WorldspaceHealthBar_t74F5087839EF95CD78752D943A539D313AA3B9E3, ___healthBarPivot_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_healthBarPivot_6() const { return ___healthBarPivot_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_healthBarPivot_6() { return &___healthBarPivot_6; }
	inline void set_healthBarPivot_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___healthBarPivot_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthBarPivot_6), (void*)value);
	}

	inline static int32_t get_offset_of_hideFullHealthBar_7() { return static_cast<int32_t>(offsetof(WorldspaceHealthBar_t74F5087839EF95CD78752D943A539D313AA3B9E3, ___hideFullHealthBar_7)); }
	inline bool get_hideFullHealthBar_7() const { return ___hideFullHealthBar_7; }
	inline bool* get_address_of_hideFullHealthBar_7() { return &___hideFullHealthBar_7; }
	inline void set_hideFullHealthBar_7(bool value)
	{
		___hideFullHealthBar_7 = value;
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_11;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_12;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_13;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_14;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_18;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_CachedMesh_21;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_CachedUvs_22;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_23;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_11() const { return ___m_RectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_11() { return &___m_RectTransform_11; }
	inline void set_m_RectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_12)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_12() const { return ___m_CanvasRenderer_12; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_12() { return &___m_CanvasRenderer_12; }
	inline void set_m_CanvasRenderer_12(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_13)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_14)); }
	inline bool get_m_VertsDirty_14() const { return ___m_VertsDirty_14; }
	inline bool* get_address_of_m_VertsDirty_14() { return &___m_VertsDirty_14; }
	inline void set_m_VertsDirty_14(bool value)
	{
		___m_VertsDirty_14 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_15)); }
	inline bool get_m_MaterialDirty_15() const { return ___m_MaterialDirty_15; }
	inline bool* get_address_of_m_MaterialDirty_15() { return &___m_MaterialDirty_15; }
	inline void set_m_MaterialDirty_15(bool value)
	{
		___m_MaterialDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_16() const { return ___m_OnDirtyLayoutCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_16() { return &___m_OnDirtyLayoutCallback_16; }
	inline void set_m_OnDirtyLayoutCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_17)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_17() const { return ___m_OnDirtyVertsCallback_17; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_17() { return &___m_OnDirtyVertsCallback_17; }
	inline void set_m_OnDirtyVertsCallback_17(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_18)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_18() const { return ___m_OnDirtyMaterialCallback_18; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_18() { return &___m_OnDirtyMaterialCallback_18; }
	inline void set_m_OnDirtyMaterialCallback_18(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_21() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedMesh_21)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_CachedMesh_21() const { return ___m_CachedMesh_21; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_CachedMesh_21() { return &___m_CachedMesh_21; }
	inline void set_m_CachedMesh_21(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_CachedMesh_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_22() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedUvs_22)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_CachedUvs_22() const { return ___m_CachedUvs_22; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_CachedUvs_22() { return &___m_CachedUvs_22; }
	inline void set_m_CachedUvs_22(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_CachedUvs_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_23() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_23)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_23() const { return ___m_ColorTweenRunner_23; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_23() { return &___m_ColorTweenRunner_23; }
	inline void set_m_ColorTweenRunner_23(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_24(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_24 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_19;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_20;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_19)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_19() const { return ___s_Mesh_19; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_19() { return &___s_Mesh_19; }
	inline void set_s_Mesh_19(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_20)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_20() const { return ___s_VertexHelper_20; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_20() { return &___s_VertexHelper_20; }
	inline void set_s_VertexHelper_20(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_25;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_26;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_27;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_30;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_31;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_32;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_33;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_34;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_25)); }
	inline bool get_m_ShouldRecalculateStencil_25() const { return ___m_ShouldRecalculateStencil_25; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_25() { return &___m_ShouldRecalculateStencil_25; }
	inline void set_m_ShouldRecalculateStencil_25(bool value)
	{
		___m_ShouldRecalculateStencil_25 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_26() const { return ___m_MaskMaterial_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_26() { return &___m_MaskMaterial_26; }
	inline void set_m_MaskMaterial_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_27)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_27() const { return ___m_ParentMask_27; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_27() { return &___m_ParentMask_27; }
	inline void set_m_ParentMask_27(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_28)); }
	inline bool get_m_Maskable_28() const { return ___m_Maskable_28; }
	inline bool* get_address_of_m_Maskable_28() { return &___m_Maskable_28; }
	inline void set_m_Maskable_28(bool value)
	{
		___m_Maskable_28 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IsMaskingGraphic_29)); }
	inline bool get_m_IsMaskingGraphic_29() const { return ___m_IsMaskingGraphic_29; }
	inline bool* get_address_of_m_IsMaskingGraphic_29() { return &___m_IsMaskingGraphic_29; }
	inline void set_m_IsMaskingGraphic_29(bool value)
	{
		___m_IsMaskingGraphic_29 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_30)); }
	inline bool get_m_IncludeForMasking_30() const { return ___m_IncludeForMasking_30; }
	inline bool* get_address_of_m_IncludeForMasking_30() { return &___m_IncludeForMasking_30; }
	inline void set_m_IncludeForMasking_30(bool value)
	{
		___m_IncludeForMasking_30 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_31)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_31() const { return ___m_OnCullStateChanged_31; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_31() { return &___m_OnCullStateChanged_31; }
	inline void set_m_OnCullStateChanged_31(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_32)); }
	inline bool get_m_ShouldRecalculate_32() const { return ___m_ShouldRecalculate_32; }
	inline bool* get_address_of_m_ShouldRecalculate_32() { return &___m_ShouldRecalculate_32; }
	inline void set_m_ShouldRecalculate_32(bool value)
	{
		___m_ShouldRecalculate_32 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_33)); }
	inline int32_t get_m_StencilValue_33() const { return ___m_StencilValue_33; }
	inline int32_t* get_address_of_m_StencilValue_33() { return &___m_StencilValue_33; }
	inline void set_m_StencilValue_33(int32_t value)
	{
		___m_StencilValue_33 = value;
	}

	inline static int32_t get_offset_of_m_Corners_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_34)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_34() const { return ___m_Corners_34; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_34() { return &___m_Corners_34; }
	inline void set_m_Corners_34(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_34), (void*)value);
	}
};


// UnityEngine.UI.Image
struct  Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_Sprite_36;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_OverrideSprite_37;
	// UnityEngine.UI.Image_Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_38;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_39;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_40;
	// UnityEngine.UI.Image_FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_41;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_42;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_43;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_44;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_45;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_46;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_47;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_48;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_49;

public:
	inline static int32_t get_offset_of_m_Sprite_36() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Sprite_36)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_Sprite_36() const { return ___m_Sprite_36; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_Sprite_36() { return &___m_Sprite_36; }
	inline void set_m_Sprite_36(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_Sprite_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_37() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_OverrideSprite_37)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_OverrideSprite_37() const { return ___m_OverrideSprite_37; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_OverrideSprite_37() { return &___m_OverrideSprite_37; }
	inline void set_m_OverrideSprite_37(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_OverrideSprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_38() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Type_38)); }
	inline int32_t get_m_Type_38() const { return ___m_Type_38; }
	inline int32_t* get_address_of_m_Type_38() { return &___m_Type_38; }
	inline void set_m_Type_38(int32_t value)
	{
		___m_Type_38 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_39() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PreserveAspect_39)); }
	inline bool get_m_PreserveAspect_39() const { return ___m_PreserveAspect_39; }
	inline bool* get_address_of_m_PreserveAspect_39() { return &___m_PreserveAspect_39; }
	inline void set_m_PreserveAspect_39(bool value)
	{
		___m_PreserveAspect_39 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_40() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillCenter_40)); }
	inline bool get_m_FillCenter_40() const { return ___m_FillCenter_40; }
	inline bool* get_address_of_m_FillCenter_40() { return &___m_FillCenter_40; }
	inline void set_m_FillCenter_40(bool value)
	{
		___m_FillCenter_40 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_41() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillMethod_41)); }
	inline int32_t get_m_FillMethod_41() const { return ___m_FillMethod_41; }
	inline int32_t* get_address_of_m_FillMethod_41() { return &___m_FillMethod_41; }
	inline void set_m_FillMethod_41(int32_t value)
	{
		___m_FillMethod_41 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_42() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillAmount_42)); }
	inline float get_m_FillAmount_42() const { return ___m_FillAmount_42; }
	inline float* get_address_of_m_FillAmount_42() { return &___m_FillAmount_42; }
	inline void set_m_FillAmount_42(float value)
	{
		___m_FillAmount_42 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_43() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillClockwise_43)); }
	inline bool get_m_FillClockwise_43() const { return ___m_FillClockwise_43; }
	inline bool* get_address_of_m_FillClockwise_43() { return &___m_FillClockwise_43; }
	inline void set_m_FillClockwise_43(bool value)
	{
		___m_FillClockwise_43 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_44() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillOrigin_44)); }
	inline int32_t get_m_FillOrigin_44() const { return ___m_FillOrigin_44; }
	inline int32_t* get_address_of_m_FillOrigin_44() { return &___m_FillOrigin_44; }
	inline void set_m_FillOrigin_44(int32_t value)
	{
		___m_FillOrigin_44 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_45() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_AlphaHitTestMinimumThreshold_45)); }
	inline float get_m_AlphaHitTestMinimumThreshold_45() const { return ___m_AlphaHitTestMinimumThreshold_45; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_45() { return &___m_AlphaHitTestMinimumThreshold_45; }
	inline void set_m_AlphaHitTestMinimumThreshold_45(float value)
	{
		___m_AlphaHitTestMinimumThreshold_45 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_46() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Tracked_46)); }
	inline bool get_m_Tracked_46() const { return ___m_Tracked_46; }
	inline bool* get_address_of_m_Tracked_46() { return &___m_Tracked_46; }
	inline void set_m_Tracked_46(bool value)
	{
		___m_Tracked_46 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_47() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_UseSpriteMesh_47)); }
	inline bool get_m_UseSpriteMesh_47() const { return ___m_UseSpriteMesh_47; }
	inline bool* get_address_of_m_UseSpriteMesh_47() { return &___m_UseSpriteMesh_47; }
	inline void set_m_UseSpriteMesh_47(bool value)
	{
		___m_UseSpriteMesh_47 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_48() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PixelsPerUnitMultiplier_48)); }
	inline float get_m_PixelsPerUnitMultiplier_48() const { return ___m_PixelsPerUnitMultiplier_48; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_48() { return &___m_PixelsPerUnitMultiplier_48; }
	inline void set_m_PixelsPerUnitMultiplier_48(float value)
	{
		___m_PixelsPerUnitMultiplier_48 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_49() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_CachedReferencePixelsPerUnit_49)); }
	inline float get_m_CachedReferencePixelsPerUnit_49() const { return ___m_CachedReferencePixelsPerUnit_49; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_49() { return &___m_CachedReferencePixelsPerUnit_49; }
	inline void set_m_CachedReferencePixelsPerUnit_49(float value)
	{
		___m_CachedReferencePixelsPerUnit_49 = value;
	}
};

struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_ETC1DefaultUI_35;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_VertScratch_50;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_UVScratch_51;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Xy_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Uv_53;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA * ___m_TrackedTexturelessImages_54;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_55;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_35() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_ETC1DefaultUI_35)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_ETC1DefaultUI_35() const { return ___s_ETC1DefaultUI_35; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_ETC1DefaultUI_35() { return &___s_ETC1DefaultUI_35; }
	inline void set_s_ETC1DefaultUI_35(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_ETC1DefaultUI_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_35), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_50() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_VertScratch_50)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_VertScratch_50() const { return ___s_VertScratch_50; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_VertScratch_50() { return &___s_VertScratch_50; }
	inline void set_s_VertScratch_50(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_VertScratch_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_50), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_51() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_UVScratch_51)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_UVScratch_51() const { return ___s_UVScratch_51; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_UVScratch_51() { return &___s_UVScratch_51; }
	inline void set_s_UVScratch_51(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_UVScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_52() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Xy_52)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Xy_52() const { return ___s_Xy_52; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Xy_52() { return &___s_Xy_52; }
	inline void set_s_Xy_52(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Xy_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_53() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Uv_53)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Uv_53() const { return ___s_Uv_53; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Uv_53() { return &___s_Uv_53; }
	inline void set_s_Uv_53(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Uv_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_53), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_54() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___m_TrackedTexturelessImages_54)); }
	inline List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA * get_m_TrackedTexturelessImages_54() const { return ___m_TrackedTexturelessImages_54; }
	inline List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA ** get_address_of_m_TrackedTexturelessImages_54() { return &___m_TrackedTexturelessImages_54; }
	inline void set_m_TrackedTexturelessImages_54(List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA * value)
	{
		___m_TrackedTexturelessImages_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_54), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_55() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Initialized_55)); }
	inline bool get_s_Initialized_55() const { return ___s_Initialized_55; }
	inline bool* get_address_of_s_Initialized_55() { return &___s_Initialized_55; }
	inline void set_s_Initialized_55(bool value)
	{
		___s_Initialized_55 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * m_Items[1];

public:
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * m_Items[1];

public:
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void DebugUtility::HandleErrorIfNullGetComponent<System.Object,System.Object>(UnityEngine.Component,UnityEngine.Component,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebugUtility_HandleErrorIfNullGetComponent_TisRuntimeObject_TisRuntimeObject_m0853B8F56BBFE566ED61CE0827C7D878331D54E4_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___source1, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___onObject2, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_mE3957366B74863C807E6E8A23D239A0CB079BB9C_gshared (const RuntimeMethod* method);
// System.Void DebugUtility::HandleErrorIfNullFindObject<System.Object,System.Object>(UnityEngine.Object,UnityEngine.Component)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebugUtility_HandleErrorIfNullFindObject_TisRuntimeObject_TisRuntimeObject_mEB795A33EA3D14E3AD81BA13EBF7C0FE786D3CD9_gshared (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___source1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_2__ctor_m1B64926E89A02F7759BD3F92051761AFCCDCCF46_gshared (UnityAction_2_t4BFEF410445D61DFA0D55D04E20ED20869EAD343 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_1__ctor_mF6AE3BA9395C61DE1466BE7BB863A77F3584EEC3_gshared (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m765EEDB3D86CE4EADC667B84C18E793D14144E1D_gshared (RuntimeObject * ___original0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m50D861A91F15E3169935F47FB656C3ED5486E74E_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Component_GetComponentsInChildren_TisRuntimeObject_m6D4C38C330FCFD3C323B34031A7E877A5E2D453A_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);

// !!0 UnityEngine.Component::GetComponent<WeaponController>()
inline WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * Component_GetComponent_TisWeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C_m4A2B12FF0B97DC381C3C3D31D3090889AA93D808 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void DebugUtility::HandleErrorIfNullGetComponent<WeaponController,WeaponFuelCellHandler>(UnityEngine.Component,UnityEngine.Component,UnityEngine.GameObject)
inline void DebugUtility_HandleErrorIfNullGetComponent_TisWeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C_TisWeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A_mE36EED9FBC1F3EF970D00FBD4DFF4525C767674D (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___source1, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___onObject2, const RuntimeMethod* method)
{
	((  void (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))DebugUtility_HandleErrorIfNullGetComponent_TisRuntimeObject_TisRuntimeObject_m0853B8F56BBFE566ED61CE0827C7D878331D54E4_gshared)(___component0, ___source1, ___onObject2, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Single WeaponController::get_currentAmmoRatio()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float WeaponController_get_currentAmmoRatio_mF7180E6799FB3AD19BE14019CE98B6A12125C842_inline (WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_InverseLerp_m7054CDF25056E9B27D2467F91C95D628508F1F31 (float ___a0, float ___b1, float ___value2, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B (float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<PlayerWeaponsManager>()
inline PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * Object_FindObjectOfType_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_m59AFFA4CEAD4A223D456E870A02391948EB93FA6 (const RuntimeMethod* method)
{
	return ((  PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_mE3957366B74863C807E6E8A23D239A0CB079BB9C_gshared)(method);
}
// System.Void DebugUtility::HandleErrorIfNullFindObject<PlayerWeaponsManager,WeaponHUDManager>(UnityEngine.Object,UnityEngine.Component)
inline void DebugUtility_HandleErrorIfNullFindObject_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_TisWeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773_m2AF55E66787FD7690127282340AB8101B5E1032D (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___source1, const RuntimeMethod* method)
{
	((  void (*) (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))DebugUtility_HandleErrorIfNullFindObject_TisRuntimeObject_TisRuntimeObject_mEB795A33EA3D14E3AD81BA13EBF7C0FE786D3CD9_gshared)(___obj0, ___source1, method);
}
// WeaponController PlayerWeaponsManager::GetActiveWeapon()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * PlayerWeaponsManager_GetActiveWeapon_m2E9EF7BAE1D06DB5CCC560272A3FC7754FBC3ABD (PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___exists0, const RuntimeMethod* method);
// System.Int32 PlayerWeaponsManager::get_activeWeaponIndex()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PlayerWeaponsManager_get_activeWeaponIndex_mC60445132555E8ABA60E7B70B18C05FC15426764_inline (PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * __this, const RuntimeMethod* method);
// System.Void WeaponHUDManager::AddWeapon(WeaponController,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponHUDManager_AddWeapon_m40F4058D81FFCA0640432F656F1506825190559D (WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773 * __this, WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * ___newWeapon0, int32_t ___weaponIndex1, const RuntimeMethod* method);
// System.Void WeaponHUDManager::ChangeWeapon(WeaponController)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponHUDManager_ChangeWeapon_mF6F576F5B15E561DF5D09CCE7417BA699E072683 (WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773 * __this, WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * ___weapon0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<WeaponController,System.Int32>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_2__ctor_m94489AC66327CA797AFC605B03BED338805074C7 (UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_2__ctor_m1B64926E89A02F7759BD3F92051761AFCCDCCF46_gshared)(__this, ___object0, ___method1, method);
}
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<WeaponController>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_m9362988F4BB4CD4FE51DB792660114863167415B (UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_mF6AE3BA9395C61DE1466BE7BB863A77F3584EEC3_gshared)(__this, ___object0, ___method1, method);
}
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m1CDF66D563B03D37B37264800222D4F3B307EDA0 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___original0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent1, const RuntimeMethod* method)
{
	return ((  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m765EEDB3D86CE4EADC667B84C18E793D14144E1D_gshared)(___original0, ___parent1, method);
}
// !!0 UnityEngine.GameObject::GetComponent<AmmoCounter>()
inline AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * GameObject_GetComponent_TisAmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1_m39A810E8DC3B5DED898125C40BAFAB7B76097890 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GameObject_get_gameObject_mB8D6D847ABF95430B098554F3F2D63EC1D30C815 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void DebugUtility::HandleErrorIfNullGetComponent<AmmoCounter,WeaponHUDManager>(UnityEngine.Component,UnityEngine.Component,UnityEngine.GameObject)
inline void DebugUtility_HandleErrorIfNullGetComponent_TisAmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1_TisWeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773_mD00757FACC332D1A15B551F8489368227494D2FE (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___source1, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___onObject2, const RuntimeMethod* method)
{
	((  void (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))DebugUtility_HandleErrorIfNullGetComponent_TisRuntimeObject_TisRuntimeObject_m0853B8F56BBFE566ED61CE0827C7D878331D54E4_gshared)(___component0, ___source1, ___onObject2, method);
}
// System.Void AmmoCounter::Initialize(WeaponController,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AmmoCounter_Initialize_m8213538E0F5CCEF79F8FF69501026966062B9FAB (AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * __this, WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * ___weapon0, int32_t ___weaponIndex1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<AmmoCounter>::Add(!0)
inline void List_1_Add_mB69FED340E5222A49CA180DAD48E3BD3D31B05A7 (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * __this, AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 *, AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// !0 System.Collections.Generic.List`1<AmmoCounter>::get_Item(System.Int32)
inline AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * List_1_get_Item_m612C715B84047B764A1764A095EDD7E7CF2EA032_inline (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * (*) (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Int32 AmmoCounter::get_weaponCounterIndex()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t AmmoCounter_get_weaponCounterIndex_mB3100D3B5713CA73962911C3D47D0E84BD4584B6_inline (AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<AmmoCounter>::get_Count()
inline int32_t List_1_get_Count_m104F10F39220D74146D1DE9669E44D6AB9E09802_inline (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<AmmoCounter>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_mF496CA03E4E0DC9E1F02770EE5B86E17A49212B7 (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m50D861A91F15E3169935F47FB656C3ED5486E74E_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.UI.LayoutRebuilder::ForceRebuildLayoutImmediate(UnityEngine.RectTransform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LayoutRebuilder_ForceRebuildLayoutImmediate_m376EFAB3B8D5C86A085DCFEECA0538B8BA03B732 (RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___layoutRoot0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<AmmoCounter>::.ctor()
inline void List_1__ctor_mEDA6C28C20A6EEA03532D2EF4E7196BCB85A6859 (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<Pickup>()
inline Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * Component_GetComponent_TisPickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281_m7BE6031779B73EEFDE3F1F50ABEA23A1EF8DE198 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// System.Void DebugUtility::HandleErrorIfNullGetComponent<Pickup,WeaponPickup>(UnityEngine.Component,UnityEngine.Component,UnityEngine.GameObject)
inline void DebugUtility_HandleErrorIfNullGetComponent_TisPickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281_TisWeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2_m4356919BC776A07298536A42B1D737045A9768B2 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___source1, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___onObject2, const RuntimeMethod* method)
{
	((  void (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))DebugUtility_HandleErrorIfNullGetComponent_TisRuntimeObject_TisRuntimeObject_m0853B8F56BBFE566ED61CE0827C7D878331D54E4_gshared)(___component0, ___source1, ___onObject2, method);
}
// System.Void UnityEngine.Events.UnityAction`1<PlayerCharacterController>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_m4317C8EF454DAF2A7178F5A3854C7271732082FA (UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_mF6AE3BA9395C61DE1466BE7BB863A77F3584EEC3_gshared)(__this, ___object0, ___method1, method);
}
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Transform>()
inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* Component_GetComponentsInChildren_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m335E18CE04D2AD7313D89DBEA3A88EDB76255113 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m6D4C38C330FCFD3C323B34031A7E877A5E2D453A_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_set_layer_mDAC8037FCFD0CE62DB66004C4342EA20CF604907 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, int32_t ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<PlayerWeaponsManager>()
inline PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * Component_GetComponent_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_m6268AA831D180B5189871925A88950D5E2D9F873 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// System.Boolean PlayerWeaponsManager::AddWeapon(WeaponController)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerWeaponsManager_AddWeapon_m6687BD9B7E34BB41035B0CB80BD14EF2488B5761 (PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * __this, WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * ___weaponPrefab0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void PlayerWeaponsManager::SwitchWeapon(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerWeaponsManager_SwitchWeapon_m08848BFC67A6310BEEC3D2717525745A25997DA2 (PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * __this, bool ___ascendingOrder0, const RuntimeMethod* method);
// System.Void Pickup::PlayPickupFeedback()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pickup_PlayPickupFeedback_mD48A3AE0650E0A46BE9490336496B59741B0F1FC (Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * __this, const RuntimeMethod* method);
// System.Single Health::get_currentHealth()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float Health_get_currentHealth_m3702D4E7A03A5153B3383738CC91CECC14EA5D47_inline (Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_fillAmount(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_fillAmount_m613B7AC920F2EA886AF0D931005C6CD13F89A160 (Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * __this, float ___value0, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_LookAt_m3EC94482B2585FE03AEEDF90325A1F0B9A84960E (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition0, const RuntimeMethod* method);
// System.Single UnityEngine.UI.Image::get_fillAmount()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float Image_get_fillAmount_m5FF6EE8DB33C4A219B3677FB24F50A6E5CCF44F0_inline (Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WeaponFuelCellHandler::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponFuelCellHandler_Start_mC5BDF5D098F7138A800B85BA4D934514C8DDD3FC (WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeaponFuelCellHandler_Start_mC5BDF5D098F7138A800B85BA4D934514C8DDD3FC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// m_Weapon = GetComponent<WeaponController>();
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_0 = Component_GetComponent_TisWeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C_m4A2B12FF0B97DC381C3C3D31D3090889AA93D808(__this, /*hidden argument*/Component_GetComponent_TisWeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C_m4A2B12FF0B97DC381C3C3D31D3090889AA93D808_RuntimeMethod_var);
		__this->set_m_Weapon_8(L_0);
		// DebugUtility.HandleErrorIfNullGetComponent<WeaponController, WeaponFuelCellHandler>(m_Weapon, this, gameObject);
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_1 = __this->get_m_Weapon_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		DebugUtility_HandleErrorIfNullGetComponent_TisWeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C_TisWeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A_mE36EED9FBC1F3EF970D00FBD4DFF4525C767674D(L_1, __this, L_2, /*hidden argument*/DebugUtility_HandleErrorIfNullGetComponent_TisWeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C_TisWeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A_mE36EED9FBC1F3EF970D00FBD4DFF4525C767674D_RuntimeMethod_var);
		// m_FuelCellsCooled = new bool[fuelCells.Length];
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_3 = __this->get_fuelCells_5();
		NullCheck(L_3);
		BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* L_4 = (BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040*)(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040*)SZArrayNew(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_3)->max_length)))));
		__this->set_m_FuelCellsCooled_9(L_4);
		// for (int i = 0; i < m_FuelCellsCooled.Length; i++)
		V_0 = 0;
		goto IL_0042;
	}

IL_0035:
	{
		// m_FuelCellsCooled[i] = true;
		BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* L_5 = __this->get_m_FuelCellsCooled_9();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (bool)1);
		// for (int i = 0; i < m_FuelCellsCooled.Length; i++)
		int32_t L_7 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0042:
	{
		// for (int i = 0; i < m_FuelCellsCooled.Length; i++)
		int32_t L_8 = V_0;
		BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* L_9 = __this->get_m_FuelCellsCooled_9();
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_0035;
		}
	}
	{
		// }
		return;
	}
}
// System.Void WeaponFuelCellHandler::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponFuelCellHandler_Update_mDB4277536619C89299A9072410D1462BADEC4A47 (WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeaponFuelCellHandler_Update_mDB4277536619C89299A9072410D1462BADEC4A47_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		// if (SimultaneousFuelCellsUsage)
		bool L_0 = __this->get_SimultaneousFuelCellsUsage_4();
		if (!L_0)
		{
			goto IL_004a;
		}
	}
	{
		// for (int i = 0; i < fuelCells.Length; i++)
		V_0 = 0;
		goto IL_003e;
	}

IL_000c:
	{
		// fuelCells[i].transform.localPosition = Vector3.Lerp(fuelCellUsedPosition, fuelCellUnusedPosition, m_Weapon.currentAmmoRatio);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_1 = __this->get_fuelCells_5();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_4, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = __this->get_fuelCellUsedPosition_6();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = __this->get_fuelCellUnusedPosition_7();
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_8 = __this->get_m_Weapon_8();
		NullCheck(L_8);
		float L_9 = WeaponController_get_currentAmmoRatio_mF7180E6799FB3AD19BE14019CE98B6A12125C842_inline(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1(L_6, L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_5, L_10, /*hidden argument*/NULL);
		// for (int i = 0; i < fuelCells.Length; i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_003e:
	{
		// for (int i = 0; i < fuelCells.Length; i++)
		int32_t L_12 = V_0;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_13 = __this->get_fuelCells_5();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length)))))))
		{
			goto IL_000c;
		}
	}
	{
		// }
		return;
	}

IL_004a:
	{
		// for (int i = 0; i < fuelCells.Length; i++)
		V_1 = 0;
		goto IL_00a8;
	}

IL_004e:
	{
		// float length = fuelCells.Length;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_14 = __this->get_fuelCells_5();
		NullCheck(L_14);
		V_2 = (((float)((float)(((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length)))))));
		// float lim1 = i / length;
		int32_t L_15 = V_1;
		float L_16 = V_2;
		// float lim2 = (i + 1) / length;
		int32_t L_17 = V_1;
		float L_18 = V_2;
		V_3 = ((float)((float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1)))))/(float)L_18));
		// float value = Mathf.InverseLerp(lim1, lim2, m_Weapon.currentAmmoRatio);
		float L_19 = V_3;
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_20 = __this->get_m_Weapon_8();
		NullCheck(L_20);
		float L_21 = WeaponController_get_currentAmmoRatio_mF7180E6799FB3AD19BE14019CE98B6A12125C842_inline(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_22 = Mathf_InverseLerp_m7054CDF25056E9B27D2467F91C95D628508F1F31(((float)((float)(((float)((float)L_15)))/(float)L_16)), L_19, L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		// value = Mathf.Clamp01(value);
		float L_23 = V_4;
		float L_24 = Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B(L_23, /*hidden argument*/NULL);
		V_4 = L_24;
		// fuelCells[i].transform.localPosition = Vector3.Lerp(fuelCellUsedPosition, fuelCellUnusedPosition, value);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_25 = __this->get_fuelCells_5();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_28);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_29 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_28, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30 = __this->get_fuelCellUsedPosition_6();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = __this->get_fuelCellUnusedPosition_7();
		float L_32 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_33 = Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1(L_30, L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_29, L_33, /*hidden argument*/NULL);
		// for (int i = 0; i < fuelCells.Length; i++)
		int32_t L_34 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
	}

IL_00a8:
	{
		// for (int i = 0; i < fuelCells.Length; i++)
		int32_t L_35 = V_1;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_36 = __this->get_fuelCells_5();
		NullCheck(L_36);
		if ((((int32_t)L_35) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_36)->max_length)))))))
		{
			goto IL_004e;
		}
	}
	{
		// }
		return;
	}
}
// System.Void WeaponFuelCellHandler::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponFuelCellHandler__ctor_m5D0C4964CB7F4C149BFEE22852F4FB224F4BF4AA (WeaponFuelCellHandler_t99E52276E655A35DC1A4A4ED6EA087B48C26002A * __this, const RuntimeMethod* method)
{
	{
		// public Vector3 fuelCellUnusedPosition = new Vector3(0f, -0.1f, 0f);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_0), (0.0f), (-0.1f), (0.0f), /*hidden argument*/NULL);
		__this->set_fuelCellUnusedPosition_7(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WeaponHUDManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponHUDManager_Start_mB3691A6FADA9D605A53BD1B56E24442CBF7B97CC (WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeaponHUDManager_Start_mB3691A6FADA9D605A53BD1B56E24442CBF7B97CC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * V_0 = NULL;
	{
		// m_PlayerWeaponsManager = FindObjectOfType<PlayerWeaponsManager>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_0 = Object_FindObjectOfType_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_m59AFFA4CEAD4A223D456E870A02391948EB93FA6(/*hidden argument*/Object_FindObjectOfType_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_m59AFFA4CEAD4A223D456E870A02391948EB93FA6_RuntimeMethod_var);
		__this->set_m_PlayerWeaponsManager_6(L_0);
		// DebugUtility.HandleErrorIfNullFindObject<PlayerWeaponsManager, WeaponHUDManager>(m_PlayerWeaponsManager, this);
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_1 = __this->get_m_PlayerWeaponsManager_6();
		DebugUtility_HandleErrorIfNullFindObject_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_TisWeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773_m2AF55E66787FD7690127282340AB8101B5E1032D(L_1, __this, /*hidden argument*/DebugUtility_HandleErrorIfNullFindObject_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_TisWeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773_m2AF55E66787FD7690127282340AB8101B5E1032D_RuntimeMethod_var);
		// WeaponController activeWeapon = m_PlayerWeaponsManager.GetActiveWeapon();
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_2 = __this->get_m_PlayerWeaponsManager_6();
		NullCheck(L_2);
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_3 = PlayerWeaponsManager_GetActiveWeapon_m2E9EF7BAE1D06DB5CCC560272A3FC7754FBC3ABD(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// if (activeWeapon)
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_4 = V_0;
		bool L_5 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		// AddWeapon(activeWeapon, m_PlayerWeaponsManager.activeWeaponIndex);
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_6 = V_0;
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_7 = __this->get_m_PlayerWeaponsManager_6();
		NullCheck(L_7);
		int32_t L_8 = PlayerWeaponsManager_get_activeWeaponIndex_mC60445132555E8ABA60E7B70B18C05FC15426764_inline(L_7, /*hidden argument*/NULL);
		WeaponHUDManager_AddWeapon_m40F4058D81FFCA0640432F656F1506825190559D(__this, L_6, L_8, /*hidden argument*/NULL);
		// ChangeWeapon(activeWeapon);
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_9 = V_0;
		WeaponHUDManager_ChangeWeapon_mF6F576F5B15E561DF5D09CCE7417BA699E072683(__this, L_9, /*hidden argument*/NULL);
	}

IL_0044:
	{
		// m_PlayerWeaponsManager.onAddedWeapon += AddWeapon;
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_10 = __this->get_m_PlayerWeaponsManager_6();
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_11 = L_10;
		NullCheck(L_11);
		UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * L_12 = L_11->get_onAddedWeapon_26();
		UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * L_13 = (UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C *)il2cpp_codegen_object_new(UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m94489AC66327CA797AFC605B03BED338805074C7(L_13, __this, (intptr_t)((intptr_t)WeaponHUDManager_AddWeapon_m40F4058D81FFCA0640432F656F1506825190559D_RuntimeMethod_var), /*hidden argument*/UnityAction_2__ctor_m94489AC66327CA797AFC605B03BED338805074C7_RuntimeMethod_var);
		Delegate_t * L_14 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		L_11->set_onAddedWeapon_26(((UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C *)CastclassSealed((RuntimeObject*)L_14, UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C_il2cpp_TypeInfo_var)));
		// m_PlayerWeaponsManager.onRemovedWeapon += RemoveWeapon;
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_15 = __this->get_m_PlayerWeaponsManager_6();
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_16 = L_15;
		NullCheck(L_16);
		UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * L_17 = L_16->get_onRemovedWeapon_27();
		UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C * L_18 = (UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C *)il2cpp_codegen_object_new(UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m94489AC66327CA797AFC605B03BED338805074C7(L_18, __this, (intptr_t)((intptr_t)WeaponHUDManager_RemoveWeapon_m01F57E6BDDB655C20A2308D7D15FD8201AE58F15_RuntimeMethod_var), /*hidden argument*/UnityAction_2__ctor_m94489AC66327CA797AFC605B03BED338805074C7_RuntimeMethod_var);
		Delegate_t * L_19 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		L_16->set_onRemovedWeapon_27(((UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C *)CastclassSealed((RuntimeObject*)L_19, UnityAction_2_tD554EEECE8D1B737178AC071C4CEFD15988DA88C_il2cpp_TypeInfo_var)));
		// m_PlayerWeaponsManager.onSwitchedToWeapon += ChangeWeapon;
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_20 = __this->get_m_PlayerWeaponsManager_6();
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_21 = L_20;
		NullCheck(L_21);
		UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F * L_22 = L_21->get_onSwitchedToWeapon_25();
		UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F * L_23 = (UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F *)il2cpp_codegen_object_new(UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m9362988F4BB4CD4FE51DB792660114863167415B(L_23, __this, (intptr_t)((intptr_t)WeaponHUDManager_ChangeWeapon_mF6F576F5B15E561DF5D09CCE7417BA699E072683_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_m9362988F4BB4CD4FE51DB792660114863167415B_RuntimeMethod_var);
		Delegate_t * L_24 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->set_onSwitchedToWeapon_25(((UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F *)CastclassSealed((RuntimeObject*)L_24, UnityAction_1_t542078F3B444DF90A2B41A08D9468CE8C413626F_il2cpp_TypeInfo_var)));
		// }
		return;
	}
}
// System.Void WeaponHUDManager::AddWeapon(WeaponController,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponHUDManager_AddWeapon_m40F4058D81FFCA0640432F656F1506825190559D (WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773 * __this, WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * ___newWeapon0, int32_t ___weaponIndex1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeaponHUDManager_AddWeapon_m40F4058D81FFCA0640432F656F1506825190559D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_0 = NULL;
	AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * V_1 = NULL;
	{
		// GameObject ammoCounterInstance = Instantiate(ammoCounterPrefab, ammosPanel);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_ammoCounterPrefab_5();
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_1 = __this->get_ammosPanel_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m1CDF66D563B03D37B37264800222D4F3B307EDA0(L_0, L_1, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m1CDF66D563B03D37B37264800222D4F3B307EDA0_RuntimeMethod_var);
		V_0 = L_2;
		// AmmoCounter newAmmoCounter = ammoCounterInstance.GetComponent<AmmoCounter>();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = V_0;
		NullCheck(L_3);
		AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * L_4 = GameObject_GetComponent_TisAmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1_m39A810E8DC3B5DED898125C40BAFAB7B76097890(L_3, /*hidden argument*/GameObject_GetComponent_TisAmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1_m39A810E8DC3B5DED898125C40BAFAB7B76097890_RuntimeMethod_var);
		V_1 = L_4;
		// DebugUtility.HandleErrorIfNullGetComponent<AmmoCounter, WeaponHUDManager>(newAmmoCounter, this, ammoCounterInstance.gameObject);
		AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * L_5 = V_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = V_0;
		NullCheck(L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = GameObject_get_gameObject_mB8D6D847ABF95430B098554F3F2D63EC1D30C815(L_6, /*hidden argument*/NULL);
		DebugUtility_HandleErrorIfNullGetComponent_TisAmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1_TisWeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773_mD00757FACC332D1A15B551F8489368227494D2FE(L_5, __this, L_7, /*hidden argument*/DebugUtility_HandleErrorIfNullGetComponent_TisAmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1_TisWeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773_mD00757FACC332D1A15B551F8489368227494D2FE_RuntimeMethod_var);
		// newAmmoCounter.Initialize(newWeapon, weaponIndex);
		AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * L_8 = V_1;
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_9 = ___newWeapon0;
		int32_t L_10 = ___weaponIndex1;
		NullCheck(L_8);
		AmmoCounter_Initialize_m8213538E0F5CCEF79F8FF69501026966062B9FAB(L_8, L_9, L_10, /*hidden argument*/NULL);
		// m_AmmoCounters.Add(newAmmoCounter);
		List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * L_11 = __this->get_m_AmmoCounters_7();
		AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * L_12 = V_1;
		NullCheck(L_11);
		List_1_Add_mB69FED340E5222A49CA180DAD48E3BD3D31B05A7(L_11, L_12, /*hidden argument*/List_1_Add_mB69FED340E5222A49CA180DAD48E3BD3D31B05A7_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void WeaponHUDManager::RemoveWeapon(WeaponController,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponHUDManager_RemoveWeapon_m01F57E6BDDB655C20A2308D7D15FD8201AE58F15 (WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773 * __this, WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * ___newWeapon0, int32_t ___weaponIndex1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeaponHUDManager_RemoveWeapon_m01F57E6BDDB655C20A2308D7D15FD8201AE58F15_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// int foundCounterIndex = -1;
		V_0 = (-1);
		// for (int i = 0; i < m_AmmoCounters.Count; i++)
		V_1 = 0;
		goto IL_0036;
	}

IL_0006:
	{
		// if(m_AmmoCounters[i].weaponCounterIndex == weaponIndex)
		List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * L_0 = __this->get_m_AmmoCounters_7();
		int32_t L_1 = V_1;
		NullCheck(L_0);
		AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * L_2 = List_1_get_Item_m612C715B84047B764A1764A095EDD7E7CF2EA032_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m612C715B84047B764A1764A095EDD7E7CF2EA032_RuntimeMethod_var);
		NullCheck(L_2);
		int32_t L_3 = AmmoCounter_get_weaponCounterIndex_mB3100D3B5713CA73962911C3D47D0E84BD4584B6_inline(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___weaponIndex1;
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0032;
		}
	}
	{
		// foundCounterIndex = i;
		int32_t L_5 = V_1;
		V_0 = L_5;
		// Destroy(m_AmmoCounters[i].gameObject);
		List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * L_6 = __this->get_m_AmmoCounters_7();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * L_8 = List_1_get_Item_m612C715B84047B764A1764A095EDD7E7CF2EA032_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m612C715B84047B764A1764A095EDD7E7CF2EA032_RuntimeMethod_var);
		NullCheck(L_8);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_9, /*hidden argument*/NULL);
	}

IL_0032:
	{
		// for (int i = 0; i < m_AmmoCounters.Count; i++)
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0036:
	{
		// for (int i = 0; i < m_AmmoCounters.Count; i++)
		int32_t L_11 = V_1;
		List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * L_12 = __this->get_m_AmmoCounters_7();
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m104F10F39220D74146D1DE9669E44D6AB9E09802_inline(L_12, /*hidden argument*/List_1_get_Count_m104F10F39220D74146D1DE9669E44D6AB9E09802_RuntimeMethod_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0006;
		}
	}
	{
		// if(foundCounterIndex >= 0)
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) < ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		// m_AmmoCounters.RemoveAt(foundCounterIndex);
		List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * L_15 = __this->get_m_AmmoCounters_7();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		List_1_RemoveAt_mF496CA03E4E0DC9E1F02770EE5B86E17A49212B7(L_15, L_16, /*hidden argument*/List_1_RemoveAt_mF496CA03E4E0DC9E1F02770EE5B86E17A49212B7_RuntimeMethod_var);
	}

IL_0054:
	{
		// }
		return;
	}
}
// System.Void WeaponHUDManager::ChangeWeapon(WeaponController)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponHUDManager_ChangeWeapon_mF6F576F5B15E561DF5D09CCE7417BA699E072683 (WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773 * __this, WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * ___weapon0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeaponHUDManager_ChangeWeapon_mF6F576F5B15E561DF5D09CCE7417BA699E072683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(ammosPanel);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_0 = __this->get_ammosPanel_4();
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_il2cpp_TypeInfo_var);
		LayoutRebuilder_ForceRebuildLayoutImmediate_m376EFAB3B8D5C86A085DCFEECA0538B8BA03B732(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WeaponHUDManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponHUDManager__ctor_m1B5D93240B1B0EDD834D7294AD637F936C66D180 (WeaponHUDManager_t719B00A33A6C1DB51E6E9D86B5700FB6F10BD773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeaponHUDManager__ctor_m1B5D93240B1B0EDD834D7294AD637F936C66D180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// List<AmmoCounter> m_AmmoCounters = new List<AmmoCounter>();
		List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 * L_0 = (List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0 *)il2cpp_codegen_object_new(List_1_t1C050897AADFD3E233A64605E0C873F3DDE111E0_il2cpp_TypeInfo_var);
		List_1__ctor_mEDA6C28C20A6EEA03532D2EF4E7196BCB85A6859(L_0, /*hidden argument*/List_1__ctor_mEDA6C28C20A6EEA03532D2EF4E7196BCB85A6859_RuntimeMethod_var);
		__this->set_m_AmmoCounters_7(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WeaponPickup::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponPickup_Start_mDD98BD5363E0B4D89AA720E4BD6D942924A5A7DB (WeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeaponPickup_Start_mDD98BD5363E0B4D89AA720E4BD6D942924A5A7DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* V_0 = NULL;
	int32_t V_1 = 0;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_2 = NULL;
	{
		// m_Pickup = GetComponent<Pickup>();
		Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * L_0 = Component_GetComponent_TisPickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281_m7BE6031779B73EEFDE3F1F50ABEA23A1EF8DE198(__this, /*hidden argument*/Component_GetComponent_TisPickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281_m7BE6031779B73EEFDE3F1F50ABEA23A1EF8DE198_RuntimeMethod_var);
		__this->set_m_Pickup_5(L_0);
		// DebugUtility.HandleErrorIfNullGetComponent<Pickup, WeaponPickup>(m_Pickup, this, gameObject);
		Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * L_1 = __this->get_m_Pickup_5();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		DebugUtility_HandleErrorIfNullGetComponent_TisPickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281_TisWeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2_m4356919BC776A07298536A42B1D737045A9768B2(L_1, __this, L_2, /*hidden argument*/DebugUtility_HandleErrorIfNullGetComponent_TisPickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281_TisWeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2_m4356919BC776A07298536A42B1D737045A9768B2_RuntimeMethod_var);
		// m_Pickup.onPick += OnPicked;
		Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * L_3 = __this->get_m_Pickup_5();
		Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * L_4 = L_3;
		NullCheck(L_4);
		UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D * L_5 = L_4->get_onPick_9();
		UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D * L_6 = (UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D *)il2cpp_codegen_object_new(UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4317C8EF454DAF2A7178F5A3854C7271732082FA(L_6, __this, (intptr_t)((intptr_t)WeaponPickup_OnPicked_m19C08F01C850D8A2FA89CC1A961006A541A2AF6C_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_m4317C8EF454DAF2A7178F5A3854C7271732082FA_RuntimeMethod_var);
		Delegate_t * L_7 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_onPick_9(((UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D *)CastclassSealed((RuntimeObject*)L_7, UnityAction_1_t760260B2BC32532D46A72AE976AC0A4A773EE27D_il2cpp_TypeInfo_var)));
		// foreach(Transform t in GetComponentsInChildren<Transform>())
		TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* L_8 = Component_GetComponentsInChildren_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m335E18CE04D2AD7313D89DBEA3A88EDB76255113(__this, /*hidden argument*/Component_GetComponentsInChildren_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m335E18CE04D2AD7313D89DBEA3A88EDB76255113_RuntimeMethod_var);
		V_0 = L_8;
		V_1 = 0;
		goto IL_0072;
	}

IL_0050:
	{
		// foreach(Transform t in GetComponentsInChildren<Transform>())
		TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_2 = L_12;
		// if (t != transform)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = V_2;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006e;
		}
	}
	{
		// t.gameObject.layer = 0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = V_2;
		NullCheck(L_16);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_17 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		GameObject_set_layer_mDAC8037FCFD0CE62DB66004C4342EA20CF604907(L_17, 0, /*hidden argument*/NULL);
	}

IL_006e:
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0072:
	{
		// foreach(Transform t in GetComponentsInChildren<Transform>())
		int32_t L_19 = V_1;
		TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* L_20 = V_0;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length)))))))
		{
			goto IL_0050;
		}
	}
	{
		// }
		return;
	}
}
// System.Void WeaponPickup::OnPicked(PlayerCharacterController)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponPickup_OnPicked_m19C08F01C850D8A2FA89CC1A961006A541A2AF6C (WeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2 * __this, PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072 * ___byPlayer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeaponPickup_OnPicked_m19C08F01C850D8A2FA89CC1A961006A541A2AF6C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * V_0 = NULL;
	{
		// PlayerWeaponsManager playerWeaponsManager = byPlayer.GetComponent<PlayerWeaponsManager>();
		PlayerCharacterController_t9C9F4E7D313D11458D6BF34095EC57228C65D072 * L_0 = ___byPlayer0;
		NullCheck(L_0);
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_1 = Component_GetComponent_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_m6268AA831D180B5189871925A88950D5E2D9F873(L_0, /*hidden argument*/Component_GetComponent_TisPlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB_m6268AA831D180B5189871925A88950D5E2D9F873_RuntimeMethod_var);
		V_0 = L_1;
		// if (playerWeaponsManager)
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0048;
		}
	}
	{
		// if (playerWeaponsManager.AddWeapon(weaponPrefab))
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_4 = V_0;
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_5 = __this->get_weaponPrefab_4();
		NullCheck(L_4);
		bool L_6 = PlayerWeaponsManager_AddWeapon_m6687BD9B7E34BB41035B0CB80BD14EF2488B5761(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}
	{
		// if (playerWeaponsManager.GetActiveWeapon() == null)
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_7 = V_0;
		NullCheck(L_7);
		WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * L_8 = PlayerWeaponsManager_GetActiveWeapon_m2E9EF7BAE1D06DB5CCC560272A3FC7754FBC3ABD(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_8, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0032;
		}
	}
	{
		// playerWeaponsManager.SwitchWeapon(true);
		PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * L_10 = V_0;
		NullCheck(L_10);
		PlayerWeaponsManager_SwitchWeapon_m08848BFC67A6310BEEC3D2717525745A25997DA2(L_10, (bool)1, /*hidden argument*/NULL);
	}

IL_0032:
	{
		// m_Pickup.PlayPickupFeedback();
		Pickup_tE2790DAF1EA3DBCE93CC71EB358E85500CA93281 * L_11 = __this->get_m_Pickup_5();
		NullCheck(L_11);
		Pickup_PlayPickupFeedback_mD48A3AE0650E0A46BE9490336496B59741B0F1FC(L_11, /*hidden argument*/NULL);
		// Destroy(gameObject);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_12, /*hidden argument*/NULL);
	}

IL_0048:
	{
		// }
		return;
	}
}
// System.Void WeaponPickup::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponPickup__ctor_m1ADFB171B8333C4EA16C3FB0B09F861548EB206E (WeaponPickup_tBC11E30510115015643342FC5DE084D826536AB2 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldspaceHealthBar::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldspaceHealthBar_Update_mE1B2CE097FF63BDA7D2EA4A5D538FBF65BF676B8 (WorldspaceHealthBar_t74F5087839EF95CD78752D943A539D313AA3B9E3 * __this, const RuntimeMethod* method)
{
	{
		// healthBarImage.fillAmount = health.currentHealth / health.maxHealth;
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_0 = __this->get_healthBarImage_5();
		Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * L_1 = __this->get_health_4();
		NullCheck(L_1);
		float L_2 = Health_get_currentHealth_m3702D4E7A03A5153B3383738CC91CECC14EA5D47_inline(L_1, /*hidden argument*/NULL);
		Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * L_3 = __this->get_health_4();
		NullCheck(L_3);
		float L_4 = L_3->get_maxHealth_4();
		NullCheck(L_0);
		Image_set_fillAmount_m613B7AC920F2EA886AF0D931005C6CD13F89A160(L_0, ((float)((float)L_2/(float)L_4)), /*hidden argument*/NULL);
		// healthBarPivot.LookAt(Camera.main.transform.position);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = __this->get_healthBarPivot_6();
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_6 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_LookAt_m3EC94482B2585FE03AEEDF90325A1F0B9A84960E(L_5, L_8, /*hidden argument*/NULL);
		// if (hideFullHealthBar)
		bool L_9 = __this->get_hideFullHealthBar_7();
		if (!L_9)
		{
			goto IL_0069;
		}
	}
	{
		// healthBarPivot.gameObject.SetActive(healthBarImage.fillAmount != 1);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = __this->get_healthBarPivot_6();
		NullCheck(L_10);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_10, /*hidden argument*/NULL);
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_12 = __this->get_healthBarImage_5();
		NullCheck(L_12);
		float L_13 = Image_get_fillAmount_m5FF6EE8DB33C4A219B3677FB24F50A6E5CCF44F0_inline(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_11, (bool)((((int32_t)((((float)L_13) == ((float)(1.0f)))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0069:
	{
		// }
		return;
	}
}
// System.Void WorldspaceHealthBar::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldspaceHealthBar__ctor_m9DD18744523EDD803B3A75898E4EDEF38DFC17FE (WorldspaceHealthBar_t74F5087839EF95CD78752D943A539D313AA3B9E3 * __this, const RuntimeMethod* method)
{
	{
		// public bool hideFullHealthBar = true;
		__this->set_hideFullHealthBar_7((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float WeaponController_get_currentAmmoRatio_mF7180E6799FB3AD19BE14019CE98B6A12125C842_inline (WeaponController_t781A5E34C5F3AC769CDB75787E091A1F45E18C8C * __this, const RuntimeMethod* method)
{
	{
		// public float currentAmmoRatio { get; private set; }
		float L_0 = __this->get_U3CcurrentAmmoRatioU3Ek__BackingField_44();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PlayerWeaponsManager_get_activeWeaponIndex_mC60445132555E8ABA60E7B70B18C05FC15426764_inline (PlayerWeaponsManager_tA9C7C8D8E25CBBA635B4B6976D2DC751ED2BEFCB * __this, const RuntimeMethod* method)
{
	{
		// public int activeWeaponIndex { get; private set; }
		int32_t L_0 = __this->get_U3CactiveWeaponIndexU3Ek__BackingField_24();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t AmmoCounter_get_weaponCounterIndex_mB3100D3B5713CA73962911C3D47D0E84BD4584B6_inline (AmmoCounter_t5DC68D8946E191186466FF564D5BC2C46B2474E1 * __this, const RuntimeMethod* method)
{
	{
		// public int weaponCounterIndex { get; set; }
		int32_t L_0 = __this->get_U3CweaponCounterIndexU3Ek__BackingField_14();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float Health_get_currentHealth_m3702D4E7A03A5153B3383738CC91CECC14EA5D47_inline (Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * __this, const RuntimeMethod* method)
{
	{
		// public float currentHealth { get; set; }
		float L_0 = __this->get_U3CcurrentHealthU3Ek__BackingField_9();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float Image_get_fillAmount_m5FF6EE8DB33C4A219B3677FB24F50A6E5CCF44F0_inline (Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * __this, const RuntimeMethod* method)
{
	{
		// public float fillAmount { get { return m_FillAmount; } set { if (SetPropertyUtility.SetStruct(ref m_FillAmount, Mathf.Clamp01(value))) SetVerticesDirty(); } }
		float L_0 = __this->get_m_FillAmount_42();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, (int32_t)L_3);
		return L_4;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
