﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern void Collision_get_gameObject_m9A7069ABE50D4BB957A8ED76E5F4A59ACEC57C49 ();
// 0x00000002 UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern void RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9_AdjustorThunk ();
// 0x00000003 UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern void RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001_AdjustorThunk ();
// 0x00000004 System.Void UnityEngine.RaycastHit::set_point(UnityEngine.Vector3)
extern void RaycastHit_set_point_mCB652835DA3A0AED8A8574B4A47FD9BF8F9A4191_AdjustorThunk ();
// 0x00000005 UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern void RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94_AdjustorThunk ();
// 0x00000006 System.Void UnityEngine.RaycastHit::set_normal(UnityEngine.Vector3)
extern void RaycastHit_set_normal_mB21B55B92CE45F93EB140793D31DFE69693F7303_AdjustorThunk ();
// 0x00000007 System.Single UnityEngine.RaycastHit::get_distance()
extern void RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543_AdjustorThunk ();
// 0x00000008 System.Void UnityEngine.RaycastHit::set_distance(System.Single)
extern void RaycastHit_set_distance_mD9B4333B194688CF5D126F993FE4576D268EAF07_AdjustorThunk ();
// 0x00000009 System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern void Rigidbody_set_isKinematic_m856AB59E5A6207892C439AFC8DDF5620B941E71B ();
// 0x0000000A System.Boolean UnityEngine.Collider::get_isTrigger()
extern void Collider_get_isTrigger_m08B7B55C34B99492CE923444B5253A7812BC8D8E ();
// 0x0000000B System.Void UnityEngine.Collider::set_isTrigger(System.Boolean)
extern void Collider_set_isTrigger_mD9EB1E99EA96B08398D68188F2DEB2434C1890C5 ();
// 0x0000000C System.Void UnityEngine.Collider::.ctor()
extern void Collider__ctor_m4E6D2F06C3BDB9CB006BD94C294049209D7563ED ();
// 0x0000000D UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern void CharacterController_Move_m31D77B4E934015FE3D6CE04BF8017A1DD0487434 ();
// 0x0000000E System.Single UnityEngine.CharacterController::get_radius()
extern void CharacterController_get_radius_m338506473432572441A8EC04C6315A2E272EED62 ();
// 0x0000000F System.Single UnityEngine.CharacterController::get_height()
extern void CharacterController_get_height_m03BF94F70626679E706F9529E7D9C2E42B4D33AA ();
// 0x00000010 System.Void UnityEngine.CharacterController::set_height(System.Single)
extern void CharacterController_set_height_mEC0B265791CB25FA59892CD2273A205EE78825B4 ();
// 0x00000011 UnityEngine.Vector3 UnityEngine.CharacterController::get_center()
extern void CharacterController_get_center_mB221CC1D4FE223B4854187F98E6903685DE4FF9A ();
// 0x00000012 System.Void UnityEngine.CharacterController::set_center(UnityEngine.Vector3)
extern void CharacterController_set_center_m85EAA97EF9C9369A10B488251038910B843590A0 ();
// 0x00000013 System.Single UnityEngine.CharacterController::get_slopeLimit()
extern void CharacterController_get_slopeLimit_m1F45E11460DB926AE861B8F606C7A0F41613869E ();
// 0x00000014 System.Single UnityEngine.CharacterController::get_skinWidth()
extern void CharacterController_get_skinWidth_mD41A2207DDC115DF41AED202A76E8C83F6400BBE ();
// 0x00000015 System.Void UnityEngine.CharacterController::set_enableOverlapRecovery(System.Boolean)
extern void CharacterController_set_enableOverlapRecovery_m715E63C38CD05B2A94C98C1DBD7F2854AD57AFD7 ();
// 0x00000016 UnityEngine.CollisionFlags UnityEngine.CharacterController::Move_Injected(UnityEngine.Vector3&)
extern void CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551 ();
// 0x00000017 System.Void UnityEngine.CharacterController::get_center_Injected(UnityEngine.Vector3&)
extern void CharacterController_get_center_Injected_m9C6D74959FA2165C808F544FA55F8D3D20526DBE ();
// 0x00000018 System.Void UnityEngine.CharacterController::set_center_Injected(UnityEngine.Vector3&)
extern void CharacterController_set_center_Injected_mB0CABC40795A76B769EE17341D7F1D7F4B7D74FE ();
// 0x00000019 System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
extern void MeshCollider_set_sharedMesh_m88BE136B396DA960BC78F1A563BE0A810AA0FBBA ();
// 0x0000001A System.String UnityEngine.PhysicsScene::ToString()
extern void PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9_AdjustorThunk ();
// 0x0000001B System.Int32 UnityEngine.PhysicsScene::GetHashCode()
extern void PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654_AdjustorThunk ();
// 0x0000001C System.Boolean UnityEngine.PhysicsScene::Equals(System.Object)
extern void PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9_AdjustorThunk ();
// 0x0000001D System.Boolean UnityEngine.PhysicsScene::Equals(UnityEngine.PhysicsScene)
extern void PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8_AdjustorThunk ();
// 0x0000001E System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336_AdjustorThunk ();
// 0x0000001F System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_m09DD752D166094AC2A6F19302F7DDB2EA88DE67F ();
// 0x00000020 System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B_AdjustorThunk ();
// 0x00000021 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_m2769B78FDCAA21C6F609DD709BADFF6F4B07C529 ();
// 0x00000022 System.Int32 UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1_AdjustorThunk ();
// 0x00000023 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc(UnityEngine.PhysicsScene,UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_m9E5DAC724FC74D6B618C82BC8F19980E9636D5B1 ();
// 0x00000024 System.Boolean UnityEngine.PhysicsScene::Query_CapsuleCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_CapsuleCast_m62813C3DE93000767BDF5E789E7D82B6944B96E9 ();
// 0x00000025 System.Boolean UnityEngine.PhysicsScene::Internal_CapsuleCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_CapsuleCast_m98099F6A0153C33964D1DC312467B438F2CD02BB ();
// 0x00000026 System.Boolean UnityEngine.PhysicsScene::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_CapsuleCast_mBA2BB56A5EFA893BE437056DBBE6B91D9326B0BC_AdjustorThunk ();
// 0x00000027 System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF ();
// 0x00000028 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA ();
// 0x00000029 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E ();
// 0x0000002A System.Boolean UnityEngine.PhysicsScene::Query_CapsuleCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_CapsuleCast_Injected_mB12FAFD383D49067D93BFCADF6EBB21FDF89711B ();
// 0x0000002B UnityEngine.PhysicsScene UnityEngine.Physics::get_defaultPhysicsScene()
extern void Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30 ();
// 0x0000002C System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m511ECD9EF3223645EF104DDA50BB81C07F29717C ();
// 0x0000002D System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_Raycast_mC87F52EDC44BBBAFFED0D9DC92B37B11C6E07BEA ();
// 0x0000002E System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_Raycast_m0583CCAA9E2F3BD031F12FA080837E9A48EEC16D ();
// 0x0000002F System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_Raycast_m6FFBDC7166A2129B70027FC392D06C735F81C73E ();
// 0x00000030 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m44270C2D7C8CD056B6D78DE60A4C91FCC0A225C1 ();
// 0x00000031 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_mFB0018A196CC9E45CA8A238899EA8B093264B024 ();
// 0x00000032 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_mBEC747ED0A7660BB12AA48B663CCBF7B1EE93D6B ();
// 0x00000033 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern void Physics_Raycast_m000FC36D038952F8AC5E3E562E7D39BAA8D1E287 ();
// 0x00000034 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_mF33E3BA9FDFE0D19B193D2AB05028B2086738D47 ();
// 0x00000035 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_Raycast_m5BA453EA32F5B660CD9A537FED97B995971820E1 ();
// 0x00000036 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern void Physics_Raycast_m198FD20B3D1BC1634AE30E80E0FD1491D55417E5 ();
// 0x00000037 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern void Physics_Raycast_mF1112EC53F3393455B4ABA0E0092A58E3CD0114A ();
// 0x00000038 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m6B6C7CB6DDED17F9B1905C606035A56FF9AE88F8 ();
// 0x00000039 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_m9F5CAD8DA7923E897C5DCAF913BD411AD1A87950 ();
// 0x0000003A System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_mE1590EE4E2DC950A9FC2437E98EE8CD2EC2DEE67 ();
// 0x0000003B System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern void Physics_Raycast_mCE618579F667A62D15CB74CEF76147136EB2D495 ();
// 0x0000003C System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CapsuleCast_mE59C9FD33569870B9C811E5930CB5D0D7A43DADC ();
// 0x0000003D UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_m366801D3E9D3DE8E9A94C5243D1FB2132DFDD851 ();
// 0x0000003E UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D ();
// 0x0000003F UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_RaycastAll_m4FC397E4B7FEF7B5C9D172F95D6FB32CFC0F66FC ();
// 0x00000040 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_RaycastAll_m93B94F31C64E8F64ACE18231FC85168D519B7F04 ();
// 0x00000041 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_RaycastAll_mE2ED29FCF8983C212F2628A8934CFAEBA4B683DE ();
// 0x00000042 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_mF5DB872CCEFE1710439DB9939EACE1DCDEFE2A7D ();
// 0x00000043 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_RaycastAll_mE9ACFB603E337E2195E4B9419099E178F7FF82F4 ();
// 0x00000044 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern void Physics_RaycastAll_m6D5629FD1D3E3B0001F2819C1AE8660A3949C51F ();
// 0x00000045 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern void Physics_RaycastAll_m37C7F423486CE3ACBAB4F1A326B9EBD7572B4056 ();
// 0x00000046 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m606B435CE959801C9B7D7111E49071E893C951DE ();
// 0x00000047 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_mBFDB05B1AA90C5D587A1BEA6BEE66FEF2DF89894 ();
// 0x00000048 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m0A9CEF12FB7EE9C790609A15CBFE975725365CF9 ();
// 0x00000049 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_m9CC4B976EA1936076FFA1339BF6FBE78A2DA593F ();
// 0x0000004A System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m120ADF6BD8DF57A40E6B395299B03FAEE348973F ();
// 0x0000004B System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_mA795C150FCE1F242C9806AAAFF04125594B56DDB ();
// 0x0000004C System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m63644DC59B0554846D62ED3BE3B5517BC97D3316 ();
// 0x0000004D System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_mD4393134F4470C9B319603A6CD3FF601F5981C88 ();
// 0x0000004E UnityEngine.RaycastHit[] UnityEngine.Physics::Query_SphereCastAll(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Query_SphereCastAll_m92155C4717FC0AC27A76DACC349E94F75685053A ();
// 0x0000004F UnityEngine.RaycastHit[] UnityEngine.Physics::SphereCastAll(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_SphereCastAll_m30813767B905D3E6D32E14BAE55D07B4E93587D0 ();
// 0x00000050 UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapCapsule_Internal_mB0EE7661B1C20F06E3EE3D9D3E1A1E68AB379655 ();
// 0x00000051 UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapCapsule_m5C6F1F8F8A29D0E465B9C898141572B8EEC12A63 ();
// 0x00000052 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_Internal_m10C15A0F86303EF92B205F16591E344E5728B6D0 ();
// 0x00000053 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_m73878904F621A28F8DD8855CB113C690CF1E89C0 ();
// 0x00000054 System.Void UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&)
extern void Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262 ();
// 0x00000055 UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913 ();
// 0x00000056 UnityEngine.RaycastHit[] UnityEngine.Physics::Query_SphereCastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Query_SphereCastAll_Injected_m679F7242E41A4D7E4ACB206EF6AE3BB92B1B6FD8 ();
// 0x00000057 UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapCapsule_Internal_Injected_m73D80DC6F91C3595C9DE41B8929986A569379BA8 ();
// 0x00000058 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_Internal_Injected_m315C4BB796FD73B41F5710D1DB52A6BCB57C344D ();
static Il2CppMethodPointer s_methodPointers[88] = 
{
	Collision_get_gameObject_m9A7069ABE50D4BB957A8ED76E5F4A59ACEC57C49,
	RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9_AdjustorThunk,
	RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001_AdjustorThunk,
	RaycastHit_set_point_mCB652835DA3A0AED8A8574B4A47FD9BF8F9A4191_AdjustorThunk,
	RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94_AdjustorThunk,
	RaycastHit_set_normal_mB21B55B92CE45F93EB140793D31DFE69693F7303_AdjustorThunk,
	RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543_AdjustorThunk,
	RaycastHit_set_distance_mD9B4333B194688CF5D126F993FE4576D268EAF07_AdjustorThunk,
	Rigidbody_set_isKinematic_m856AB59E5A6207892C439AFC8DDF5620B941E71B,
	Collider_get_isTrigger_m08B7B55C34B99492CE923444B5253A7812BC8D8E,
	Collider_set_isTrigger_mD9EB1E99EA96B08398D68188F2DEB2434C1890C5,
	Collider__ctor_m4E6D2F06C3BDB9CB006BD94C294049209D7563ED,
	CharacterController_Move_m31D77B4E934015FE3D6CE04BF8017A1DD0487434,
	CharacterController_get_radius_m338506473432572441A8EC04C6315A2E272EED62,
	CharacterController_get_height_m03BF94F70626679E706F9529E7D9C2E42B4D33AA,
	CharacterController_set_height_mEC0B265791CB25FA59892CD2273A205EE78825B4,
	CharacterController_get_center_mB221CC1D4FE223B4854187F98E6903685DE4FF9A,
	CharacterController_set_center_m85EAA97EF9C9369A10B488251038910B843590A0,
	CharacterController_get_slopeLimit_m1F45E11460DB926AE861B8F606C7A0F41613869E,
	CharacterController_get_skinWidth_mD41A2207DDC115DF41AED202A76E8C83F6400BBE,
	CharacterController_set_enableOverlapRecovery_m715E63C38CD05B2A94C98C1DBD7F2854AD57AFD7,
	CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551,
	CharacterController_get_center_Injected_m9C6D74959FA2165C808F544FA55F8D3D20526DBE,
	CharacterController_set_center_Injected_mB0CABC40795A76B769EE17341D7F1D7F4B7D74FE,
	MeshCollider_set_sharedMesh_m88BE136B396DA960BC78F1A563BE0A810AA0FBBA,
	PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9_AdjustorThunk,
	PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654_AdjustorThunk,
	PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9_AdjustorThunk,
	PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8_AdjustorThunk,
	PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336_AdjustorThunk,
	PhysicsScene_Internal_RaycastTest_m09DD752D166094AC2A6F19302F7DDB2EA88DE67F,
	PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B_AdjustorThunk,
	PhysicsScene_Internal_Raycast_m2769B78FDCAA21C6F609DD709BADFF6F4B07C529,
	PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1_AdjustorThunk,
	PhysicsScene_Internal_RaycastNonAlloc_m9E5DAC724FC74D6B618C82BC8F19980E9636D5B1,
	PhysicsScene_Query_CapsuleCast_m62813C3DE93000767BDF5E789E7D82B6944B96E9,
	PhysicsScene_Internal_CapsuleCast_m98099F6A0153C33964D1DC312467B438F2CD02BB,
	PhysicsScene_CapsuleCast_mBA2BB56A5EFA893BE437056DBBE6B91D9326B0BC_AdjustorThunk,
	PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF,
	PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA,
	PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E,
	PhysicsScene_Query_CapsuleCast_Injected_mB12FAFD383D49067D93BFCADF6EBB21FDF89711B,
	Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30,
	Physics_Raycast_m511ECD9EF3223645EF104DDA50BB81C07F29717C,
	Physics_Raycast_mC87F52EDC44BBBAFFED0D9DC92B37B11C6E07BEA,
	Physics_Raycast_m0583CCAA9E2F3BD031F12FA080837E9A48EEC16D,
	Physics_Raycast_m6FFBDC7166A2129B70027FC392D06C735F81C73E,
	Physics_Raycast_m44270C2D7C8CD056B6D78DE60A4C91FCC0A225C1,
	Physics_Raycast_mFB0018A196CC9E45CA8A238899EA8B093264B024,
	Physics_Raycast_mBEC747ED0A7660BB12AA48B663CCBF7B1EE93D6B,
	Physics_Raycast_m000FC36D038952F8AC5E3E562E7D39BAA8D1E287,
	Physics_Raycast_mF33E3BA9FDFE0D19B193D2AB05028B2086738D47,
	Physics_Raycast_m5BA453EA32F5B660CD9A537FED97B995971820E1,
	Physics_Raycast_m198FD20B3D1BC1634AE30E80E0FD1491D55417E5,
	Physics_Raycast_mF1112EC53F3393455B4ABA0E0092A58E3CD0114A,
	Physics_Raycast_m6B6C7CB6DDED17F9B1905C606035A56FF9AE88F8,
	Physics_Raycast_m9F5CAD8DA7923E897C5DCAF913BD411AD1A87950,
	Physics_Raycast_mE1590EE4E2DC950A9FC2437E98EE8CD2EC2DEE67,
	Physics_Raycast_mCE618579F667A62D15CB74CEF76147136EB2D495,
	Physics_CapsuleCast_mE59C9FD33569870B9C811E5930CB5D0D7A43DADC,
	Physics_Internal_RaycastAll_m366801D3E9D3DE8E9A94C5243D1FB2132DFDD851,
	Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D,
	Physics_RaycastAll_m4FC397E4B7FEF7B5C9D172F95D6FB32CFC0F66FC,
	Physics_RaycastAll_m93B94F31C64E8F64ACE18231FC85168D519B7F04,
	Physics_RaycastAll_mE2ED29FCF8983C212F2628A8934CFAEBA4B683DE,
	Physics_RaycastAll_mF5DB872CCEFE1710439DB9939EACE1DCDEFE2A7D,
	Physics_RaycastAll_mE9ACFB603E337E2195E4B9419099E178F7FF82F4,
	Physics_RaycastAll_m6D5629FD1D3E3B0001F2819C1AE8660A3949C51F,
	Physics_RaycastAll_m37C7F423486CE3ACBAB4F1A326B9EBD7572B4056,
	Physics_RaycastNonAlloc_m606B435CE959801C9B7D7111E49071E893C951DE,
	Physics_RaycastNonAlloc_mBFDB05B1AA90C5D587A1BEA6BEE66FEF2DF89894,
	Physics_RaycastNonAlloc_m0A9CEF12FB7EE9C790609A15CBFE975725365CF9,
	Physics_RaycastNonAlloc_m9CC4B976EA1936076FFA1339BF6FBE78A2DA593F,
	Physics_RaycastNonAlloc_m120ADF6BD8DF57A40E6B395299B03FAEE348973F,
	Physics_RaycastNonAlloc_mA795C150FCE1F242C9806AAAFF04125594B56DDB,
	Physics_RaycastNonAlloc_m63644DC59B0554846D62ED3BE3B5517BC97D3316,
	Physics_RaycastNonAlloc_mD4393134F4470C9B319603A6CD3FF601F5981C88,
	Physics_Query_SphereCastAll_m92155C4717FC0AC27A76DACC349E94F75685053A,
	Physics_SphereCastAll_m30813767B905D3E6D32E14BAE55D07B4E93587D0,
	Physics_OverlapCapsule_Internal_mB0EE7661B1C20F06E3EE3D9D3E1A1E68AB379655,
	Physics_OverlapCapsule_m5C6F1F8F8A29D0E465B9C898141572B8EEC12A63,
	Physics_OverlapSphere_Internal_m10C15A0F86303EF92B205F16591E344E5728B6D0,
	Physics_OverlapSphere_m73878904F621A28F8DD8855CB113C690CF1E89C0,
	Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262,
	Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913,
	Physics_Query_SphereCastAll_Injected_m679F7242E41A4D7E4ACB206EF6AE3BB92B1B6FD8,
	Physics_OverlapCapsule_Internal_Injected_m73D80DC6F91C3595C9DE41B8929986A569379BA8,
	Physics_OverlapSphere_Internal_Injected_m315C4BB796FD73B41F5710D1DB52A6BCB57C344D,
};
static const int32_t s_InvokerIndices[88] = 
{
	14,
	14,
	1000,
	1001,
	1000,
	1001,
	654,
	275,
	31,
	102,
	31,
	23,
	1257,
	654,
	654,
	275,
	1000,
	1001,
	654,
	654,
	31,
	1258,
	6,
	6,
	26,
	14,
	10,
	9,
	1259,
	1260,
	1261,
	1262,
	1263,
	1264,
	1265,
	1266,
	1267,
	1268,
	1269,
	1270,
	1271,
	1272,
	1273,
	1274,
	1275,
	1276,
	1094,
	1277,
	1278,
	1279,
	1280,
	1281,
	1282,
	1283,
	1284,
	1285,
	1286,
	1287,
	1288,
	1289,
	1290,
	1291,
	1292,
	1293,
	1294,
	1295,
	1296,
	1297,
	1298,
	1299,
	1300,
	1301,
	1302,
	1303,
	1304,
	1305,
	1306,
	1307,
	1308,
	1309,
	1291,
	1310,
	1311,
	17,
	1312,
	1313,
	1314,
	1312,
};
extern const Il2CppCodeGenModule g_UnityEngine_PhysicsModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_PhysicsModuleCodeGenModule = 
{
	"UnityEngine.PhysicsModule.dll",
	88,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
